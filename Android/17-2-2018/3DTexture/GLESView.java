package com.astromedicomp.PP_3DTEXTURE;

import android.content.Context; 
import android.opengl.GLSurfaceView; // for OpenGL Surface View and all related
import javax.microedition.khronos.opengles.GL10; 
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32; // for OpenGLES 3.2
import android.view.MotionEvent; 
import android.view.GestureDetector; 
import android.view.GestureDetector.OnGestureListener; 
import android.view.GestureDetector.OnDoubleTapListener; 

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix; 

import android.graphics.BitmapFactory; 
import android.graphics.Bitmap; 
import android.opengl.GLUtils; 

public class GLESView extends GLSurfaceView 
implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] gVao_Pyramid = new int[1];
	private int[] gVao_Cube = new int[1];
	
	private int[] gVbo_Pyramid_Vertex = new int [1];
	//private int[] gVbo_Pyramid_Color = new int [1];
	private int[] gVbo_Pyramid_Texture = new int[1];
	
	private int[] gVbo_Cube_Vertex = new int [1];
	//private int[] gVbo_Cube_Color = new int [1];
	private int[] gVbo_Cube_Texture = new int[1];
	
	private int mvpUniform;
	private int texture0_sampler_uniform;
	private float perspectiveProjectionMatrix[] = new float[16];  
	
	
	//ANGLE OF ROTATION
	private float gfanglePyramid=0.0f;
	private float gfangleCube =0.0f;

	private int[] texture_Kundali = new int[1];
	private int[] texture_Stone = new int[1];
	

	public GLESView (Context drawingContext)
	{
	   super(drawingContext);

	   context = drawingContext;

	    setEGLContextClientVersion(3);

	   setRenderer(this);

	   setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	   gestureDetector = new GestureDetector(context,this,null,false);

	   gestureDetector.setOnDoubleTapListener(this);

	}

	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
	  
      String glesVersion =gl.glGetString(GL10.GL_VERSION);
      System.out.println("VDG: OpenGL-ES Version = " + glesVersion);
      
	
	  String glslVersion=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
	  System.out.println("VDG: GLSL Version = " +glslVersion);

	  initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
	   draw();
	} 

	
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
	   int eventAction = e.getAction();
	   
	   if(!gestureDetector.onTouchEvent(e))
	   {
	     super.onTouchEvent(e);
	   } 

	   return(true);
	}

	@Override 
	public boolean onDoubleTap(MotionEvent e)
	{
        return(true);
	}

	@Override 
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	   return(true);
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	   
	}

	@Override 
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override 
	public void onShowPress(MotionEvent e)
	{
	  
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		
        System.out.println("VDG :"+"initialize()");

	// Vertex Shader
		
		// Create Shader Program Object

		vertexShaderObject= GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		// vertex shader source code

		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;"+
			"in vec2 vTexture0_Coord;" +
			"out vec2 Out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"Out_texture0_coord=vTexture0_Coord;"+
			"}"
		);

		// provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// SHADER COMPILATION AND EROR CHECKING
	   GLES32.glCompileShader(vertexShaderObject);
	   int[] iShaderCompiledStatus = new int[1];
	   int[]iInfoLogLength= new int[1];
	   String szInfoLog=null;

	   GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
	   {
	      GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
			System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	// Fragment Shader
	 
	   // Create Fragment Shader

		fragmentShaderObject= GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		// fragment shader source code

		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;"+
			"in vec2 Out_texture0_coord;"+
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = texture(u_texture0_sampler,Out_texture0_coord);"+
			"}"
		);

		
		// provide source code to shader
		GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		//COMPILATION AND ERROR CHECKING
	   GLES32.glCompileShader(fragmentShaderObject);
	   iShaderCompiledStatus[0] = 0;
	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
	   {
	      GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
			System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // Create Shader Program object

	   shaderProgramObject= GLES32.glCreateProgram();

	   // Attach vertex shader and fragment shader to shader Program

	   GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);

	   GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

	   // pre-link binding of shader program object with vertex shader attributes

	   GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	   GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

	   //LINK BOTH SHADERS TO SHADERPROGRAMOBJECT
	   GLES32.glLinkProgram(shaderProgramObject);

	   int[] iShaderProgramLinkStatus = new int[1];

	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);

	   if(iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
	   {
	      GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);
			System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	   // get MVP uniform location
	   mvpUniform= GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
	  texture0_sampler_uniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_texture0_sampler");

	   //LOAD TEXTURE
	   texture_Stone[0] = loadGLTexture(R.raw.stone);
	   texture_Kundali[0] = loadGLTexture(R.raw.vijay_kundali_horz_inverted);

	   final float pyramidVertices[] = new float[]
	   {
			0.0f, 1.0f, 0.0f,   

			-1.0f, -1.0f, 1.0f,

			1.0f, -1.0f, 1.0f, 

			//right face

			0.0f, 1.0f, 0.0f,   

			1.0f, -1.0f, 1.0f,  

			1.0f, -1.0f, -1.0f, 

			//Back face

			0.0f, 1.0f, 0.0f, 

			1.0f, -1.0f, -1.0f, 

			-1.0f, -1.0f, -1.0f, 

			//left face

			0.0f, 1.0f, 0.0f, 

			-1.0f, -1.0f, -1.0f, 

			-1.0f, -1.0f, 1.0f, 


	   };
	   
		float pyramidTexcoords[]= new float[]
        {
            0.5f, 1.0f, 
            0.0f, 0.0f, 
            1.0f, 0.0f, 
                        
            0.5f, 1.0f, 
            1.0f, 0.0f, 
            0.0f, 0.0f, 
                        
            0.5f, 1.0f, 
            1.0f, 0.0f, 
            0.0f, 0.0f, 
                        
            0.5f, 1.0f, 
            0.0f, 0.0f, 
            1.0f, 0.0f 
        };
	  
//START PYRAMID RECORDER
	   GLES32.glGenVertexArrays(1,gVao_Pyramid,0);
	   GLES32.glBindVertexArray(gVao_Pyramid[0]);
			//BIND WITH VERTEX_BUFFER
			   GLES32.glGenBuffers(1,gVbo_Pyramid_Vertex,0);
			   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,gVbo_Pyramid_Vertex[0]);

			   ByteBuffer byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length * 4);
			   byteBuffer.order(ByteOrder.nativeOrder());
			   FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
			   verticesBuffer.put(pyramidVertices);
			   verticesBuffer.position(0);

			   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pyramidVertices.length * 4,verticesBuffer,GLES32.GL_STATIC_DRAW);
			   GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES32.GL_FLOAT,false,0,0);

			   GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
			   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
			 
		//BIND WITH COLOR_BUFFER 

			   GLES32.glGenBuffers(1,gVbo_Pyramid_Vertex,0);
			   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,gVbo_Pyramid_Vertex[0]);

			   ByteBuffer textureByteBuffer=ByteBuffer.allocateDirect(pyramidTexcoords.length * 4);
			   textureByteBuffer.order(ByteOrder.nativeOrder());
			   FloatBuffer textureBuffer = textureByteBuffer.asFloatBuffer();
			   textureBuffer.put(pyramidTexcoords);
			   textureBuffer.position(0);

			   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pyramidTexcoords.length * 4,textureBuffer,GLES32.GL_STATIC_DRAW);
			   GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES32.GL_FLOAT,false,0,0);

			   GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
			   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

			   
	   GLES32.glBindVertexArray(0);
	//UNBIND FROM PYRAMID RECORDER   
	
	//START CUBE RECORDER
	   final float cubeVertices[] = new float[]
	   {
		// Top face
			1.0f, 1.0f, -1.0f, 
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f, 
			1.0f, 1.0f, 1.0f, 

			//Bottom face
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f, 

			//Front face
			1.0f, 1.0f, 1.0f,   
			-1.0f, 1.0f, 1.0f, 	
			-1.0f, -1.0f, 1.0f, 
			1.0f, -1.0f, 1.0f, 	


			//Back face	
			1.0f, 1.0f, -1.0f,  
			-1.0f, 1.0f, -1.0f, 
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f, 

			//Right face
			1.0f, 1.0f, -1.0f,  
			1.0f, 1.0f, 1.0f, 	
			1.0f, -1.0f, 1.0f,  
			1.0f, -1.0f, -1.0f, 

			//Left face
			-1.0f, 1.0f, 1.0f,   
			-1.0f, 1.0f, -1.0f, 
			-1.0f, -1.0f, -1.0f, 
			-1.0f, -1.0f, 1.0f 
	   };
	   
	 float cubeTexcoords[]= new float[]
        {
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
        };

//VAO FOR CUBE
	
	 GLES32.glGenVertexArrays(1,gVao_Cube,0);
	 GLES32.glBindVertexArray(gVao_Cube[0]);

		   GLES32.glGenBuffers(1,gVbo_Cube_Vertex,0);
		   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,gVbo_Cube_Vertex[0]);

		   ByteBuffer byteBuffer_cubeVertices=ByteBuffer.allocateDirect(cubeVertices.length * 4);
		   byteBuffer_cubeVertices.order(ByteOrder.nativeOrder());
		   FloatBuffer cubeVerticesBuffer = byteBuffer_cubeVertices.asFloatBuffer();
		   cubeVerticesBuffer.put(cubeVertices);
		   cubeVerticesBuffer.position(0);

		   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,cubeVertices.length * 4,cubeVerticesBuffer,GLES32.GL_STATIC_DRAW);
		   GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES32.GL_FLOAT,false,0,0);

		   GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		   
		   // Cube_Color Vbo
	   
		   GLES32.glGenBuffers(1,gVbo_Cube_Texture,0);
		   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,gVbo_Cube_Texture[0]);

		   ByteBuffer byteBuffer_cubeTexture=ByteBuffer.allocateDirect(cubeTexcoords.length * 4);
		   byteBuffer_cubeTexture.order(ByteOrder.nativeOrder());
		   FloatBuffer cubetextureBuffer = byteBuffer_cubeTexture.asFloatBuffer();
		   cubetextureBuffer.put(cubeTexcoords);
		   cubetextureBuffer.position(0);

		   GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,cubeTexcoords.length * 4,cubetextureBuffer,GLES32.GL_STATIC_DRAW);
		   GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,2,GLES32.GL_FLOAT,false,0,0);

		   GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);
		   GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);  

	   
	GLES32.glBindVertexArray(0); 

	
//UNBIND FROM CUBE RECORDER

	   GLES32.glEnable(GLES32.GL_DEPTH_TEST);

	   GLES32.glDepthFunc(GLES32.GL_LEQUAL);

	  // GLES32.glEnable(GLES32.GL_CULL_FACE);

	   GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

	   Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	   
	  
	}

	private void resize(int width,int height)
	{
		GLES32.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,((float)width/(float)height),0.1f,100.0f);
	}
  private int loadGLTexture(int imageFileResourceID)
    {
        
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);
        
        int[] texture=new int[1];
      
        GLES32.glGenTextures(1, texture, 0);
 
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,1);
        
        // bind to input texture
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture[0]);

        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MAG_FILTER,GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MIN_FILTER,GLES32.GL_LINEAR_MIPMAP_LINEAR);

        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);
        
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
        
        return(texture[0]);
    }
    
   
	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		
		float modelViewProjectionMatrix[]= new float[16];
		float modelViewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];


		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		Matrix.translateM(modelViewMatrix,0,-2.0f,0.0f,-6.0f);

		Matrix.rotateM(rotationMatrix,0,gfanglePyramid,0.0f,1.0f,0.0f);
	
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES32.glBindVertexArray(gVao_Pyramid[0]);
		
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_Stone[0]);
        GLES32.glUniform1i(texture0_sampler_uniform, 0);


		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,12);

		GLES32.glBindVertexArray(0);
		
		//DRAW NOW CUBE
			Matrix.setIdentityM(modelViewMatrix,0);
			Matrix.setIdentityM(modelViewProjectionMatrix,0);
			Matrix.setIdentityM(rotationMatrix,0);
			
			Matrix.translateM(modelViewMatrix,0,2.0f,0.0f,-6.0f);
			Matrix.scaleM(modelViewMatrix,0,0.75f,0.75f,0.75f);

			
			Matrix.rotateM(rotationMatrix,0,gfangleCube,1.0f,0.0f,0.0f);
			Matrix.rotateM(rotationMatrix,0,gfangleCube,0.0f,1.0f,0.0f);
			Matrix.rotateM(rotationMatrix,0,gfangleCube,0.0f,0.0f,1.0f);	
			
			Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

			GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			GLES32.glBindVertexArray(gVao_Cube[0]);
			
			 GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
			GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_Kundali[0]);
        
			GLES32.glUniform1i(texture0_sampler_uniform, 0);


			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,8,4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,12,4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,16,4);
			GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,20,4);
		
			GLES32.glBindVertexArray(0);


		GLES32.glUseProgram(0);
		
		updateAngle();

		requestRender();

	}
	
	void updateAngle()
	{
		gfanglePyramid = gfanglePyramid + 0.6f;
		gfangleCube = gfangleCube + 1.5f;
		if (gfanglePyramid >= 360.0f)
		{
			gfanglePyramid = 0.0f;
		}
		if (gfangleCube >= 360.0f)
		{
			gfangleCube = 0.0f;
		}
	}



	void uninitialize()
	{
		if(gVao_Pyramid[0]!=0)
		{
		  GLES32.glDeleteVertexArrays(1,gVao_Pyramid,0);
		  gVao_Pyramid[0]=0;
		}

		if(gVbo_Pyramid_Vertex[0]!=0)
		{
			GLES32.glDeleteBuffers(1,gVbo_Pyramid_Vertex,0);
			gVbo_Pyramid_Vertex[0]=0;
		}

		if(shaderProgramObject!=0)
		{
		   if(vertexShaderObject !=0)
		   {
		     GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
			 GLES32.glDeleteShader(vertexShaderObject);
			 vertexShaderObject=0;
		   }

		   if(fragmentShaderObject !=0)
		   {
		      GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
			  GLES32.glDeleteShader(fragmentShaderObject);
			  fragmentShaderObject=0;
		   }
		}

		if(shaderProgramObject!=0)
		{
		   GLES32.glDeleteProgram(shaderProgramObject);
		   shaderProgramObject=0;
		}
	}

}
