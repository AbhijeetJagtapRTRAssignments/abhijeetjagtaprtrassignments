package com.astromedicomp.PP_2DROTATION;

import android.content.Context;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;  // for OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig;  // for EGLConfig needed as param type EGLConfig
import android.opengl.GLES31;  // for OpenGLES 3.1
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;


import java.nio.ByteBuffer;   // nio for non blocking I/O
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

// for matrix math
import android.opengl.Matrix;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] gVao_Triangle = new int[1];
	private int[] gVao_Rectangle = new int[1];
	private int[] gVbo_Triangle_Vertex = new int [1];
	private int[] gVbo_Triangle_Color = new int [1];
	private int[] gVbo_Square_Vertex = new int [1];
	private int[] gVbo_Square_Color = new int [1];
	
	private int mvpUniform;

	private float perspectiveProjectionMatrix[] = new float[16];  
	
	//ANGLE OF ROTATION
	private float gfangleTriangle=0.0f;
	private float gfangleSquare =0.0f;


	public GLESView (Context drawingContext)
	{
	   super(drawingContext);

	   context = drawingContext;

	    setEGLContextClientVersion(3);

	   setRenderer(this);

	   setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	   gestureDetector = new GestureDetector(context,this,null,false);

	   gestureDetector.setOnDoubleTapListener(this);

	}

	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
	  
      String glesVersion =gl.glGetString(GL10.GL_VERSION);
      System.out.println("VDG: OpenGL-ES Version = " + glesVersion);
      
	
	  String glslVersion=gl.glGetString(GLES31.GL_SHADING_LANGUAGE_VERSION);
	  System.out.println("VDG: GLSL Version = " +glslVersion);

	  initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
	   draw();
	} 

	
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
	   int eventAction = e.getAction();
	   
	   if(!gestureDetector.onTouchEvent(e))
	   {
	     super.onTouchEvent(e);
	   } 

	   return(true);
	}

	@Override 
	public boolean onDoubleTap(MotionEvent e)
	{
        return(true);
	}

	@Override 
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
	   return(true);
	}

	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
	   
	}

	@Override 
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override 
	public void onShowPress(MotionEvent e)
	{
	  
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		
        System.out.println("VDG :"+"initialize()");

	// Vertex Shader
		
		// Create Shader Program Object

		vertexShaderObject= GLES31.glCreateShader(GLES31.GL_VERTEX_SHADER);

		// vertex shader source code

		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;"+
			"in vec4 vColor;" +
			"out vec4 OutColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"OutColor=vColor;"+
			"}"
		);

		// provide source code to shader
		GLES31.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		// SHADER COMPILATION AND EROR CHECKING
	   GLES31.glCompileShader(vertexShaderObject);
	   int[] iShaderCompiledStatus = new int[1];
	   int[]iInfoLogLength= new int[1];
	   String szInfoLog=null;

	   GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(vertexShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(vertexShaderObject);
			System.out.println("VDG: Vertex Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	// Fragment Shader
	 
	   // Create Fragment Shader

		fragmentShaderObject= GLES31.glCreateShader(GLES31.GL_FRAGMENT_SHADER);

		// fragment shader source code

		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;"+
			"in vec4 OutColor;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = OutColor;"+
			"}"
		);

		
		// provide source code to shader
		GLES31.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

		//COMPILATION AND ERROR CHECKING
	   GLES31.glCompileShader(fragmentShaderObject);
	   iShaderCompiledStatus[0] = 0;
	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

	   if(iShaderCompiledStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetShaderiv(fragmentShaderObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetShaderInfoLog(fragmentShaderObject);
			System.out.println("VDG: Fragment Shader Compilation Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }


	   // Create Shader Program object

	   shaderProgramObject= GLES31.glCreateProgram();

	   // Attach vertex shader and fragment shader to shader Program

	   GLES31.glAttachShader(shaderProgramObject,vertexShaderObject);

	   GLES31.glAttachShader(shaderProgramObject,fragmentShaderObject);

	   // pre-link binding of shader program object with vertex shader attributes

	   GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	   GLES31.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_COLOR,"vColor");

	   //LINK BOTH SHADERS TO SHADERPROGRAMOBJECT
	   GLES31.glLinkProgram(shaderProgramObject);

	   int[] iShaderProgramLinkStatus = new int[1];

	   iInfoLogLength[0]= 0;
	   szInfoLog=null;

	   GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_LINK_STATUS,iShaderProgramLinkStatus,0);

	   if(iShaderProgramLinkStatus[0]==GLES31.GL_FALSE)
	   {
	      GLES31.glGetProgramiv(shaderProgramObject,GLES31.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		  if (iInfoLogLength[0]>0)
		  {
		    szInfoLog=GLES31.glGetProgramInfoLog(shaderProgramObject);
			System.out.println("VDG: Shader Program Link Log = "+szInfoLog);
			uninitialize();
			System.exit(0);
		  }
	   }

	   // get MVP uniform location
	   mvpUniform= GLES31.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

	   final float triangleVertices[] = new float[]
	   {
	   	   0.0f,1.0f,0.0f, 
		   -1.0f,-1.0f,0.0f, 
		   1.0f,-1.0f,0.0f 
	   };
	   
	   final float triangleColor[] = new float[]
	   {
	   	   1.0f,0.0f,0.0f, 
		   0.0f,1.0f,0.0f, 
		   0.0f,0.0f,1.0f 
	   };
	  
//START TRIANGLE RECORDER
	   GLES31.glGenVertexArrays(1,gVao_Triangle,0);
	   GLES31.glBindVertexArray(gVao_Triangle[0]);
			//BIND WITH VERTEX_BUFFER
			   GLES31.glGenBuffers(1,gVbo_Triangle_Vertex,0);
			   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,gVbo_Triangle_Vertex[0]);

			   ByteBuffer byteBuffer=ByteBuffer.allocateDirect(triangleVertices.length * 4);
			   byteBuffer.order(ByteOrder.nativeOrder());
			   FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
			   verticesBuffer.put(triangleVertices);
			   verticesBuffer.position(0);

			   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,triangleVertices.length * 4,verticesBuffer,GLES31.GL_STATIC_DRAW);
			   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

			   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
			   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
			 
		//BIND WITH COLOR_BUFFER 

			   GLES31.glGenBuffers(1,gVbo_Triangle_Color,0);
			   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,gVbo_Triangle_Color[0]);

			   ByteBuffer colorByteBuffer=ByteBuffer.allocateDirect(triangleColor.length * 4);
			   colorByteBuffer.order(ByteOrder.nativeOrder());
			   FloatBuffer colorBuffer = colorByteBuffer.asFloatBuffer();
			   colorBuffer.put(triangleColor);
			   colorBuffer.position(0);

			   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,triangleColor.length * 4,colorBuffer,GLES31.GL_STATIC_DRAW);
			   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR,3,GLES31.GL_FLOAT,false,0,0);

			   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
			   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);

			   
	   GLES31.glBindVertexArray(0);
	//UNBIND FROM TRIANGLE RECORDER   
	
	//START RECTANGLE RECORDER
	   final float squareVertices[] = new float[]
	   {
	   	    -1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			1.0f, 1.0f, 0.0f
	   };

	
	 GLES31.glGenVertexArrays(1,gVao_Rectangle,0);
	 GLES31.glBindVertexArray(gVao_Rectangle[0]);

		   GLES31.glGenBuffers(1,gVbo_Square_Vertex,0);
		   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,gVbo_Square_Vertex[0]);

		   ByteBuffer byteBuffer_squareVertices=ByteBuffer.allocateDirect(squareVertices.length * 4);
		   byteBuffer_squareVertices.order(ByteOrder.nativeOrder());
		   FloatBuffer squareVerticesBuffer = byteBuffer_squareVertices.asFloatBuffer();
		   squareVerticesBuffer.put(squareVertices);
		   squareVerticesBuffer.position(0);

		   GLES31.glBufferData(GLES31.GL_ARRAY_BUFFER,squareVertices.length * 4,squareVerticesBuffer,GLES31.GL_STATIC_DRAW);
		   GLES31.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,3,GLES31.GL_FLOAT,false,0,0);

		   GLES31.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
		   GLES31.glBindBuffer(GLES31.GL_ARRAY_BUFFER,0);
		   
		   GLES31.glVertexAttrib3f(GLESMacros.VDG_ATTRIBUTE_COLOR,0.258824f, 0.258824f, 0.435294f);
	   
	GLES31.glBindVertexArray(0); 

	
	//UNBIND FROM RECTANGLE RECORDER

	   GLES31.glEnable(GLES31.GL_DEPTH_TEST);

	   GLES31.glDepthFunc(GLES31.GL_LEQUAL);

	  // GLES31.glEnable(GLES31.GL_CULL_FACE);

	   GLES31.glClearColor(0.0f,0.0f,0.0f,1.0f);

	   Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		GLES31.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,((float)width/(float)height),0.1f,100.0f);
	}

	public void draw()
	{
		GLES31.glClear(GLES31.GL_COLOR_BUFFER_BIT|GLES31.GL_DEPTH_BUFFER_BIT);

		GLES31.glUseProgram(shaderProgramObject);

		
		float modelViewProjectionMatrix[]= new float[16];
		float modelViewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];


		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);


		
		Matrix.translateM(modelViewMatrix,0,-2.0f,0.0f,-6.0f);

		Matrix.rotateM(rotationMatrix,0,gfangleTriangle,0.0f,1.0f,0.0f);

		
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES31.glBindVertexArray(gVao_Triangle[0]);

		GLES31.glDrawArrays(GLES31.GL_TRIANGLES,0,3);

		GLES31.glBindVertexArray(0);
		
		//DRAW NOW SQUARE
			Matrix.setIdentityM(modelViewMatrix,0);
			Matrix.setIdentityM(modelViewProjectionMatrix,0);
			Matrix.setIdentityM(rotationMatrix,0);
			
			Matrix.translateM(modelViewMatrix,0,2.0f,0.0f,-6.0f);
			
			Matrix.rotateM(rotationMatrix,0,gfangleSquare,1.0f,0.0f,0.0f);

			
			Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

			GLES31.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			GLES31.glBindVertexArray(gVao_Rectangle[0]);

			GLES31.glDrawArrays(GLES31.GL_TRIANGLE_FAN,0,4);
		
			GLES31.glBindVertexArray(0);


		GLES31.glUseProgram(0);
		
		updateAngle();

		requestRender();

	}
	
	void updateAngle()
	{
		gfangleTriangle = gfangleTriangle + 0.6f;
		gfangleSquare = gfangleSquare + 0.6f;
		if (gfangleTriangle >= 360.0f)
		{
			gfangleTriangle = 0.0f;
		}
		if (gfangleSquare >= 360.0f)
		{
			gfangleSquare = 0.0f;
		}
	}



	void uninitialize()
	{
		if(gVao_Triangle[0]!=0)
		{
		  GLES31.glDeleteVertexArrays(1,gVao_Triangle,0);
		  gVao_Triangle[0]=0;
		}

		if(gVbo_Triangle_Vertex[0]!=0)
		{
			GLES31.glDeleteBuffers(1,gVbo_Triangle_Vertex,0);
			gVbo_Triangle_Vertex[0]=0;
		}

		if(shaderProgramObject!=0)
		{
		   if(vertexShaderObject !=0)
		   {
		     GLES31.glDetachShader(shaderProgramObject,vertexShaderObject);
			 GLES31.glDeleteShader(vertexShaderObject);
			 vertexShaderObject=0;
		   }

		   if(fragmentShaderObject !=0)
		   {
		      GLES31.glDetachShader(shaderProgramObject,fragmentShaderObject);
			  GLES31.glDeleteShader(fragmentShaderObject);
			  fragmentShaderObject=0;
		   }
		}

		if(shaderProgramObject!=0)
		{
		   GLES31.glDeleteProgram(shaderProgramObject);
		   shaderProgramObject=0;
		}
	}

}
