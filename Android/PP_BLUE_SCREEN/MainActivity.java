package com.astromedicomp.PP_BLUE_SCREEN;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;	
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

import android.widget.TextView;	
import android.graphics.Color;
import android.view.Gravity;

public class MainActivity extends Activity 
{
	private GLESView glesView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
	{
		// MyView myView;		
		
        super.onCreate(savedInstanceState);

       // setContentView(R.layout.activity_main);   
	   this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   	   
	   MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	   getWindow().getDecorView().setBackgroundColor(Color.BLACK);
	   
	   glesView = new GLESView(this);
		
	  setContentView(glesView);
	   
    }
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	@Override
	protected void onResume()
	{
		super.onResume();
	}
}
