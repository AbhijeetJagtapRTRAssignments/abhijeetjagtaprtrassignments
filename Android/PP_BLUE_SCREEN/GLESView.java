package com.astromedicomp.PP_BLUE_SCREEN;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.Gravity;

public class GLESView extends GLSurfaceView 
implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	
	private GestureDetector gestureDetector;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG:"+version);
		initialize(gl);
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width,height);
	}	
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("VDG:"+"DOUBLE TAP");
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//We are not writing any code here because already written
		return true;
	}
	
	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("SINGLE_TAP");
		return true;
	}
	
	@Override 
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public void onLongPress(MotionEvent e)
	{
		System.out.println("LONG PRESS");
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onFling(MotionEvent q1, MotionEvent e2, float velocityX, float velocityY )
	{
		return true;
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("SCROLL");
		return true;
	}
	
	private void initialize(GL10 gl)
	{
		GLES32.glClearColor(0.0f,0.0f,1.0f,0.0f);
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0,0,width,height);
	}
	
	private void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		//GLES32.glClear(GLES32.GL_CLEAR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
	}
}