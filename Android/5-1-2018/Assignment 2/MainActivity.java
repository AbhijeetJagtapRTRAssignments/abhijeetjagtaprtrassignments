package com.astromedicomp.Hello_world_multifile;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window;	
import android.view.WindowManager;
import android.content.pm.ActivityInfo;

import android.widget.TextView;	
import android.graphics.Color;
import android.view.Gravity;

public class MainActivity extends Activity 
{
	private MyView myView;	// FOR OpenGL we have to declare variable Of class  MyView as CLASS_VARIABLE instead of LOCAL_VARIABLE
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
	{
		// MyView myView;		
		
        super.onCreate(savedInstanceState);

       // setContentView(R.layout.activity_main);   
	   this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	   getWindow().getDecorView().setBackgroundColor(Color.BLACK);
	   
	   myView = new MyView(this);
		
	  setContentView(myView);
	   
    }
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	@Override
	protected void onResume()
	{
		super.onResume();
	}
}
