package com.astromedicomp.Hello_world_multifile;

import android.widget.TextView;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;

public class MyView extends TextView
{
	MyView(Context context)
	{
		super(context);
		
		/*setTextColor(Color.GREEN);
		setText("Hello World Multi File");
		setTextSize(60);
		setGravity(Gravity.CENTER);*/
		
		setText("Hello World Multi File");
		setTextSize(60);
		setTextColor(Color.GREEN);
		setGravity(Gravity.CENTER);
	}
	
}