package com.astromedicomp.event_handling;

import android.content.Context;
import android.graphics.Color;
import android.widget.TextView;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.Gravity;

public class MyView extends TextView 
implements OnGestureListener, OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	MyView(Context context)
	{
		super(context);
		
		setText("Hello World Multi File");
		setTextSize(60);
		setTextColor(Color.GREEN);
		setGravity(Gravity.CENTER);
		
		gestureDetector = new GestureDetector(context,this,null,false);
		
		gestureDetector.setOnDoubleTapListener(this);
		
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return true;
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		setText("DOUBLE TAP");
		return true;
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		//We are not writing any code here because already written
		return true;
	}
	
	@Override 
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		setText("SINGLE TAP");
		return true;
	}
	
	@Override 
	public boolean onDown(MotionEvent e)
	{
		return true;
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return true;
	}
	
	@Override 
	public void onLongPress(MotionEvent e)
	{
		setText("LONG PRESS");
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	
	@Override
	public boolean onFling(MotionEvent q1, MotionEvent e2, float velocityX, float velocityY )
	{
		return true;
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		setText("SCROLL");
		return true;
	}
}