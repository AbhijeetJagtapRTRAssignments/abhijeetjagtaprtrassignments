#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;

enum
{
   VDG_ATTRIBUTE_POSITION=0,
   VDG_ATTRIBUTE_COLOR,
   VDG_ATTRIBUTE_NORMAL,
   VDG_ATTRIBUTE_TEXTURE
};

// THIS IS THE FUNCTION FOR GETTING THE FRAME REFRESH RATE...WHICH WE ARE GOING TO USE TO SHOW 
//THE ANIMATION THAT IS LIKE glSwapBuffer
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *fp=NULL;

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
    
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)
:(NSNotification *)aNotification
{
    
    // WHILE CREATING THE LOG FILE WE HAVE TO DO ALL BELOW STEPS 
	// IOTHERWISE LOG FILE WILL GETS CREATED INSIDE Mac directory 
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    fp=fopen(pszLogFileNameWithPath,"w");
    if(fp==NULL)
    {
        printf("\nUnable to create file...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(fp, "\n FILE CREATEED SUCCESSFULLY \n");
    

    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"OPENGL_BLUE_SCREEN"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    
    fprintf(fp, "\n EXITING THE PROGRAM \n");
    
    if(fp)
    {
        fclose(fp);
        fp=NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    
    [NSApp terminate:self];
}

- (void)dealloc
{
    
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
	
	//OPENGL VARIABLES
	
	GLuint gVao_Triangle;
	GLuint gVao_Rectangle;

	GLuint gVbo_Position;
	GLuint gVbo_Color_Triangle;
	GLuint gVbo_Color_Rectangle;

	GLuint gMVPUniform;

	mat4 gPerspectiveProjectionMatrix;
	mat4 gRotationMatrix;
	mat4 ScaleMatrix;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	float gfUpdateAngle = 0.0f;
}

-(id)initWithFrame:(NSRect)frame;
{
    
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
			//HERE we cannot use above 4.2 as Apple supports OpenGL upto 4.2
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0}; // THIS 0 is like NULL TERMINATED...MUST
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(fp, "\n No Valid OpenGL Pixel Format Is Available.\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; 
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

//WRITE ALL initialise() code inside this function
-(void)prepareOpenGL
{
    
 
    fprintf(fp, "\nOPENGL VERSION : %s\n",glGetString(GL_VERSION));
    fprintf(fp, "SHADING LANGUAGE VERSION : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   /*START THE OPENGL CODE*/
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 outColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outColor=vColor;"\
		"}";
		
		

	glShaderSource(gVertexShaderObject,1,(const GLchar **)&VertexShaderCode,NULL);

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;
	
	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\nVERTEX SHADER COMPILATION LOG:%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
				exit(0);
			}
		}
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 outColor;"\
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = outColor;" \
		"}";

		
	glShaderSource(gFragmentShaderObject,1,(const GLchar **)&FragmentShaderSourceCode,NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\n FRAGMENT SHADER COMPILATION LOG:%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
				exit(0);
		    }
	     }
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();
	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");

	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\n LINK TIME ERROR LOG: \n%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                [NSApp terminate:self];
				exit(0);
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
//START THE BINDING OF THE POSITION AND COLORE VBOs
const GLfloat triangleVertices[] =
	{
		/*PHASE ONE*/
			0.0f, 1.0,0.0f,
			-1.0,- 1.0,1.0f,
			1.0,-1.0,1.0f,

		/*PHASE TWO*/
			0.0f, 1.0,0.0f,
			1.0,-1.0,1.0f,
			1.0,-1.0,-1.0f,

		/*PHASE THREE*/
			0.0f, 1.0,0.0f,
			1.0,-1.0,-1.0f,
			-1.0,-1.0,-1.0f,

		/*PHASE FOUR*/
			0.0f, 1.0,0.0f,
			-1.0,-1.0,-1.0f,
			-1.0,-1.0,1.0f
	};
	const GLfloat triangleColor[] =
	{
	  /*PHASE ONE*/
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,

		/*PHASE TWO*/
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,

		/*PHASE THREE*/
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,

		/*PHASE FOUR*/
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f
	};

	//START THE RECORDER	---SAME AS glBegin()
	glGenVertexArrays(1, &gVao_Triangle);
	glBindVertexArray(gVao_Triangle);

	//CREATE THE OUR SLOT OF VERTEX AND COLOR MEMORY INSIDE GL_ARRAY_BUFFER WHICH IS INSIDE FRAMR BUFFER
	glGenBuffers(1, &gVbo_Position);


	//BIND TO TH VERTEX_BUFFER AND TRANSFER DATA TO IT AT RUNTIME
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM VERTEX BUFFER

	//CREATE AND BIND TO THE COLOR BUFFER
	glGenBuffers(1, &gVbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color_Triangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM COLOR BUFFER

	//END OF RECORDER ----SAME AS glEnd()
	glBindVertexArray(0);



	const GLfloat rectangleVertices[] =
	{

		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

	//BOTTOM FACE
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

	//FRONT FACE
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

	//BACK FACE
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

	//RIGHT FACE
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

	//LEFT FACE
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat rectangleColor[] =
	{
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

	//BOTTOM FACE
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

	//FRONT FACE
	    0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

	//BACK FACE
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 1.0f,

	//RIGHT FACE
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f,

	//LEFT FACE
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 0.0f
	};

	//START THE RECORDER	---SAME AS glBegin() for RECTANGLE
	glGenVertexArrays(1, &gVao_Rectangle);
	glBindVertexArray(gVao_Rectangle);

	//CREATE THE OUR SLOT OF MEMORY INSIDE GL_ARRAY_BUFFER WHICH IS INSIDE FRAMR BUFFER
	glGenBuffers(1, &gVbo_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM VERTEX BUFFER

	//CREATE AND BIND TO THE COLOR BUFFER
	glGenBuffers(1, &gVbo_Color_Rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color_Rectangle);

	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleColor), rectangleColor, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM COLOR BUFFER

	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()
   
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
  
	glClearColor(0.0f,0.0f,0.0f, 1.0);
	gPerspectiveProjectionMatrix = mat4::identity();
	mat4 gRotationMatrix = mat4::identity();

   /*END THE OPENGL CODE*/
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}

//WRITE ALL resize() code inside this function
-(void)reshape
{
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
        height=1;
    
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
		if(width <= height)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f);
	}
	else if(height <= width)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f); 
	}
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    
    [self drawView];
}

//THIS IS OUR display() function

- (void)drawView
{
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
//START OF ONLINE RENDERING
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	vmath::mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = vmath::translate(-2.0f, 0.0f, -6.0f);
	gRotationMatrix = vmath::rotate(gfUpdateAngle, 0.0f, 1.0f, 0.0f);

	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	modelViewProjectionMatrix = modelViewProjectionMatrix * gRotationMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);


	glBindVertexArray(gVao_Triangle);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	//NOW DRAW RECTANGLE BEFORE THAT MAKE IDENTITY TO modelViewMatrix and modelViewProjectionMatrix
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	gRotationMatrix = vmath::mat4::identity();

	gRotationMatrix = vmath::rotate(gfUpdateAngle, gfUpdateAngle, gfUpdateAngle);
	ScaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

	modelViewMatrix = vmath::translate(2.0f, 0.0f, -6.0f);
	
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	modelViewProjectionMatrix = modelViewProjectionMatrix * gRotationMatrix;

	modelViewProjectionMatrix = modelViewProjectionMatrix * ScaleMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_Rectangle);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

	glUseProgram(0);

//NOW HERE UPDATE THE ANGLE OF ROTATION

  gfUpdateAngle = gfUpdateAngle + 0.01f;
  if(gfUpdateAngle >= 360.0f)
  {
	gfUpdateAngle = 0.0f;
  }

//END OF ONLINE RENDERING

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    
    [[self window]makeFirstResponder:self];
    return(YES);
}

//WM_KEYDOWN FROM OUR WndPorc
-(void)keyDown:(NSEvent *)theEvent
{
    
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: 
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
  
}

- (void) dealloc
{
	
	if(gVbo_Color_Rectangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Rectangle);
		gVbo_Color_Rectangle = 0;
	}
	if(gVbo_Color_Triangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Triangle);
		gVbo_Color_Triangle = 0;
	}
	if(gVbo_Position)
	{
		glDeleteVertexArrays(1,&gVbo_Position);
		gVbo_Position = 0;
	}
	if(gVao_Triangle)
	{
		glDeleteVertexArrays(1,&gVao_Triangle);
		gVao_Triangle = 0;
	}
	if(gVao_Rectangle)
	{
		glDeleteVertexArrays(1,&gVao_Rectangle);
		gVao_Rectangle = 0;
	}

	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject =0;

    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
