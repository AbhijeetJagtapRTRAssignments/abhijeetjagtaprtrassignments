#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;

enum
{
   VDG_ATTRIBUTE_POSITION=0,
   VDG_ATTRIBUTE_COLOR,
   VDG_ATTRIBUTE_NORMAL,
   VDG_ATTRIBUTE_TEXTURE
};

// THIS IS THE FUNCTION FOR GETTING THE FRAME REFRESH RATE...WHICH WE ARE GOING TO USE TO SHOW 
//THE ANIMATION THAT IS LIKE glSwapBuffer
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *fp=NULL;

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
    
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)
:(NSNotification *)aNotification
{
    
    // WHILE CREATING THE LOG FILE WE HAVE TO DO ALL BELOW STEPS 
	// IOTHERWISE LOG FILE WILL GETS CREATED INSIDE Mac directory 
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    fp=fopen(pszLogFileNameWithPath,"w");
    if(fp==NULL)
    {
        printf("\nUnable to create file...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(fp, "\n FILE CREATEED SUCCESSFULLY \n");
    

    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"OPENGL_BLUE_SCREEN"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    
    fprintf(fp, "\n EXITING THE PROGRAM \n");
    
    if(fp)
    {
        fclose(fp);
        fp=NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    
    [NSApp terminate:self];
}

- (void)dealloc
{
    
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
   CVDisplayLinkRef displayLink;

   //OPENGL VARIABLES
   
		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;

		GLuint vao_Cube;
		
		GLuint vbo_Cube_Position;
		GLuint vbo_Cube_Normal;

		GLuint modelViewMatrixUniform, projectionMatrixUniform;


		GLuint LdUniform,KdUniform,LightPositionUniform;
		GLuint LKeyPressedUniform;

		vmath::mat4 perspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
    
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
			//HERE we cannot use above 4.2 as Apple supports OpenGL upto 4.2
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0}; // THIS 0 is like NULL TERMINATED...MUST
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(fp, "\n No Valid OpenGL Pixel Format Is Available.\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; 
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
  fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
			"#version 410 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;"\
			"uniform mat4 u_model_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;"\
			"uniform vec3 u_Ld;"\
			"uniform vec3 u_Kd;"\
			"uniform vec4 u_light_position;"\
			"out vec3 diffuse_light ;" \
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)"\
			"{"\
			"vec4 eyeCoordinates = u_model_view_matrix * vPosition  ;"\
			"vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);"\
			"vec3 s = normalize(vec3(u_light_position - eyeCoordinates ));"\
			"diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);"\
			"}"\
			"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition ;"\
			"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	// Fragment Shader

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
			"#version 410"\
			"\n" \
			"in vec3 diffuse_light;" \
			"out vec4 FragColor;" \
			"uniform int u_LKeyPressed;" \
			"void main(void)" \
			"{" \
			"vec4 color;"\
			"if (u_LKeyPressed == 1)"\
			"{"\
			"color = vec4(diffuse_light,1.0);"\
			"}"\
			"else"\
			"{"\
			"color = vec4(1.0,1.0,1.0,1.0);"\
			"}"\
			"FragColor = color ;" \
			"}" ;

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	shaderProgramObject = glCreateProgram();
 
	glAttachShader (shaderProgramObject,vertexShaderObject);
	glAttachShader (shaderProgramObject,fragmentShaderObject);
	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");
	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");


	glLinkProgram(shaderProgramObject);

	GLint iShaderProgramLinkStatus=0;

	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	 modelViewMatrixUniform = glGetUniformLocation(shaderProgramObject,"u_model_view_matrix");

     projectionMatrixUniform = glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

	 LKeyPressedUniform = glGetUniformLocation(shaderProgramObject,"u_LKeyPressed");

    LdUniform = glGetUniformLocation(shaderProgramObject,"u_Ld");

    KdUniform = glGetUniformLocation(shaderProgramObject,"u_Kd");

    LightPositionUniform = glGetUniformLocation(shaderProgramObject,"u_light_position");


	const GLfloat cubeVertices []=
    {
	1.0f, 1.0f, -1.0f, 
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f, 
	1.0f, 1.0f, 1.0f, 

	1.0f, -1.0f, -1.0f, 
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f, 
	1.0f, -1.0f, 1.0f, 

	1.0f, 1.0f, 1.0f,  
	-1.0f, 1.0f, 1.0f, 
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f, 


	1.0f, 1.0f, -1.0f,  
	-1.0f, 1.0f, -1.0f, 
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f, 

	1.0f, 1.0f, -1.0f,  
	1.0f, 1.0f, 1.0f, //
	1.0f, -1.0f, 1.0f,  
	1.0f, -1.0f, -1.0f, 

	-1.0f, 1.0f, 1.0f,  
	-1.0f, 1.0f, -1.0f, 
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f 

    };


    glGenVertexArrays(1,&vao_Cube);
    glBindVertexArray(vao_Cube);

    glGenBuffers(1,& vbo_Cube_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeVertices),cubeVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); 
      
    const GLfloat cubeNormals []=
    {
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f,
		-1.0f, 0.0f, 1.0f
    };

	
    glGenBuffers(1,& vbo_Cube_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Cube_Normal);
    glBufferData(GL_ARRAY_BUFFER,sizeof(cubeNormals),cubeNormals,GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER,0); 
    glBindVertexArray(0); 

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);


    glClearColor(0.0f,0.0f,0.0f,0.0f); 


	perspectiveProjectionMatrix = vmath::mat4::identity();

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); 
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); 
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); 
}

//WRITE ALL resize() code inside this function
-(void)reshape
{
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
        height=1;
    
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
		if(width <= height)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f);
	}
	else if(height <= width)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f); 
	}
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    
    [self drawView];
}

//THIS IS OUR display() function

- (void)drawView
{
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
//START OF ONLINE RENDERING
glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);

	if (isLightEnabled == true)
    {
        glUniform1i(LKeyPressedUniform,1);

        glUniform3f(LdUniform,1.0f,1.0f,1.0f);
        glUniform3f(KdUniform,0.5f,0.5f,0.5f);

        float lightPosition[]={0.0f,0.0f,2.0f,1.0f};
        glUniform4fv(LightPositionUniform,1,(GLfloat *)lightPosition);
    }
    
    else
    {
        glUniform1i(LKeyPressedUniform,0);
    }

	vmath::mat4 modelViewMatrix = vmath::mat4::identity(); 
	vmath::mat4 modelMatrix = vmath::mat4::identity(); 
	vmath::mat4 rotationMatrix = vmath::mat4::identity(); 
	modelMatrix = vmath::translate(0.0f,0.0f,-6.0f);
	rotationMatrix = vmath::rotate(angleCube,angleCube,angleCube);
	modelViewMatrix = modelMatrix * rotationMatrix ;

	glUniformMatrix4fv(modelViewMatrixUniform,1,GL_FALSE,modelViewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);


	glBindVertexArray(vao_Cube);


	glDrawArrays(GL_TRIANGLE_FAN,0,4); 
    glDrawArrays(GL_TRIANGLE_FAN,4,4); 
    glDrawArrays(GL_TRIANGLE_FAN,8,4); 
    glDrawArrays(GL_TRIANGLE_FAN,12,4); 
    glDrawArrays(GL_TRIANGLE_FAN,16,4); 
    glDrawArrays(GL_TRIANGLE_FAN,20,4); 

	glBindVertexArray(0);

	glUseProgram(0);

//NOW HERE UPDATE THE ANGLE OF ROTATION

  gfUpdateAngle = gfUpdateAngle + 0.01f;
  if(gfUpdateAngle >= 360.0f)
  {
	gfUpdateAngle = 0.0f;
  }

//END OF ONLINE RENDERING

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    
    [[self window]makeFirstResponder:self];
    return(YES);
}

//WM_KEYDOWN FROM OUR WndPorc
-(void)keyDown:(NSEvent *)theEvent
{
    
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: 
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
  
}

- (void) dealloc
{
	
	if(gVbo_Color_Triangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Triangle);
		gVbo_Color_Triangle = 0;
	}
	if(gVbo_Color_Rectangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Rectangle);
		gVbo_Color_Rectangle = 0;
	}
	if(gVbo_Position)
	{
		glDeleteVertexArrays(1,&gVbo_Position);
		gVbo_Position = 0;
	}
	if(gVao_Triangle)
	{
		glDeleteVertexArrays(1,&gVao_Triangle);
		gVao_Triangle = 0;
	}
	if(gVao_Rectangle)
	{
		glDeleteVertexArrays(1,&gVao_Rectangle);
		gVao_Rectangle = 0;
	}

	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject =0;
	
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
