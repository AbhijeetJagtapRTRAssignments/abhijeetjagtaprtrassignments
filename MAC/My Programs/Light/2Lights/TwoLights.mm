#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
using namespace vmath;

enum
{
   VDG_ATTRIBUTE_POSITION=0,
   VDG_ATTRIBUTE_COLOR,
   VDG_ATTRIBUTE_NORMAL,
   VDG_ATTRIBUTE_TEXTURE
};

// THIS IS THE FUNCTION FOR GETTING THE FRAME REFRESH RATE...WHICH WE ARE GOING TO USE TO SHOW 
//THE ANIMATION THAT IS LIKE glSwapBuffer
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *fp=NULL;

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
    
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)
:(NSNotification *)aNotification
{
    
    // WHILE CREATING THE LOG FILE WE HAVE TO DO ALL BELOW STEPS 
	// IOTHERWISE LOG FILE WILL GETS CREATED INSIDE Mac directory 
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    fp=fopen(pszLogFileNameWithPath,"w");
    if(fp==NULL)
    {
        printf("\nUnable to create file...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(fp, "\n FILE CREATEED SUCCESSFULLY \n");
    

    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"OPENGL_BLUE_SCREEN"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    
    fprintf(fp, "\n EXITING THE PROGRAM \n");
    
    if(fp)
    {
        fclose(fp);
        fp=NULL;
    }
}

- (void)windowWillClose:(NSNotification *)notification
{
    
    [NSApp terminate:self];
}

- (void)dealloc
{
    
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
@private
   CVDisplayLinkRef displayLink;

   //OPENGL VARIABLES
		GLuint vertexShaderObject;
		GLuint fragmentShaderObject;
		GLuint shaderProgramObject;

		GLuint vao_Pyramid;
		
		GLuint vbo_Pyramid_Position;
		GLuint vbo_Pyramid_Normal;

	    GLuint ModelMatrixUniform,ViewMatrixUniform,ProjectionMatrixUniform;
		GLuint LaUniform,LdUniform, LsUniform,LightPositionUniform;
		GLuint KaUniform,KdUniform, KsUniform, MaterialShininessUniform;

		

		vmath::mat4 perspectiveProjectionMatrix;


}

-(id)initWithFrame:(NSRect)frame;
{
    
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // Must specify the 4.1 Core Profile to use OpenGL 4.1
			//HERE we cannot use above 4.2 as Apple supports OpenGL upto 4.2
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0}; // THIS 0 is like NULL TERMINATED...MUST
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs] autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(fp, "\n No Valid OpenGL Pixel Format Is Available.\n");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:glContext]; 
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    
    [self drawView];
    
    [pool release];
    return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
  fprintf(gpFile,"OpenGL Version : %s \n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext]; 
     
    GLint swapInt = 1;

    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // CP for Context Parameter 


	// Vertex Shader 

	vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
			"#version 410 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;" \
			"uniform mat4 u_model_matrix ;" \
			"uniform mat4 u_view_matrix ;" \
			"uniform mat4 u_projection_matrix ;" \
			"uniform vec3 u_La[2] ;" \
			"uniform vec3 u_Ld[2] ;" \
			"uniform vec3 u_Ls[2] ;" \
			"uniform vec4 u_light_position[2] ;" \
			"uniform vec3 u_Ka ;" \
			"uniform vec3 u_Kd ;" \
			"uniform vec3 u_Ks ;" \
			"uniform float u_material_shininess ;" \
			"out vec3 phong_ads_color;" \
			"void main(void)" \
			"{" \
			/*"phong_ads_color= vec3(1.0,1.0,1.0);"\*/
			"vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
			"vec3 transformed_normals = normalize(mat3 (u_view_matrix * u_model_matrix) * vNormal );"\
			"vec3 light_direction;"\
			"float tn_dot_ld;"\
			"vec3 ambient[2];"\
			"vec3 diffuse[2];"\
			"vec3 reflection_vector;"\
			"vec3 viewer_vector;"\
			"vec3 specular[2];"\
			"for(int i=0;i<2;i++)"\
			"{"\
			"light_direction = normalize (vec3 (u_light_position[i]) - eye_coordinates.xyz);"\
			"tn_dot_ld = max(dot (transformed_normals ,light_direction), 0.0);"\
			"ambient[i] = u_La[i] * u_Ka;"\
			"diffuse[i] = u_Ld[i] * u_Kd * tn_dot_ld ;"\
			"reflection_vector = reflect(-light_direction,transformed_normals);"\
			"viewer_vector = normalize(-eye_coordinates.xyz);"\
			"specular[i] = u_Ls[i] * u_Ks * pow (max(dot (reflection_vector,viewer_vector),0.0),u_material_shininess);"\
			"}"\
			/*"for(int j=0;j<2;j++)"\
			"{"\*/
			"phong_ads_color = ambient[0] + ambient[1] + diffuse[0] + diffuse[1] + specular[0] + specular[1] ;"\
			/*"}"\*/
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
			"}";

	glShaderSource(vertexShaderObject,1,(const GLchar **) &vertexShaderSourceCode,NULL);

	glCompileShader(vertexShaderObject);
	
	GLint iInfoLogLength =0;
	GLint iShaderCompiledStatus =0;
	char *szInfoLog = NULL;

	glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Vertex Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
			"#version 410"\
			"\n" \
			"in vec3 phong_ads_color;" \
			"out vec4 FragColor;" \
			"void main(void)" \
			"{" \
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}";
			

	glShaderSource(fragmentShaderObject,1, (const GLchar **)&fragmentShaderSourceCode,NULL);

	glCompileShader(fragmentShaderObject);

	glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Fragment Shader Compilation Log :%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

	shaderProgramObject = glCreateProgram();

	glAttachShader(shaderProgramObject,vertexShaderObject);
	glAttachShader(shaderProgramObject,fragmentShaderObject);
	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");
	glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");
	glLinkProgram(shaderProgramObject);
	GLint iShaderProgramLinkStatus=0;
	glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

		if(iInfoLogLength>0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);

			if(szInfoLog !=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
				fprintf(gpFile,"Shader Program Link Log:%s\n",szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}

			ModelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
            ViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
            ProjectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
            LaUniform = glGetUniformLocation(shaderProgramObject, "u_La");
            LdUniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
            LsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls");  
            LightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");  
            KaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka");    
            KdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd");   
            KsUniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
            MaterialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");


	const GLfloat pyramidVertices []=
    {
			0.0f, 1.0f, 0.0f,   
			-1.0f, -1.0f, 1.0f, 
			 1.0f, -1.0f, 1.0f, 

			0.0f, 1.0f, 0.0f,   
			1.0f, -1.0f, 1.0f,  
			1.0f, -1.0f, -1.0f, 

			0.0f, 1.0f, 0.0f, 
			1.0f, -1.0f, -1.0f, 
			-1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f, 
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f, 
    };


    glGenVertexArrays(1,&vao_Pyramid);
    glBindVertexArray(vao_Pyramid);

    glGenBuffers(1,& vbo_Pyramid_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Position);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidVertices),pyramidVertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); 
      
    const GLfloat pyramidNormals []=
    {
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f
    };

    glGenBuffers(1,& vbo_Pyramid_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Pyramid_Normal);
    glBufferData(GL_ARRAY_BUFFER,sizeof(pyramidNormals),pyramidNormals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0); 
    glBindVertexArray(0); 

    glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);
    glClearColor(0.0f,0.0f,0.0f,0.0f); 

	perspectiveProjectionMatrix = vmath::mat4::identity();


    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink); 
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self); 
    CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj]; 
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink); 
}

//WRITE ALL resize() code inside this function
-(void)reshape
{
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if(height==0)
        height=1;
    
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
		if(width <= height)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f);
	}
	else if(height <= width)
	{
		gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f); 
	}
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    
    [self drawView];
}

//THIS IS OUR display() function

- (void)drawView
{
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
//START OF ONLINE RENDERING
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderProgramObject);
	
	glUniform3fv(LaUniform,2,light_ambient);
    glUniform3fv(LdUniform, 2,light_diffuse);
    glUniform3fv(LsUniform, 2, light_specular);
    glUniform4fv(LightPositionUniform, 2,light_position);


	vmath::mat4 modelMatrix = vmath::mat4::identity(); 
	vmath::mat4 viewMatrix = vmath::mat4::identity(); 
	vmath::mat4 rotationMatrix = vmath::mat4::identity(); 
	modelMatrix = vmath::translate(0.0f,0.0f,-6.0f);
	rotationMatrix = vmath::rotate(anglePyramid,0.0f, 1.0f, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix ;
	glUniformMatrix4fv(ModelMatrixUniform,1,GL_FALSE,modelMatrix);
	glUniformMatrix4fv(ViewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(ProjectionMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);
	glBindVertexArray(vao_Pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	glUseProgram(0);


//NOW HERE UPDATE THE ANGLE OF ROTATION

  gfUpdateAngle = gfUpdateAngle + 0.01f;
  if(gfUpdateAngle >= 360.0f)
  {
	gfUpdateAngle = 0.0f;
  }

//END OF ONLINE RENDERING

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    
    [[self window]makeFirstResponder:self];
    return(YES);
}

//WM_KEYDOWN FROM OUR WndPorc
-(void)keyDown:(NSEvent *)theEvent
{
    
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: 
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
  
}

- (void) dealloc
{
	
	if(gVbo_Color_Triangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Triangle);
		gVbo_Color_Triangle = 0;
	}
	if(gVbo_Color_Rectangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Rectangle);
		gVbo_Color_Rectangle = 0;
	}
	if(gVbo_Position)
	{
		glDeleteVertexArrays(1,&gVbo_Position);
		gVbo_Position = 0;
	}
	if(gVao_Triangle)
	{
		glDeleteVertexArrays(1,&gVao_Triangle);
		gVao_Triangle = 0;
	}
	if(gVao_Rectangle)
	{
		glDeleteVertexArrays(1,&gVao_Rectangle);
		gVao_Rectangle = 0;
	}

	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject =0;
	
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);

    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                               CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
