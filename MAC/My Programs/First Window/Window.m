#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

//INTERFACE DECLARATION

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface MyView : NSView
@end

int main (int argc, const char argv[])
{
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication SharedApplication];
	
	[NSApp setDelegate:[[AppDelegate alloc]init]];
	
	[NSApp run];
	
	[pPool release];
	
	return(0);
}

//INTERFACE IMPLETATIONS

@implementation AppDelegate
{
  @private
		NSWindow *window;
		MyView *view;
}
		
  - (void)applicationDidFinishLaunching : (NSNotification *)aNotification
  {
		NSRect win_rect;
		win_rect = NSMakeRect(0.0, 0.0, 00.0, 600.0);
		
		//CREATE WINDOW
		window = [[NSWindow alloc] initWithContentRect: win_rect 
									styleMask:NSWindowStyleMaskTitled |
									NSWindowStyleMaskClosable |
									NSWindowStyleMaskMiniaturizable |
									NSWindowStyleMaskResizable 
									backing:NSBackingStoreBuffered 
									defer:NO];
									
		[window setTitle:@"macOS window"];
		[window center];
		
		view=[[MyView alloc]initWithFrame:win_rect];
		
		[window setContentView:view];
		[window setDelegate:self];
		[window makeKeyAndOrderFront:self];
  }
  
  - (void)applicationWillTerminate:(NSNotification *)notification 
  {
	
  }
  
  - (void)windowWillCLose:(NSNotification *)notification 
  {
	[NSApp terminate:self];
  }
  
  - (void)dealloc 
  {	
	[view release];
	
	[window release];
	
	[super dealloc];
  }
@end

@implementation MyView 
{
	NSString *centralText;
}

-(id)initWithFrame:(NSRect)frame;
{
	
}
@end