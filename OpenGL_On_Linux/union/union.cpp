#include<stdio.h>

int main()
{
	union Variable
	{
		char ch;
		short sh;
		int no;
	}obj;
	
	int SizeOfVar = 2;
	
	if(SizeOfVar == 1)
	{
		obj.ch ='c';
		printf("\n Value of char from union :%c\n",obj.ch);
	}
	if(SizeOfVar == 2)
	{
		obj.sh = 2;
		printf("\n Value of short from union :%d\n",obj.sh);		
	}
	
	printf("\n Sizeof Union : %d\n",sizeof(obj));
	printf("\n Size of short: %d \n",sizeof(obj.sh));


return 0;
}
