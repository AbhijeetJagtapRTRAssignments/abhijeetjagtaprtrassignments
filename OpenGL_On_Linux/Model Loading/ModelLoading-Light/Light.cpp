#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<vector>

using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;


FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;
float gfRotationAngle = 0.0f;


float gfUpdateAngle = 0.0f;

/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);

/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int,int);
void rotate(void);

/*LIGHT REGARDING VARIABLES*/

GLfloat light_ambient[]  = {0.5f,0.5f,0.5f,1.0f};
GLfloat light_diffused[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat light_specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat light_position[] = {0.0f,0.0f,01.0f,0.0f};
bool gbLighting = false;


/*MODEL LOADING REGARDING VARIABLES*/
#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3		//Minimum number of entries in face data

//Vector of vector of floats to hold vertex data
vector<vector<float> > g_vertices;

//Vector of vector of floats to hold texture data
std::vector< std::vector<float> > g_texture;

//Vector of vector of floats to hold Normal data
std::vector<std::vector<float> > g_normals;

//Vector of vector of int to hold face data
std::vector<std::vector<int> > g_face_tri,g_face_texture,g_face_normals;

FILE *g_fp_MeshFile =NULL;

char line[BUFFER_SIZE];

float g_fl_angle_y =0.0f;

void LoadMeshFile(char *szFilename);
/*-----------------------------------*/


int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("ErroLog.txt" , "w");
 if(fp == NULL)
 {
   printf(" \n Error while creating file \n ");
   exit(0);
 }

 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
			giWindowWidth  = event.xconfigure.width;
			giWindowHeight = event.xconfigure.height;	
			resize(giWindowHeight,giWindowWidth);
			//fprintf(fp,"\n Inside case WM_SIZE i.e. ConfigureNotify\n giWindowWidth <%d> \ngiWindowHeight <%d> \n",giWindowWidth,giWindowHeight);	
			break;
		case Expose:
		   //WM_PAINT	
			break;
		case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
		switch(keysym)
		{
			case XK_Escape:
				fprintf(fp,"\n Inside XK_ESCAPE\n");
				bDone = true;
				//uninitialize();		//IN GEME LOOP HERE ONLY bDone = FALSE;
				//exit(0);
			break;
			case XK_F:
			case XK_f:
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
					
					ToggleFullScreen();
					gbFullScreen = true;		
				}
				else
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
				
			break;
			
			case XK_L:
			case XK_l:
    			if(gbLighting == false)
    			{
    			    glEnable(GL_LIGHT0);
    			    glEnable(GL_LIGHTING);
    			    gbLighting = true;
    			}
			break;

			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				   //WM_LBUTTONDOWN
					fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
					break;
				case 2:
				  //WM_MBUTTONDOWN
					fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
					break;
				case 3:
				  //WM_RBUTTONDOWN
					fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
					break;
			} 
			break;			
		}
		break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }

   display();
  
 }

return 0;
	
}

void CreateWindow()
{
  XSetWindowAttributes winAttribs;
  int DefaultScreen;
  int DefaultDepth;
  int styleMask;

  static  int frameBufferAttributes[]=
  {
	GLX_RGBA,
	GLX_RED_SIZE,8,
	GLX_GREEN_SIZE,8,
	GLX_BLUE_SIZE,8,
	GLX_ALPHA_SIZE, 1,
	GLX_DEPTH_SIZE,24,
	GLX_DOUBLEBUFFER,True,
	None
		
  };
 
 /*STEP 1*/
  gpDisplay = XOpenDisplay(NULL);
  if(gpDisplay == NULL)
  {
	printf("\nUnable to open Display!!!\n");
	uninitialize();
	exit(1);
  }

  /*STEP 2*/
  DefaultScreen = XDefaultScreen(gpDisplay);
  
  
  /*STEP 3*/
  DefaultDepth = DefaultDepth(gpDisplay,DefaultScreen);
  
  /*STEP 4*/
  gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while allocating memory to XVisualInfo.\Exiting\n");
	uninitialize();
 	exit(1);
  }  

  
  /*STEP 5*/
  /*XMatchVisualInfo(gpDisplay,DefaultScreen,DefaultDepth,TrueColor,gpXVisualInfo);
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while Getting Visual\n Exiting\n");
	uninitialize();
	exit(1);
  }*/
  gpXVisualInfo = glXChooseVisual(gpDisplay,DefaultScreen,frameBufferAttributes);
 
  /*SETTING WinAttributes--->Similar like filling WNDCLASSEX struct*/
  winAttribs.border_pixel = 0;
  winAttribs.background_pixmap = 0;
  winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
  gColormap = winAttribs.colormap;
  winAttribs.background_pixel = BlackPixel(gpDisplay,DefaultScreen);
  winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
  
  styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow = XCreateWindow(gpDisplay,
			  RootWindow(gpDisplay,gpXVisualInfo->screen),
			  0,
			  0,
			  giWindowWidth,
			  giWindowHeight,
		  	  0,
			  gpXVisualInfo->depth,
			  InputOutput,	/*Only InputOutput i.e. Remote client cant access this window*/
			  gpXVisualInfo->visual,
			  styleMask,
			  &winAttribs);
  if(!gWindow)
  {
	printf("\n Error while creating the window\nExiting now\n")	;
	uninitialize();
	exit(0);
  }

 XStoreName(gpDisplay,gWindow,"XLIB Perspective Triangle");

 Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XMapWindow(gpDisplay,gWindow);	//LIKE ShowWindow() and UpdateWindow();

}


void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
 
  
}

void initialize()
{
  gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,0.0f, 1.0);

 glShadeModel(GL_SMOOTH);

 glClearDepth(1.0f);
 glEnable(GL_DEPTH_TEST);
 glDepthFunc(GL_LEQUAL);
 
 glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
 glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffused);
 glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
 glLightfv(GL_LIGHT0,GL_POSITION,light_position);

 glEnable(GL_LIGHT0);

 LoadMeshFile("MonkeyHead.OBJ");
  
 resize(giWindowHeight,giWindowHeight);
 
}

void updateangle()
{
	g_fl_angle_y += 0.5f;

	if(g_fl_angle_y > 360.0f)
	{
		g_fl_angle_y = 0.0f;
	}
}

void display()
{
	fprintf(fp,"\n Inside Display\n");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-5.0f);
	glScalef(1.5f,1.5f,1.5f);
	glRotatef(g_fl_angle_y,0,1,0);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	
	for(int i=0; i !=g_face_tri.size(); i++)
	{
		glBegin(GL_TRIANGLES);
		for(int j =0; j != g_face_tri[i].size(); j++)
		{
				int vi = g_face_tri[i][j] -1;
				int vin = g_face_normals[i][j] - 1;
				glNormal3f(g_normals[vin][0],g_normals[vin][1],g_normals[vin][2]);
				glVertex3f(g_vertices[vi][0],g_vertices[vi][1],g_vertices[vi][2]);
		}
		glEnd();

	}

	updateangle();
	glXSwapBuffers(gpDisplay,gWindow);
	return;
}


void resize(int height, int width)
{
  if(height == 0)
  {
	height = 1;
  }
 glViewport(0,0,(GLsizei)width,(GLsizei)height);
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();

 //glOrtho(-5.f,5.0f,-5.0f,5.0f,-5.0f,5.0f);
 gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f); 

}


void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


void LoadMeshFile(char *szFilename)
{	
	if(szFilename == NULL)
	{
		fprintf(fp,"\n Empty fileName string!!!\n");
		exit(1);
	}
	
	g_fp_MeshFile = fopen(szFilename,"r");
	if(g_fp_MeshFile == NULL)
	{
		fprintf(fp,"\n Error while openinhg file \n");
		exit(1);
	}

	//Seprator strings
	char *sep_space = " ";
	char *sep_fslash = "/";

	//Character pointer for holding first word
	char *first_token =NULL;
	//Character pointer for holding word seperated by specified seperator strtok
	char *token = NULL;

	//Character pointers for ho;ding strings associated with 
	//1] vertex index	2] Texture Index	3]Normal Index
	char *token_vertex_index= NULL,*token_texture_index= NULL,*token_normal_index= NULL;
	char *Face_tokens[NR_FACE_TOKENS];

	int nr_tokens;

	while(fgets(line,BUFFER_SIZE,g_fp_MeshFile))
	{
		first_token = strtok(line,sep_space);

		if(strcmp(first_token,"v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for(int i = 0; i!=NR_POINT_COORDS; i++)
			{
				vec_point_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_vertices.push_back(vec_point_coord);

		}
		else if(strcmp(first_token,"vt")== S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for(int i =0; i != NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_normals.push_back(vec_texture_coord);
		}
		else if(strcmp(first_token,"vn")== S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for(int i =0; i != NR_NORMAL_COORDS; i++)
			{
				vec_normal_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_normals.push_back(vec_normal_coord);
		}
		else if(strcmp(first_token,"f")== S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3),texture_vertex_indices(3),normal_vertex_indices(3);

			memset((void *)Face_tokens,0,NR_FACE_TOKENS);

			nr_tokens =0;

			while(token = strtok(NULL,sep_space))
			{
				if(strlen(token) < 3)
				{
					break;
				}
				Face_tokens[nr_tokens] = token;
				nr_tokens++;
			}
	
			for(int i =0; i !=NR_FACE_TOKENS; i++)
			{
				token_vertex_index  = strtok(Face_tokens[i],sep_fslash);
				token_texture_index = strtok(NULL,sep_fslash);
				token_normal_index  = strtok(NULL,sep_fslash);

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i]  = atoi(token_texture_index);
				normal_vertex_indices[i]   = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(triangle_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void *)line,(int)'\0',BUFFER_SIZE);
	}

	fprintf(fp,"\n g_vertices: <%llu>\t g_texture:<%llu>\t g_normals:<%llu>\t g_face_tri:<%llu>\n",
				g_vertices.size(),g_texture.size(),g_normals.size(),g_face_tri.size());
	fprintf(fp,"\n Closing Mesh File\n");
	fclose(g_fp_MeshFile);
}

