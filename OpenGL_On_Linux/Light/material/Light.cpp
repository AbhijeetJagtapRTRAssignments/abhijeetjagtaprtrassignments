#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<SOIL/SOIL.h>

using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;


FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;
float gfRotationAngle = 0.0f;


float gfUpdateAngle = 0.0f;

/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);




/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int,int);
void rotate(void);

/*LIGHT RELATED VARIABLES*/
GLfloat light0_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat light0_diffused[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light0_specular[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light0_position[] = {0.0f,0.0f,1.0f,0.0f};

GLfloat material_ambient[][4]  = {{0.0215f,0.1745f,0.0215f,1.0f},{0.135f,0.2225f,0.1575f,1.0f},{0.05375f,0.05f,0.06625f,1.0f},
								 {0.25f,0.20725f,0.20725f,1.0f},{0.1745f,0.01175f,0.01175f,1.0f},{0.1f,0.18725f,0.1745f,1.0f},
								 {0.329412f,0.223529f,0.027451f,1.0f}, {0.2125f,0.1275f,0.054f,1.0f},{0.25f,0.25f,0.25f,1.0f},
								 {0.19125f,0.0735f,0.0225f,1.0f},{0.24725f,0.1995f,1.0f},{0.19225f,0.19225f,0.19225f,1.0f},	
								 {0.0f,0.0f,0.0f,1.0f}, {0.0f,0.1f,0.06f,1.0f}, {0.0f,0.0f,0.0f,1.0f},
							     {0.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},
								 {0.02f,0.02f,0.02f,1.0f},{0.00f,0.05f,0.05f,1.0f},{0.0f,0.05f,0.0f},
								 {0.5f,0.0f,0.0f,1.0f}, {0.05f,0.05f,0.05f,1.0f},{0.05f,0.05f,0.00f,1.0f}};


GLfloat material_diffused[][4] = {{0.07568f,0.61424f,0.07568f,1.0f},{0.54f,0.89f,0.63f,1.0f},{0.18275f,0.17f,0.22525f,1.0f},
								 {1.0f,0.829f,0.829f,1.0f},{0.61424f,0.04136f,0.04136f,1.0f} ,{0.396f,0.74151f,0.69102f,1.0f},
								 {0.780392f,0.568627f,0.113725f,1.0f}, {0.714f,0.4284f,0.18144f,1.0f}, {0.4f,0.4f,0.4f,1.0f},
								 {0.7038f,0.27048f,0.0828f,1.0f},{0.75164f,0.60648f,0.22648f,1.0f},{0.01f,0.01f,0.01f,1.0f},
								 {0.01f,0.01f,0.01f,1.0f}, {0.0f,0.50980392f,0.50980392f,1.0f},{0.1f,0.35f,0.1f,1.0f},
								 {0.5f,0.0f,0.0f,1.0f},{0.550f,0.550f,0.550f,1.0f},{0.50f,0.50f,0.0f,1.0f},
								 {0.01f,0.01f,0.01f,1.0f},{0.4f,0.5f,0.5f,1.0f},{0.40f,0.05f,0.40f,1.0f},
								 {0.5f,0.40f,0.40f,1.0f},{0.5f,0.5f,0.5f,1.0f},{0.5f,0.5f,0.4f,1.0f}};


GLfloat material_specular[][4] = {{0.633f,0.727811f,0.633f,1.0f},{0.316228f,0.316228f,0.316228f},{0.332741f,0.328634f,0.346435f,1.0f},
								 {0.296648f,0.296648f,0.296648f,1.0f},{0.72781f,0.626959f,0.626959f,1.0f},{0.297254f,0.30829f,0.306678f,1.0f},
							     {0.992157f,0.941176f,0.807843f,1.0f}, {0.393546f,0.271906f,0.166721f,1.0f}, {0.774597f,0.774597f,0.774597f,1.0f},
{0.256777f,0.137622f,0.086014f,1.0f},{0.628281f,0.555802f,0.366065f,1.0f},{0.5f,0.5f,0.5f,1.0f},
{0.50f,0.5f,0.50f,1.0f}, {0.50196078f,0.50196078f,0.50196078f,1.0f},{0.45f,0.55f,0.45f,1.0f},
{0.70f,0.6f,0.6f,1.0f},{0.70f,0.70f,0.70f,1.0f},{0.60f,0.60f,0.60f,1.0f},
								 {0.4f,0.4f,0.4f,1.0f},{0.04f,0.7f,0.7f,1.0f},{0.04f,0.7f,0.04f,1.0f},
								 {0.7f,0.04f,0.04f,1.0f},{0.7f,0.7f,0.7f,1.0f}, {0.7f,0.7f,0.04f,1.0f}};


GLfloat material_shinynes[][1]= {{(0.6f*128.0f)},{0.6f*128.0f},{0.3f*128.0f},{0.088f*128.0f},{0.6f*128.0f},{0.1f*128.0f},
								 {0.21794872f*128.0f}, {0.2f*128.0f},{0.6f*128.0f},{0.1f*128.0f},{0.4f*128.0f},{0.25f*128.0f},
								 {0.25f*128.0f}, {0.25f*128.0f}, {0.25f*128.0f},{0.25f*128.0f},{0.25f*128.0f},{0.25f*128.0f},
								 {0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f}};


bool gbIsXKeypressed = false;
bool gbIsYKeypressed = false;
bool gbIsZKeypressed = false;

GLfloat gfRightX = -9.0f;
GLfloat gfTopY	= 8.5f;
GLfloat gfDistanceAtX = 6.5f;
GLfloat gfDistanceAtY = 2.5f;
GLfloat gfPositionX = 0.0f, gfPositionY =0.0f; 

float gfAngleOfLight   = 0.0f;
float gfAngleY  = 0.0f;
float gfAngleZ = 0.0f;

bool gbLihting = false;
GLUquadric *quadric = NULL;

int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("ErroLog.txt" , "w");
 if(fp == NULL)
 {
   printf(" \n Error while creating file \n ");
   exit(0);
 }

 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
			giWindowWidth  = event.xconfigure.width;
			giWindowHeight = event.xconfigure.height;	
			resize(giWindowHeight,giWindowWidth);
			//fprintf(fp,"\n Inside case WM_SIZE i.e. ConfigureNotify\n giWindowWidth <%d> \ngiWindowHeight <%d> \n",giWindowWidth,giWindowHeight);	
			break;
		case Expose:
		   //WM_PAINT	
			break;
		case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
		switch(keysym)
		{
			case XK_Escape:
				fprintf(fp,"\n Inside XK_ESCAPE\n");
				bDone = true;
				//uninitialize();		//IN GEME LOOP HERE ONLY bDone = FALSE;
				//exit(0);
			break;
			case XK_F:
			case XK_f:
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
					
					ToggleFullScreen();
					gbFullScreen = true;		
				}
				else
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
				
			break;
			case XK_L:
			case XK_l:
			fprintf(fp,"Inside case XK_L");
			if(gbLihting == false)
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_LIGHT0);
				gbLihting = true ;
			}
			else
			{
				glDisable(GL_LIGHT0);
				glDisable(GL_LIGHTING);
				gbLihting = false;
			}
				
			break;
			case XK_X:
			case XK_x:
				fprintf(fp,"Inside case XK_X");
				gbIsXKeypressed = true;
				gbIsYKeypressed = false;
				gbIsZKeypressed = false;

				gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;

			break;

			case XK_Y:
			case XK_y:
				fprintf(fp,"Inside case XK_y");
				gbIsXKeypressed = false;
				gbIsYKeypressed = true;
				gbIsZKeypressed = false;

				gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
				
			break;
			
			case XK_Z:
			case XK_z:
				fprintf(fp,"Inside case XK_z");
				gbIsXKeypressed = false;
				gbIsYKeypressed = false;
				gbIsZKeypressed = true;

				gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;

			break;


			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				   //WM_LBUTTONDOWN
					fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
					break;
				case 2:
				  //WM_MBUTTONDOWN
					fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
					break;
				case 3:
				  //WM_RBUTTONDOWN
					fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
					break;
			} 
			break;			
		}
		break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }

   display();
  
 }

return 0;
	
}

void CreateWindow()
{
  XSetWindowAttributes winAttribs;
  int DefaultScreen;
  int DefaultDepth;
  int styleMask;

  static  int frameBufferAttributes[]=
  {
	GLX_RGBA,
	GLX_RED_SIZE,8,
	GLX_GREEN_SIZE,8,
	GLX_BLUE_SIZE,8,
	GLX_ALPHA_SIZE, 1,
	GLX_DEPTH_SIZE,24,
	GLX_DOUBLEBUFFER,True,
	None
		
  };
 
 /*STEP 1*/
  gpDisplay = XOpenDisplay(NULL);
  if(gpDisplay == NULL)
  {
	printf("\nUnable to open Display!!!\n");
	uninitialize();
	exit(1);
  }

  /*STEP 2*/
  DefaultScreen = XDefaultScreen(gpDisplay);
  
  
  /*STEP 3*/
  DefaultDepth = DefaultDepth(gpDisplay,DefaultScreen);
  
  /*STEP 4*/
  gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while allocating memory to XVisualInfo.\Exiting\n");
	uninitialize();
 	exit(1);
  }  

  
  /*STEP 5*/
  /*XMatchVisualInfo(gpDisplay,DefaultScreen,DefaultDepth,TrueColor,gpXVisualInfo);
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while Getting Visual\n Exiting\n");
	uninitialize();
	exit(1);
  }*/
  gpXVisualInfo = glXChooseVisual(gpDisplay,DefaultScreen,frameBufferAttributes);
 
  /*SETTING WinAttributes--->Similar like filling WNDCLASSEX struct*/
  winAttribs.border_pixel = 0;
  winAttribs.background_pixmap = 0;
  winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
  gColormap = winAttribs.colormap;
  winAttribs.background_pixel = BlackPixel(gpDisplay,DefaultScreen);
  winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
  
  styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow = XCreateWindow(gpDisplay,
			  RootWindow(gpDisplay,gpXVisualInfo->screen),
			  0,
			  0,
			  giWindowWidth,
			  giWindowHeight,
		  	  0,
			  gpXVisualInfo->depth,
			  InputOutput,	/*Only InputOutput i.e. Remote client cant access this window*/
			  gpXVisualInfo->visual,
			  styleMask,
			  &winAttribs);
  if(!gWindow)
  {
	printf("\n Error while creating the window\nExiting now\n")	;
	uninitialize();
	exit(0);
  }

 XStoreName(gpDisplay,gWindow,"XLIB Perspective Triangle");

 Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XMapWindow(gpDisplay,gWindow);	//LIKE ShowWindow() and UpdateWindow();

}


void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
 
  
}

void initialize()
{
  gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.250f,0.250f,0.250f, 1.0);

 glShadeModel(GL_SMOOTH);

 glClearDepth(1.0f);
 glEnable(GL_DEPTH_TEST);
 glDepthFunc(GL_LEQUAL);

 	glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffused);
	glLightfv(GL_LIGHT0,GL_AMBIENT,light0_ambient);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
	glEnable(GL_LIGHT0);


	quadric = gluNewQuadric();

 glEnable(GL_LIGHT0);

 resize(giWindowHeight,giWindowHeight);
 
}

void UpdateAngle()
{
	
	gfAngleOfLight   =  gfAngleOfLight + 0.5f;
		if(gfAngleOfLight  >= 360.0f )
		{
			gfAngleOfLight = 0.0f;  
		}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

  glPushMatrix();
    
    gluLookAt(0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	

	glPushMatrix();
		if(gbIsXKeypressed)
		{
			glRotatef(gfAngleOfLight,1.0f,0.0f,0.0f);
			light0_position[0] = light0_position[1] = 0.0f;
			light0_position[2] = gfAngleOfLight;
		}
		else if(gbIsYKeypressed)
		{
			glRotatef(gfAngleOfLight,0.0f,1.0f,0.0f);
			light0_position[2] = light0_position[1] = 0.0f;
			light0_position[0] = gfAngleOfLight;
		}
		else if(gbIsZKeypressed)
		{
			glRotatef(gfAngleOfLight,0.0f,0.0f,1.0f);
			light0_position[2] = light0_position[0] = 0.0f;
			light0_position[1] = gfAngleOfLight;
		}
		//
		glLightfv(GL_LIGHT0,GL_POSITION,light0_position);
	glPopMatrix();

	glTranslatef(0.0f,gfTopY,-16.5f);
	for(int j =0; j < 6; j++)
	{
		glTranslatef(0.0f,-gfDistanceAtY,0.0f);
		for(int i =0; i <4; i++)
		{
			glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,material_diffused[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,material_ambient[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,material_specular[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,material_shinynes[(j*4)+i]);

			//glRotatef(90.0f,1.0f,0.0f,0.0f);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			glPushMatrix();
			glTranslatef(gfPositionX,0.0f,0.0f);			
			gluSphere(quadric,0.5,30,30);
			glPopMatrix();
			gfPositionX = gfPositionX + gfDistanceAtX;
		}
		gfPositionX = gfRightX;
	}
	gfPositionY = gfTopY;
	gfPositionX = gfRightX;
	
  glPopMatrix();

	UpdateAngle();
	glXSwapBuffers(gpDisplay,gWindow);
	return;
}




void resize(int height, int width)
{
  if(height == 0)
  {
	height = 1;
  }
 glViewport(0,0,(GLsizei)width,(GLsizei)height);
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();

 //glOrtho(-5.f,5.0f,-5.0f,5.0f,-5.0f,5.0f);
 gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f); 

}


void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


