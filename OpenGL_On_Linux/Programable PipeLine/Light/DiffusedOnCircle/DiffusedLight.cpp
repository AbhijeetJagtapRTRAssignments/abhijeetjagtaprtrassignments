
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include"Sphere.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glew.h>
#include<GL/gl.h>
#include<GL/glx.h>

#include"vmath.h"
using namespace vmath;


using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;


FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;

typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,Bool,const int*);
GLXCreateContextAttribsARBProc GLXCreateContextAttribsARB =  NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);

/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int,int);

/*PROGRAMABLE PIPELINE RELATED VARIABLES*/

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE
};

GLuint gVao_Cube;
GLuint gVbo_Position_Cube;
GLuint gVbo_Normal_Cube;

GLuint gMVPUniform;


mat4 gPerspectiveProjectionMatrix;
mat4 gRotationMatrix;
mat4 gScaleMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float gfUpdateAngle = 0.0f;

//LIGHT RELATED VARIABLES

GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gKdUniform, gLightPositionUniform;
GLuint gLKeyPressedUniform;


bool gbAnimate = false;
bool gbLight = false;

/*SPHERE RELATED VARIABLES*/
GLuint gVao_Sphere;
GLuint gVbo_Vertex_Sphere;
GLuint gVbo_Normal_Sphere;
GLuint gVbo_Sphere_Element;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;


int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("DIFFUSE_LIGHT_ON_SPHERE.txt" , "w");
 if(fp == NULL)
 {
   printf(" \n Error while creating file \n ");
   exit(0);
 }

 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
			giWindowWidth  = event.xconfigure.width;
			giWindowHeight = event.xconfigure.height;	
			resize(giWindowHeight,giWindowWidth);
			//fprintf(fp,"\n Inside case WM_SIZE i.e. ConfigureNotify\n giWindowWidth <%d> \ngiWindowHeight <%d> \n",giWindowWidth,giWindowHeight);	
			break;
		case Expose:
		   //WM_PAINT	
			break;
		case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
		switch(keysym)
		{
			case XK_Escape:
				fprintf(fp,"\n Inside XK_ESCAPE\n");
				bDone = true;
				//uninitialize();		//IN GEME LOOP HERE ONLY bDone = FALSE;
				//exit(0);
			break;
			case XK_F:
			case XK_f:
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
					
					ToggleFullScreen();
					gbFullScreen = true;		
				}
				else
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
				
			break;
			
			case XK_L:
			case XK_l:
			  if (gbLight == false)
			  {
				  gbLight = true;
			  }
			  else
			  {
				  gbLight = false;
			  }
			break;
    
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				   //WM_LBUTTONDOWN
					fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
					break;
				case 2:
				  //WM_MBUTTONDOWN
					fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
					break;
				case 3:
				  //WM_RBUTTONDOWN
					fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
					break;
			} 
			break;			
		}
		break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }

   display();

 }

return 0;
	
}

void CreateWindow()
{
	XSetWindowAttributes winAttribs;
	
	GLXFBConfig *pGLXFBConfigs = NULL;

	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempVisualInfo = NULL;
	int iNumFBConfigs = 0;
	
	int i;
	
	/*DECLARE OUR OWN FRAME_BUFFER_CONFIGURATION*/
	
	static int frameBufferAttributes[]=
	{
		GLX_X_RENDERABLE,True,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		//GLX_SAMPLE_BUFFERS,1,
		//GLX_SAMPLES,4
		None
	};
	
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		fprintf(fp,"\nError while getting gpDisplay \n");
		exit(1);
	}
	
	//NOE GET NEW FRAMEBUFFERCONFIG THAT MEETS OUR REQUIREMENT
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);

	if(pGLXFBConfigs == NULL)
	{
		fprintf(fp,"\nError while getting valid FBConfig...Exiting now\n");
		exit(1);
	}
	fprintf(fp,"\n %d matching FB Config found\n",iNumFBConfigs);
	fprintf(fp,"\nCreateWindow()-> %d matching FB Config found\n",iNumFBConfigs);
	
	int bestFramebufferconfig = -1,worstFramebufferconfig = -1, bestNumberOfSamples = -1,worstNumberOfSamples = 999;
	for(i = 0; i < iNumFBConfigs; i++)
	{
   		pTempVisualInfo =glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempVisualInfo)
		{
			int sampleBuffer,samples;
			
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&samples);
			fprintf(fp,"\n Matching FrameBuffer Config: %d,\t Visual ID = 0x%lu:,\tSample Buffers:%d, \t Samples: %d \n",i,pTempVisualInfo->visualid,sampleBuffer,samples);
			
			if(bestFramebufferconfig <0 || sampleBuffer && samples > bestNumberOfSamples )
			{
				bestFramebufferconfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferconfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferconfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	//SET GLOBAL GLXFBConfig
	
	gGLXFBConfig = bestGLXFBConfig;
	//FREE THE LIST OF FBCONFIGS ALLOCATED BYglXChooseFBConfigs
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	fprintf(fp,"\n Choosen Visual ID : 0x%lu\n",gpXVisualInfo->visualid);
	
	//SETTING WINDOWS ATTRIBUTES
	
	winAttribs.border_pixel =0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap =XCreateColormap(gpDisplay,
										 RootWindow(gpDisplay,gpXVisualInfo->screen),
										 gpXVisualInfo->visual,
										 AllocNone
										 );

	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;
	
	int stylemask;
	stylemask = CWBorderPixel | CWEventMask | CWColormap;
	gColormap= winAttribs.colormap;
	
	gWindow = XCreateWindow( gpDisplay,
							 RootWindow(gpDisplay,gpXVisualInfo->screen),
							 0,
							 0,
							 giWindowWidth,
							 giWindowHeight,
							 0,
							 gpXVisualInfo->depth,
							 InputOutput,
							 gpXVisualInfo->visual,
							 stylemask,
							 &winAttribs
							 );

	if(!gWindow)
	{
		fprintf(fp,"\n Failure in Window Creation \n");
		uninitialize();
		exit(1);
	}	
	
	XStoreName(gpDisplay,gWindow,"DIFFUSE_LIGHT_ON_SPHERE");
	
	Atom windowManagerDelete =XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
}

void initialize()
{
  /*gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,1.0f,0.0f);
  
  resize(giWindowHeight,giWindowHeight);
 */

fprintf(fp,"\n Inside initialise\n");
 GLXCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
 
 GLint attribs[] = {
 					 GLX_CONTEXT_MAJOR_VERSION_ARB,3,
 					 GLX_CONTEXT_MINOR_VERSION_ARB,0,
 					 GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
 					 0
 				   };
fprintf(fp,"\n Before GLXCreateContextAttribsARB()--> 4.5\n");			   
 gGLXContext = GLXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
 fprintf(fp,"\n After GLXCreateContextAttribsARB()--> 4.5\n");			   
 if(gGLXContext)
 {
 	fprintf(fp,"\n OpenGL Context 3.0 gets successfully created\n");
 }
 else
 {
 	 GLint attribs[] = {
 					 GLX_CONTEXT_MAJOR_VERSION_ARB,1,
 					 GLX_CONTEXT_MINOR_VERSION_ARB,0,
 					 0
 				   };
 	fprintf(fp,"\nFailed to create GLX 4.5 ,hence using Old-style GLX Context\n");
 	fprintf(fp,"\n Before GLXCreateContextAttribsARB()--> 1.0\n");			   
    gGLXContext = GLXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
    fprintf(fp,"\n After GLXCreateContextAttribsARB()--> 1.0\n");			   
 				   
 }
 
 if(!glXIsDirect(gpDisplay,gGLXContext))
 {
 	fprintf(fp,"\n Indirect GLX rendering Context obtained\n");
 }
 else
 {
 	fprintf(fp,"\n Direct GLX Rendering Context obtained \n");
 }
 
 glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

 GLenum glew_error = glewInit();
 if(glew_error != GLEW_OK)
 {
   fprintf(fp,"\nERROR: error while initialising the GLEW\n");
   uninitialize();
   exit(0);
 }
 
 fprintf(fp,"\n OPENGL VERSION: %s ",glGetString(GL_VERSION));
 fprintf(fp,"\n OPENGL SHADER VERSION: %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));
 
//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal ;" \
		"uniform mat4 u_model_view_matrix ;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuse_light;" \
		"void main(void)" \
		"{"\
		"if(u_LKeyPressed == 1)"\
		"{"\
		"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"\
		"vec3 tnorm = normalize(mat3(u_model_view_matrix) *vNormal );"\
		"vec3 s = normalize(vec3(u_light_position - eyeCoordinates));"\
		"diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
		"}";
		
		

	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
	//COMPILE VERTEX SHADER

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\nVERTEX SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n VERTEX SHADER COMPILED SUCCESSGFULLY \n");
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 130"\
		"\n"\
		"in vec3 diffuse_light;"\
		"out vec4 FragColor;"\
		"uniform int u_LKeyPressed;"\
		"void main(void)"\
		"{"\
		"vec4 Color;"\
		"if(u_LKeyPressed ==1)"\
		"{"\
		"Color = vec4(diffuse_light,1.0);"\
		"}"\
		"else"\
		"{"\
		"Color = vec4(1.0,1.0,1.0,1.0);"\
		"}"\
		"FragColor = Color; "\
		"}";
		

	glShaderSource(gFragmentShaderObject, 1, (const char **)&fragmentShaderSourceCode, NULL);
	//COMPILE FRAGMENT SHADER
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n FRAGMENT SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n FRAGMENT SHADER COMPILED SUCCESSGFULLY \n");
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_LINK_STATUS, &iShaderLinkStatus);
	if (iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n LINK TIME ERROR LOG: \n%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\nSHADER LINKED SUCCESSFULLY \n");
	}

	gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	//VERTICES,COLOR,SHADER_ATTRIBUTES,vbo,vao initialisation

	//VERTICES,COLOR,SHADER_ATTRIBUTES,vbo,vao initialisation

	//START THE SPHERE RECORDER
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//	START THE SPHERE RECORDER

	glGenVertexArrays(1, &gVao_Sphere);
	glBindVertexArray(gVao_Sphere);

	//BIND TO THE VERTEX ARRAY OF SHERE
	glGenBuffers(1, &gVbo_Vertex_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Vertex_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE NORMAL BUFFER
	glGenBuffers(1, &gVbo_Normal_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE SPHERE ELEMENT BUFFER
	glGenBuffers(1, &gVbo_Sphere_Element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//UNBIND FROM THE SPHERE VAO
	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()


	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0);

	gPerspectiveProjectionMatrix = mat4::identity();
	gRotationMatrix = mat4::identity();
	gScaleMatrix = mat4::identity();

    resize(giWindowWidth,giWindowHeight);
}

void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle + 0.20f;
	if (gfUpdateAngle >= 360.0f)
	{
		gfUpdateAngle = 0;
	}
}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	vmath::mat4 modelMatrix = mat4::identity();
	mat4 ModelViewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);
		glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);

		float lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };
		glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	//gScaleMatrix = scale(0.75f, 0.75f, 0.75f);
	
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	//RotationMatrix = rotate(gfUpdateAngle, gfUpdateAngle, gfUpdateAngle);
	ModelViewMatrix = modelMatrix * RotationMatrix;
	//ModelViewMatrix = ModelViewMatrix *gScaleMatrix;

	glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, ModelViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	
	//NOW DRAW RECTANGLE BEFORE THAT MAKE IDENTITY TO modelViewMatrix and modelViewProjectionMatrix
	glBindVertexArray(gVao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
glUseProgram(0);

 //UpdateAngle();
 glXSwapBuffers(gpDisplay,gWindow);
  
  return;

}

void resize(int height, int width)
{
 fprintf(fp,"\nINSIDE RESIZE \n");
 fprintf(fp,"\n WIDTH: %d HEIGHT: %d \n",height,width);	
	if(height == 0)
	{
		height = 1;
	}
	
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	 gPerspectiveProjectionMatrix= vmath::perspective(45.0f,(float)width/float(height),0.1f,100.0f);
	return;
}

void uninitialize()
{
 GLXContext currentGLXContext ;
 
 if (gVbo_Normal_Cube)
	{
		glDeleteBuffers(1, &gVbo_Normal_Cube);
		gVbo_Normal_Cube = 0;
	}
	if (gVbo_Position_Cube)
	{
		glDeleteBuffers(1, &gVbo_Position_Cube);
		gVbo_Position_Cube = 0;
	}
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1, &gVao_Cube);
		gVao_Cube = 0;
	}
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject = 0;

 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {

   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


