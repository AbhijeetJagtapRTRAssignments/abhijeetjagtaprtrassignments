#include<stdio.h>
#include<stdlib.h>
#include<dlfcn.h>
int main()
{
	int iNum = 16;
	
	void *Handle =NULL;
	char *error = NULL;
	
	//typedef int (* add)(int,int);
	//Add ptrAdd;
	int (* Add)(int,int);
	
	Handle = dlopen("/home/abhijeet/programs/OpenGL/dlopen/libadd.so",RTLD_LAZY);
	
	if(!Handle)
	{
		fputs(dlerror(),stderr);
		exit(1);
	}

	printf("\nDLL gets loaded successfully\n");
	
	Add=dlsym(Handle,"Add");
	if((error = dlerror()) == NULL )
	{
		fputs(error,stderr);
		exit(1);
	}

	printf("\ndlsym for Add is done successfully \n");
	
	printf("\nAddition is: %d\n",(*Add)(10,20));

}
