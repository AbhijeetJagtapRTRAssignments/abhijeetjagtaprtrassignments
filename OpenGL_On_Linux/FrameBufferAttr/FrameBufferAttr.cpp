
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>

using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;


FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;

typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,Bool,const int*);
GLXCreateContextAttribsARBProc GLXCreateContextAttribsARB =  NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;


/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);

/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int,int);

int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("ErroLog.txt" , "w");
 if(fp == NULL)
 {
   printf(" \n Error while creating file \n ");
   exit(0);
 }

 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
			giWindowWidth  = event.xconfigure.width;
			giWindowHeight = event.xconfigure.height;	
			//fprintf(fp,"\n Inside case WM_SIZE i.e. ConfigureNotify\n giWindowWidth <%d> \ngiWindowHeight <%d> \n",giWindowWidth,giWindowHeight);	
			break;
		case Expose:
		   //WM_PAINT	
			break;
		case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
		switch(keysym)
		{
			case XK_Escape:
				fprintf(fp,"\n Inside XK_ESCAPE\n");
				bDone = true;
				//uninitialize();		//IN GEME LOOP HERE ONLY bDone = FALSE;
				//exit(0);
			break;
			case XK_F:
			case XK_f:
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
					
					ToggleFullScreen();
					gbFullScreen = true;		
				}
				else
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
				
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				   //WM_LBUTTONDOWN
					fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
					break;
				case 2:
				  //WM_MBUTTONDOWN
					fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
					break;
				case 3:
				  //WM_RBUTTONDOWN
					fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
					break;
			} 
			break;			
		}
		break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }

   display();

 }

return 0;
	
}

void CreateWindow()
{
	XSetWindowAttributes winAttribs;
	
	GLXFBConfig *pGLXFBConfigs = NULL;

	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempVisualInfo = NULL;
	int iNumFBConfigs = 0;
	
	int i;
	
	/*DECLARE OUR OWN FRAME_BUFFER_CONFIGURATION*/
	
	static int frameBufferAttributes[]=
	{
		GLX_X_RENDERABLE,True,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		//GLX_SAMPLE_BUFFERS,1,
		//GLX_SAMPLES,4
		None
	};
	
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("\nError while getting gpDisplay \n");
		exit(1);
	}
	
	//NOE GET NEW FRAMEBUFFERCONFIG THAT MEETS OUR REQUIREMENT
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);

	if(pGLXFBConfigs == NULL)
	{
		printf("\nError while getting valid FBConfig...Exiting now\n");
		exit(1);
	}
	printf("\n %d matching FB Config found\n",iNumFBConfigs);
	fprintf(fp,"\nCreateWindow()-> %d matching FB Config found\n",iNumFBConfigs);
	
	int bestFramebufferconfig = -1,worstFramebufferconfig = -1, bestNumberOfSamples = -1,worstNumberOfSamples = 999;
	for(i = 0; i < iNumFBConfigs; i++)
	{
   		pTempVisualInfo =glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempVisualInfo)
		{
			int sampleBuffer,samples;
			
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&samples);
			printf("\n Matching FrameBuffer Config: %d,\t Visual ID = 0x%lu:,\tSample Buffers:%d, \t Samples: %d \n",i,pTempVisualInfo->visualid,sampleBuffer,samples);
			
			if(bestFramebufferconfig <0 || sampleBuffer && samples > bestNumberOfSamples )
			{
				bestFramebufferconfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferconfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferconfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	//SET GLOBAL GLXFBConfig
	
	gGLXFBConfig = bestGLXFBConfig;
	//FREE THE LIST OF FBCONFIGS ALLOCATED BYglXChooseFBConfigs
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	printf("\n Choosen Visual ID : 0x%lu\n",gpXVisualInfo->visualid);
	
	//SETTING WINDOWS ATTRIBUTES
	
	winAttribs.border_pixel =0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap =XCreateColormap(gpDisplay,
										 RootWindow(gpDisplay,gpXVisualInfo->screen),
										 gpXVisualInfo->visual,
										 AllocNone
										 );

	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;
	
	int stylemask;
	stylemask = CWBorderPixel | CWEventMask | CWColormap;
	gColormap= winAttribs.colormap;
	
	gWindow = XCreateWindow( gpDisplay,
							 RootWindow(gpDisplay,gpXVisualInfo->screen),
							 0,
							 0,
							 giWindowWidth,
							 giWindowHeight,
							 0,
							 gpXVisualInfo->depth,
							 InputOutput,
							 gpXVisualInfo->visual,
							 stylemask,
							 &winAttribs
							 );

	if(!gWindow)
	{
		printf("\n Failure in Window Creation \n");
		uninitialize();
		exit(1);
	}	
	
	XStoreName(gpDisplay,gWindow,"OpenGL Window");
	
	Atom windowManagerDelete =XInternAtom(gpDisplay,"WM_WINDOW_DELETE",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	XMapWindow(gpDisplay,gWindow);

}

void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
}

void initialize()
{
  /*gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,1.0f,0.0f);
  
  resize(giWindowHeight,giWindowHeight);
 */

fprintf(fp,"\n Inside initialise\n");
 GLXCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
 
 GLint attribs[] = {
 					 GLX_CONTEXT_MAJOR_VERSION_ARB,3,
 					 GLX_CONTEXT_MINOR_VERSION_ARB,0,
 					 GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
 					 0
 				   };
fprintf(fp,"\n Before GLXCreateContextAttribsARB()--> 4.5\n");			   
 gGLXContext = GLXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
 fprintf(fp,"\n After GLXCreateContextAttribsARB()--> 4.5\n");			   
 if(gGLXContext)
 {
 	printf("\n OpenGL Context 3.0 gets successfully created\n");
 }
 else
 {
 	 GLint attribs[] = {
 					 GLX_CONTEXT_MAJOR_VERSION_ARB,1,
 					 GLX_CONTEXT_MINOR_VERSION_ARB,0,
 					 0
 				   };
 	printf("\nFailed to create GLX 4.5 ,hence using Old-style GLX Context\n");
 	fprintf(fp,"\n Before GLXCreateContextAttribsARB()--> 1.0\n");			   
    gGLXContext = GLXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
    fprintf(fp,"\n After GLXCreateContextAttribsARB()--> 1.0\n");			   
 				   
 }
 
 if(!glXIsDirect(gpDisplay,gGLXContext))
 {
 	printf("\n Indirect GLX rendering Context obtained\n");
 }
 else
 {
 	printf("\n Direct GLX Rendering Context obtained \n");
 }
 
 glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
 glClearDepth(1.0f);
 glEnable(GL_DEPTH_TEST);
 glDepthFunc(GL_LEQUAL);
 glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
 glEnable(GL_CULL_FACE);
 
 glClearColor(0.0f,0.0f,1.0f,0.0f);
 
 resize(giWindowWidth,giWindowHeight);
}


void display()
{
  glClear(GL_COLOR_BUFFER_BIT);

  glXSwapBuffers(gpDisplay,gWindow);

}

void resize(int height, int width)
{
  if(height == 0)
  {
	height = 1;
  }
 glViewport(0,0,(GLsizei)width,(GLsizei)height);
}

void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


