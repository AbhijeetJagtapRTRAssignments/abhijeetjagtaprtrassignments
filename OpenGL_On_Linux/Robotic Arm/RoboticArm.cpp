
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;

FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;
float gfRotationAngle = 0.0f;

/*OpenGL Rendering variables*/
GLUquadric *quadric = NULL;
static int Shoulder = 0;
static int Elbow = 0;

float gfUpdateAngle;
float gfUpdateYCord = 0;

char ascii[32] = {'\0'};

/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);



/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int,int);
void rotate(void);

int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("Error.txt","w");
 if(fp == NULL)
 {
 	printf("\nError while opening Error file \n");
 	exit(1);
 }
 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
			giWindowWidth  = event.xconfigure.width;
			giWindowHeight = event.xconfigure.height;	
			resize(giWindowHeight,giWindowWidth);
			//fprintf(fp,"\n Inside case WM_SIZE i.e. ConfigureNotify\n giWindowWidth <%d> \ngiWindowHeight <%d> \n",giWindowWidth,giWindowHeight);	
			break;
		case Expose:
		   //WM_PAINT	
			break;
		case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
	       if(keysym == XK_Escape)
		{
			fprintf(fp,"\n Inside XK_ESCAPE\n");
			bDone = true;
		}
	      	else
	        {
		   XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
	    	  switch(ascii[0])
		  {
	
			case 'F':
			case 'f':
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
		
	 				ToggleFullScreen();
					gbFullScreen = true;		
				}
				else
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
			case XK_S:
			  Shoulder =(Shoulder + 3) %360;
			break;
			case XK_s:
	 			Shoulder =(Shoulder - 3) %360;
			break;	
			case XK_E:
				Elbow =(Elbow + 6) %360;
			break;
			case XK_e:
				Elbow =(Elbow - 6) %360;
			break;
	         }
               }
	  break;		
	case ButtonPress:
	switch(event.xbutton.button)
	{
		case 1:
		   //WM_LBUTTONDOWN
			fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
		break;
		case 2:
		  //WM_MBUTTONDOWN
	       	fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
		break;
		case 3:
		  //WM_RBUTTONDOWN
		fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
		break;
	} 
			break;			
		break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }
   	
   display();
   //rotate();

  }

return 0;
	
}

void CreateWindow()
{
  XSetWindowAttributes winAttribs;
  int DefaultScreen;
  int DefaultDepth;
  int styleMask;

  static  int frameBufferAttributes[]=
  {
	GLX_RGBA,
	GLX_RED_SIZE,8,
	GLX_GREEN_SIZE,8,
	GLX_BLUE_SIZE,8,
	GLX_ALPHA_SIZE,1,
	GLX_DEPTH_SIZE,24,
	GLX_DOUBLEBUFFER,True,
	
	None
		
  };
 
 /*STEP 1*/
  gpDisplay = XOpenDisplay(NULL);
  if(gpDisplay == NULL)
  {
	printf("\nUnable to open Display!!!\n");
	uninitialize();
	exit(1);
  }

  /*STEP 2*/
  DefaultScreen = XDefaultScreen(gpDisplay);
  
  
  /*STEP 3*/
  DefaultDepth = DefaultDepth(gpDisplay,DefaultScreen);
  
  /*STEP 4*/
  gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while allocating memory to XVisualInfo.\Exiting\n");
	uninitialize();
 	exit(1);
  }  

  
  /*STEP 5*/
  /*XMatchVisualInfo(gpDisplay,DefaultScreen,DefaultDepth,TrueColor,gpXVisualInfo);
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while Getting Visual\n Exiting\n");
	uninitialize();
	exit(1);
  }*/
  gpXVisualInfo = glXChooseVisual(gpDisplay,DefaultScreen,frameBufferAttributes);
 
  /*SETTING WinAttributes--->Similar like filling WNDCLASSEX struct*/
  winAttribs.border_pixel = 0;
  winAttribs.background_pixmap = 0;
  winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
  gColormap = winAttribs.colormap;
  winAttribs.background_pixel = BlackPixel(gpDisplay,DefaultScreen);
  winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
  
  styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow = XCreateWindow(gpDisplay,
			  RootWindow(gpDisplay,gpXVisualInfo->screen),
			  0,
			  0,
			  giWindowWidth,
			  giWindowHeight,
		  	  0,
			  gpXVisualInfo->depth,
			  InputOutput,	/*Only InputOutput i.e. Remote client cant access this window*/
			  gpXVisualInfo->visual,
			  styleMask,
			  &winAttribs);
  if(!gWindow)
  {
	printf("\n Error while creating the window\nExiting now\n")	;
	uninitialize();
	exit(0);
  }

 XStoreName(gpDisplay,gWindow,"XLIB Perspective Triangle");

 Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XMapWindow(gpDisplay,gWindow);	//LIKE ShowWindow() and UpdateWindow();

}


void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
 
  
}

void initialize()
{
  gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);
  
  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);

  resize(giWindowHeight,giWindowHeight);
 
}

void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle +0.10f;
	if(gfUpdateYCord <= 15.0f )
	{
		gfUpdateYCord = gfUpdateYCord + 0.001f;
	}

	if(gfUpdateAngle  >=  360.0f)
	{
		gfUpdateAngle = 0;
	}
}

void RoboticArm()
{
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	glTranslatef(0.0f,0.0f,-12.0f);
	glPushMatrix();		//MODEL TRANSFORMTION	--->PUSH 1
	
	glRotatef((GLfloat)Shoulder,0.0f,0.0f,1.0f);
	glTranslatef(1.0f,0.0f,0.0f);
	
	glPushMatrix();		//FOR REVOLUTION	--->PUSH 2
	glScalef(2.0f,0.5f,1.0f);

	quadric = gluNewQuadric();
	glColor3f(0.5f,0.3f,0.05f);
	gluSphere(quadric,0.5f,10,10);
	
	glPopMatrix();		//HERE ONE POLYGON GETS DRAWED AND SAVED --
	glTranslatef(1.0f,0.0f,0.0f);
	
	glRotatef((GLfloat)Elbow,0.0f,0.0f,1.0f);
	glTranslatef(1.0f,0.0f,0.0f);
	glPushMatrix();

	glScalef(2.0f,0.5f,1.0f);
	quadric = gluNewQuadric();
	glColor3f(0.5f,0.35f,0.05f);
	gluSphere(quadric,0.5,10,10);
	glPopMatrix();
	
	glPopMatrix();


}
void display()
{
  
  glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);

  glMatrixMode(GL_MODELVIEW);
	
  glLoadIdentity();
  gluLookAt(0.0f,0.0f,5.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
  RoboticArm();
glPopMatrix();	

glXSwapBuffers(gpDisplay,gWindow);

}

void resize(int height, int width)
{
  if(height == 0)
  {
	height = 1;
  }
 glViewport(0,0,(GLsizei)width,(GLsizei)height);
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();

 //glOrtho(-5.f,5.0f,-5.0f,5.0f,-5.0f,5.0f);
 gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f); 

}

void rotate()
{
  gfRotationAngle = gfRotationAngle + 0.1f;
  if(gfRotationAngle >= 360.0f) 
  {
	gfRotationAngle = 0.0f;
  }
}

void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


