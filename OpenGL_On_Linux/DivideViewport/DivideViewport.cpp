
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

using namespace std;

/*GLOBAL VARIABLES*/
bool gbFullScreen = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;

Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;

FILE *fp = NULL;

/*OpenGL Variables*/
GLXContext gGLContext;
float gfRotationAngle = 0.0f;

/*OpenGL Rendering variables*/
float giWidthOfWindow;
float giHeightOfWindow;



int giWidthOfWindow1;
int giHeightOfWindow1;
int giLowerLeftXOfWindow = 0;
int giLowerLeftYOfWindow  = 0;

float gfangleTri;

char ascii[32] = {'\0'};

/*PROTOTYPES OF FUNCTIONS FOR WINDOWING*/
void CreateWindow(void);
void ToggleFullScreen(void);
void uninitialize(void);



/*OpenGL Functions prototype*/
void initialize(void);
void display(void);
void resize(int width, int height, int iUpperLeftXOfWindow, int iUpperLeftYOfWindow );
void rotate(void);
void DevideViewPort(int wParam, int giWidthOfWindow, int giHeightOfWindow);
int main()
{
 XEvent event;
 KeySym keysym;
 bool bDone = false;
 
 fp = fopen("Error.txt","w");
 if(fp == NULL)
 {
 	printf("\nError while opening Error file \n");
 	exit(1);
 }
 
CreateWindow();

initialize();
 while(bDone == false)
 {		
   while(XPending(gpDisplay))
   {
	XNextEvent(gpDisplay , &event);
	switch(event.type)
	{
		case MapNotify:
		  //SAME AS WM_CREATE
					fprintf(fp,"\n Inside case WM_CREATE i.e. MapNotify \n");
			break;
		case MotionNotify:
		  //FOR WM_MOUSEMOVE	
			break;
		case ConfigureNotify:
		        giWidthOfWindow1  = event.xconfigure.width;
			giHeightOfWindow1 = event.xconfigure.height;	
			resize(giWidthOfWindow1, giHeightOfWindow1,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
			
			break;
		case Expose:
		   //WM_PAINT	
			break;
	      case KeyPress:
		//WM_KEYDOWN
		keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);		//SECOND 0 is SHIFT key ON/OFF
	       if(keysym == XK_Escape)
		{
			fprintf(fp,"\n Inside XK_ESCAPE\n");
			bDone = true;
		}
	      	else
	        {
		   XLookupString(&event.xkey,ascii,sizeof(ascii),NULL,NULL);
	    	   switch(ascii[0])
		   {
	
			case 'F':
			case 'f':
				fprintf(fp,"\nInside XK_F\n");
				if(gbFullScreen == false)
				{
		
	 				ToggleFullScreen();
					gbFullScreen = true;		
				}
				else 
				{
					ToggleFullScreen();
					gbFullScreen = false;
				}
			case '0':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x30, giWidthOfWindow1, giHeightOfWindow1);
			break;
			case '1':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x31, giWidthOfWindow1, giHeightOfWindow1);
			break;
			case '2':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x32, giWidthOfWindow1, giHeightOfWindow1);
			break;
			case '3':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x33, giWidthOfWindow1, giHeightOfWindow1);
			break;
			case '4':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x34, giWidthOfWindow1, giHeightOfWindow1);
			break;
			
			case '5':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x35, giWidthOfWindow1, giHeightOfWindow1);
			break;
			
			case '6':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x36, giWidthOfWindow1, giHeightOfWindow1);
			break;
			case '7':
				fprintf(fp,"\n Inside case 0\n");
				DevideViewPort(0x37, giWidthOfWindow1, giHeightOfWindow1);
			break;
			
	          }
               }
		break;
	   case ButtonPress:
	  switch(event.xbutton.button)
	  {
				case 1:
				   //WM_LBUTTONDOWN
					fprintf(fp,"\n Inside case WM_LBUTTONDOWN \n");
					break;
				case 2:
				  //WM_MBUTTONDOWN
					fprintf(fp,"\n Inside case WM_MBUTTONDOWN \n");
					break;
				case 3:
				  //WM_RBUTTONDOWN
					fprintf(fp,"\n Inside case WM_RBUTTONDOWN \n");
				break;
	}
	break;
		
		case DestroyNotify:
			break;
		case 33:
			uninitialize();
			exit(0);
		break;
		
		default:
		break;

	}

    }
   	
   display();
   //rotate();

  }
return 0;
	
}

void CreateWindow()
{
  XSetWindowAttributes winAttribs;
  int DefaultScreen;
  int DefaultDepth;
  int styleMask;

  static  int frameBufferAttributes[]=
  {
	GLX_RGBA,
	GLX_RED_SIZE,8,
	GLX_GREEN_SIZE,8,
	GLX_BLUE_SIZE,8,
	GLX_DOUBLEBUFFER,True,
	
	None
		
  };
 
 /*STEP 1*/
  gpDisplay = XOpenDisplay(NULL);
  if(gpDisplay == NULL)
  {
	printf("\nUnable to open Display!!!\n");
	uninitialize();
	exit(1);
  }

  /*STEP 2*/
  DefaultScreen = XDefaultScreen(gpDisplay);
  
  
  /*STEP 3*/
  DefaultDepth = DefaultDepth(gpDisplay,DefaultScreen);
  
  /*STEP 4*/
  gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while allocating memory to XVisualInfo.\Exiting\n");
	uninitialize();
 	exit(1);
  }  

  
  /*STEP 5*/
  /*XMatchVisualInfo(gpDisplay,DefaultScreen,DefaultDepth,TrueColor,gpXVisualInfo);
  if(gpXVisualInfo == NULL)
  {
	printf("\nError while Getting Visual\n Exiting\n");
	uninitialize();
	exit(1);
  }*/
  gpXVisualInfo = glXChooseVisual(gpDisplay,DefaultScreen,frameBufferAttributes);
 
  /*SETTING WinAttributes--->Similar like filling WNDCLASSEX struct*/
  winAttribs.border_pixel = 0;
  winAttribs.background_pixmap = 0;
  winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),gpXVisualInfo->visual,AllocNone);
  gColormap = winAttribs.colormap;
  winAttribs.background_pixel = BlackPixel(gpDisplay,DefaultScreen);
  winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;
  
  styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

  gWindow = XCreateWindow(gpDisplay,
			  RootWindow(gpDisplay,gpXVisualInfo->screen),
			  0,
			  0,
			  giWindowWidth,
			  giWindowHeight,
		  	  0,
			  gpXVisualInfo->depth,
			  InputOutput,	/*Only InputOutput i.e. Remote client cant access this window*/
			  gpXVisualInfo->visual,
			  styleMask,
			  &winAttribs);
  if(!gWindow)
  {
	printf("\n Error while creating the window\nExiting now\n")	;
	uninitialize();
	exit(0);
  }

 XStoreName(gpDisplay,gWindow,"XLIB Perspective Triangle");

 Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
 
 XMapWindow(gpDisplay,gWindow);	//LIKE ShowWindow() and UpdateWindow();

}


void ToggleFullScreen()
{
  Atom wm_state;
  Atom fullscreen;
  XEvent xev={0};

  fprintf(fp,"\nInside ToggleFullScreen\n");

  //wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",false);
  //memset(&xev,0,sizeof(xev));

	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
  
  xev.type = ClientMessage;
  xev.xclient.window = gWindow;
  xev.xclient.message_type = wm_state;
  xev.xclient.format=32;
  xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
  
  fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",false);
  xev.xclient.data.l[1]=fullscreen;
  
  XSendEvent(gpDisplay,
	     RootWindow(gpDisplay,gpXVisualInfo->screen),
	     False,
	     StructureNotifyMask,  
	     &xev);
 
  
}

void initialize()
{
  gGLContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
 
  glXMakeCurrent(gpDisplay,gWindow,gGLContext);
  
  glClearColor(0.0f,0.0f,0.0f,0.0f);
  
	glClearDepth(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);

			resize(giWidthOfWindow1, giHeightOfWindow1,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
 
}
void DevideViewPort(int wParam, int giWidthOfWindow, int giHeightOfWindow)
{
    fprintf(fp,"\n Inside DevideViewPort\n");
	
	 if(wParam == 0x30)
		{
			/*-------------------- LEFT HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			//giHeightOfWindow = giHeightOfWindow;
			giLowerLeftXOfWindow = 0;
			giLowerLeftYOfWindow = 0;
						resize(giWidthOfWindow, giHeightOfWindow1,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x31)
		{
			/*-------------------- RIGHT HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			giLowerLeftXOfWindow = giWidthOfWindow;
			giLowerLeftYOfWindow = 0;
						resize(giWidthOfWindow, giHeightOfWindow1,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x32)
		{
			/*-------------------- UPPER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftYOfWindow = giHeightOfWindow;
			giLowerLeftXOfWindow = 0;
			
						resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x33)
		{
			/*-------------------- LOWER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftXOfWindow = 0;
			giLowerLeftYOfWindow = 0;
			
						resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x34)
		{
			/*-------------------- LEFT UPPER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftXOfWindow = giLowerLeftXOfWindow;			/*Because the x of glViewPort is Lower Left X of Window*/	
			giLowerLeftYOfWindow = giHeightOfWindow;				/*Because the y of glViewPort is Lower Left Y of Window*/

						resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x35)
		{
			/*-------------------- RIGHT UPPER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftXOfWindow = giWidthOfWindow;					/*Because the x of glViewPort is Lower Left X of Window*/
			giLowerLeftYOfWindow = giHeightOfWindow;				/*Because the y of glViewPort is Lower Left Y of Window*/

						resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x36)
		{
			/*-------------------- LEFT LOWER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftXOfWindow = 0;								/*Because the x of glViewPort is Lower Left X of Window*/
			giLowerLeftYOfWindow = 0;								/*Because the y of glViewPort is Lower Left Y of Window*/

						resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
		else if(wParam == 0x37)
		{
			/*-------------------- RIGHT LOWER HALF ----------------------*/
			giWidthOfWindow = giWidthOfWindow/2;
			giHeightOfWindow= giHeightOfWindow/2;
			giLowerLeftXOfWindow = giWidthOfWindow;					/*Because the x of glViewPort is Lower Left X of Window*/
			giLowerLeftYOfWindow = 0;								/*Because the y of glViewPort is Lower Left Y of Window*/

	        	resize(giWidthOfWindow, giHeightOfWindow,giLowerLeftXOfWindow, giLowerLeftYOfWindow);
		}
}

void resize(int width, int height, int iUpperLeftXOfWindow, int iUpperLeftYOfWindow )
{
  fprintf(fp,"\nInside resize\n");

	fprintf(fp,"\nwidth:%d \theight:%d\tUpperLeftXOfWindow:%d\tiUpperLeftYOfWindow:%d\t\n",width,height,iUpperLeftXOfWindow,iUpperLeftYOfWindow);
	if(height == 0)
	{
		height = 1;
	}	                         
                              

	glViewport((GLsizei)iUpperLeftXOfWindow,(GLsizei)iUpperLeftYOfWindow,(GLsizei)width,(GLsizei)height);
	//glViewport((GLsizei)iUpperLeftXOfWindow,(GLsizei)height,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);



     /*if(height == 0)
     {
		height = 1;
     }


	 glViewport(0,0,(GLsizei)width,(GLsizei)height);
	 glMatrixMode(GL_PROJECTION);
	 glLoadIdentity();

	
	 gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f); 
	*/
	return;
}


void DrawColoredTriangle()
{
  fprintf(fp,"\nInside DrawColoredTriangle()\n");
	glBegin(GL_TRIANGLES);

		glColor3f(0.50f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);

		glColor3f(0.0f,0.8f,0.0f);
		glVertex3f(- 1.0,- 1.0,0.0f);

		glColor3f(0.0f,0.0f,0.6f);
		glVertex3f( 1.0,- 1.0,0.0f);

		
	glEnd();
	return;
}

void UpdateAngle()
{
       fprintf(fp,"\nInside UpdateAngle()\n");
	gfangleTri = gfangleTri +0.10f;
	if(gfangleTri  >=  360.0f)
	{
		gfangleTri = 0;
	}
}

void display()
{
	  fprintf(fp,"\nInside display\n");
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-3.0f);
	glRotatef(gfangleTri,0.0f, 1.0f, 0.0f);
	DrawColoredTriangle();
	
	UpdateAngle();
glXSwapBuffers(gpDisplay,gWindow);
	return;
}

void uninitialize()
{
 GLXContext currentGLXContext ;
 
 currentGLXContext =glXGetCurrentContext();

 if((currentGLXContext != NULL) && (currentGLXContext == gGLContext))
 {
	glXMakeCurrent(gpDisplay,0,0);
 }

 if(gGLContext)
 {
   glXDestroyContext(gpDisplay,gGLContext);
 } 
  if(gWindow)
  {
 	XDestroyWindow(gpDisplay,gWindow);
  }
  if(gColormap)
  {
	XFreeColormap(gpDisplay,gColormap);
  }
  if(gpXVisualInfo != NULL)
  {
	free(gpXVisualInfo);
	gpXVisualInfo = NULL;
  }
  if(gpDisplay != NULL)
  {
	XCloseDisplay(gpDisplay);
	gpDisplay = NULL;
  }
  if(fp != NULL)
 {
	fprintf(fp,"\nTerminating Successfully\n");
 	fclose(fp);
 }

}


