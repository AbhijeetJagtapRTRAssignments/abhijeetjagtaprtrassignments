#import "AppDelegate.h"
#import "ViewController.h"
#import "GLESView.h"


@implementation AppDelegate
{
@private 
	UIWindow *mainWindow;
	UIViewController *mainViewController;
	GLESView *myView;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
   
    CGRect screenBounds = [ [UIScreen mainScreen]bounds ];
    
    mainWindow = [[UIWindow alloc] initWithFrame:screenBounds];
    
    mainViewController = [[ViewController alloc]init];
    
    [mainWindow setRootViewController:mainViewController];
    
    myView=[[ GLESView alloc ] initWithFrame:screenBounds ];
    
    [mainViewController setView:myView];

    [myView release];
    
    [mainWindow addSubview:[mainViewController view]];
    
    [mainWindow makeKeyAndVisible];
    
    [myView startAnimation];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
   
    [myView stopAnimation];
}


- (void)applicationDidEnterBackground:(UIApplication *)application 
{
   
}


- (void)applicationWillEnterForeground:(UIApplication *)application 
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [myView startAnimation];
}


- (void)applicationWillTerminate:(UIApplication *)application 
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [myView stopAnimation];
}

-(void)dealloc
{
    [ myView release ];

    [ mainViewController release ];
    
    [ mainWindow release];
    
    [super dealloc];
}

@end
