#import <OpenGLES/ES3/gl.h>

#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

enum 
{
    VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE
};

@implementation GLESView
{
	EAGLContext *eaglContext;
	
	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;
	
	//OPENGL VARIABLES
    GLuint gVao_Triangle;
    GLuint gVao_Rectangle;
    GLuint gVbo_Position;
    GLuint gVbo_Color_Triangle;
    GLuint gVbo_Color_Rectangle;
    GLuint gMVPUniform;
    
    float updateAngle;
    
    mat4 gPerspectiveProjectionMatrix;
    mat4 gRotationMatrix;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
}

-(id) initWithFrame:(CGRect)frame	//initialize()
{
    self = [super initWithFrame:frame];
    
    if (self )
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[ NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat ,nil ];
        
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if (eaglContext==nil)
        {
            [self release];
            return nil;
        }
        [EAGLContext setCurrentContext:eaglContext];
     
        glGenFramebuffers(1,&defaultFrameBuffer);
        glGenRenderbuffers(1,&colorRenderBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFrameBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderBuffer);

        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
        
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            //[self release];
            return nil;
        }
        
        printf("Renderer : %s |\n GL Version : %s |\n GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
  /*START THE OPENGL CODE*/
   gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 outColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outColor=vColor;"\
		"}";
		
		

	glShaderSource(gVertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;
	
	glGetShaderiv(gVertexShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\nVERTEX SHADER COMPILATION LOG:%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                exit(0);
			}
		}
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *FragmentShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 outColor;"\
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = outColor;" \
		"}";

		
	glShaderSource(gFragmentShaderObject,1,(const GLchar **)&FragmentShaderSourceCode,NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompileStatus);
	if(iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\n FRAGMENT SHADER COMPILATION LOG:%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                exit(0);
				exit(0);
		    }
	     }
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();
	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");
	
	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject,GL_LINK_STATUS,&iShaderLinkStatus);
	if(iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLength);
		if(iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject,iInfoLength,&written,szInfoLog);
				fprintf(fp,"\n LINK TIME ERROR LOG: \n%s \n",szInfoLog);
				free(szInfoLog);
                [self release];
                exit(0);
				exit(0);
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
//START THE BINDING OF THE POSITION AND COLORE VBOs

const GLfloat triangleVertices[] =
	{
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};
	const GLfloat triangleColor[] =
	{
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f
	};

	//START THE RECORDER	---SAME AS glBegin()
	glGenVertexArrays(1, &gVao_Triangle);
	glBindVertexArray(gVao_Triangle);

	//CREATE THE OUR SLOT OF VERTEX AND COLOR MEMORY INSIDE GL_ARRAY_BUFFER WHICH IS INSIDE FRAMR BUFFER
	glGenBuffers(1, &gVbo_Position);
	

	//BIND TO TH VERTEX_BUFFER AND TRANSFER DATA TO IT AT RUNTIME
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM VERTEX BUFFER

	//CREATE AND BIND TO THE COLOR BUFFER
	glGenBuffers(1, &gVbo_Color_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color_Triangle);

		glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
		glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM COLOR BUFFER

	//END OF RECORDER ----SAME AS glEnd()
	glBindVertexArray(0);



	const GLfloat rectangleVertices[] =
	{ 
		-1.0f,1.0f,0.0f,
		-1.0f,-1.0f,0.0f,
		 1.0f,-1.0f,0.0f,
		 1.0f,1.0f,0.0f
	};

	const GLfloat rectangleColor[] =
	{
		0.392f,0.5843f,0.9294f,
		0.392f,0.5843f,0.9294f,
		0.392f,0.5843f,0.9294f,
		0.392f,0.5843f,0.9294f
	};

	//START THE RECORDER	---SAME AS glBegin() for RECTANGLE
	glGenVertexArrays(1, &gVao_Rectangle);
	glBindVertexArray(gVao_Rectangle);

	//CREATE THE OUR SLOT OF MEMORY INSIDE GL_ARRAY_BUFFER WHICH IS INSIDE FRAMR BUFFER
		glGenBuffers(1, &gVbo_Position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	 //UNBIND FROM VERTEX BUFFER

	//CREATE AND BIND TO THE COLOR BUFFER
		glGenBuffers(1, &gVbo_Color_Rectangle);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color_Rectangle);

		glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleColor), rectangleColor, GL_STATIC_DRAW);
		glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM COLOR BUFFER


	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()


        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        isAnimating=NO;
        animationFrameInterval=60;
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        gPerspectiveProjectionMatrix = mat4::identity();
      
        UITapGestureRecognizer *singleTapGestureRecognizer = [ [ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(onSingleTap:)] ;
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; 
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipGestureRecognizer = [ [UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:) ] ;
        [self addGestureRecognizer:swipGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecognizer = [ [UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:) ];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
    }
    
    return (self);
    
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}

-(void)drawView:(id)sender		//Display()
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
 //START OF ONLINE RENDERING
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	vmath::mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
    gRotationMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-2.0f, 0.0f, -6.0f);
    gRotationMatrix = vmath::rotate(updateAngle, 0.0f, 1.0f, 0.0f);
    
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
    
    modelViewProjectionMatrix = modelViewProjectionMatrix * gRotationMatrix;
    
	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);


	glBindVertexArray(gVao_Triangle);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);

	//NOW DRAW RECTANGLE BEFORE THAT MAKE IDENTITY TO modelViewMatrix and modelViewProjectionMatrix
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
    gRotationMatrix = vmath::mat4::identity();
    
    gRotationMatrix = vmath::rotate(updateAngle, 1.0f, 0.0f, 0.0f);
    
    modelViewMatrix = vmath::translate(2.0f, 0.0f, -6.0f);
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;
modelViewProjectionMatrix = modelViewProjectionMatrix * gRotationMatrix;	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao_Rectangle);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

glUseProgram(0);

    updateAngle = updateAngle + 0.51f;
    if(updateAngle >= 360.0f)
    {
        updateAngle = 0.0f;
    }

//END OF ONLINE RENDERING


    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)layoutSubviews
{
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to Create Complete FrameBuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    if (width <= height)
    {
        gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f);
    }
    else
    {
        if (width == 0)
            width = 1;
       gPerspectiveProjectionMatrix = perspective(45.0f,(float)width/float(height),0.1f,100.0f); 
    }   

    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if (isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

-(BOOL) acceptsFirstResponder
{
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
       
}


-(void) onLongPress: (UIGestureRecognizer *)gr
{

}

-(void) onSwipe: (UIGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void) onDoubleTap: (UITapGestureRecognizer *)gr
{
   
}

-(void) onSingleTap: (UITapGestureRecognizer *)gr
{
   
}

-(void)dealloc
{
  if(gVbo_Color_Rectangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Rectangle);
		gVbo_Color_Rectangle = 0;
	}
	if(gVbo_Color_Triangle)
	{
		glDeleteVertexArrays(1,&gVbo_Color_Triangle);
		gVbo_Color_Triangle = 0;
	}
	if(gVbo_Position)
	{
		glDeleteVertexArrays(1,&gVbo_Position);
		gVbo_Position = 0;
	}
	if(gVao_Triangle)
	{
		glDeleteVertexArrays(1,&gVao_Triangle);
		gVao_Triangle = 0;
	}
	if(gVao_Rectangle)
	{
		glDeleteVertexArrays(1,&gVao_Rectangle);
		gVao_Rectangle = 0;
	}

	glDetachShader(gShaderProgramObject,gVertexShaderObject);
	glDetachShader(gShaderProgramObject,gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject =0;

	
    if (depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if (colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if (defaultFrameBuffer)
    {
        glDeleteRenderbuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    if ([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];

    eaglContext=nil;
    
    [super dealloc];
}

@end
