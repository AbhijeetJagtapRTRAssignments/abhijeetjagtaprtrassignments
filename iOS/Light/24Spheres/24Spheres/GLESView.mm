#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h> 

#import "GLESView.h"

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX=0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};


@implementation GLESView
{
	EAGLContext *eaglContext;

	GLuint defaultFramebuffer;
	GLuint colorRenderbuffer;
	GLuint depthRenderbuffer;

	id displayLink;
	NSInteger animationFrameInterval;
	BOOL isAnimating;

	GLuint gVao_Cube;
	GLuint gVbo_Vertex_Cube;
	GLuint gVbo_Normal_Cube;

	GLuint gVao_Sphere;
	GLuint gVbo_Vertex_Sphere;
	GLuint gVbo_Normal_Sphere;
	GLuint gVbo_Sphere_Element;

	GLuint gMVPUniform;


	mat4 gPerspectiveProjectionMatrix;
	mat4 gRotationMatrix;
	mat4 gScaleMatrix;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	float gfUpdateAngle = 0.0f;

	//SPHERE RELATED VARIABLES
	float sphere_vertices[1146];
	float sphere_normals[1146];
	float sphere_textures[764];
	unsigned short sphere_elements[2280];
	unsigned int gNumVertices, gNumElements;

	//LIGHT RELATED VARIABLES

	GLuint  gProjectionMatrixUniform;
	GLuint  gModelMatrixUniform;
	GLuint  gViewMatrixUniform;

	GLuint gLa_Uniform;
	GLuint gLd_Uniform;
	GLuint gLs_Uniform;

	GLuint gKa_Uniform;
	GLuint gKd_Uniform;
	GLuint gKs_Uniform;
	GLuint gMaterial_Shinyness_Uniform;

	GLuint gLKeyPressedUniform;
	GLuint gLight_Position_Uniform;

	GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat lightDiffused[] = { 1.0f, 1.0f ,1.0f ,1.0f };
	GLfloat LightSpecular[] = { 1.0f ,1.0f, 1.0f ,1.0f };
	GLfloat lightPosition[] = { 50.0f, 50.0f, 150.0f, 1.0f };

	/*GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0 };
	GLfloat materialtDiffused[] = { 1.0f, 1.0f ,1.0f ,1.0f };
	GLfloat materialSpecular[] = { 1.0f ,1.0f, 1.0f ,1.0f };
	*/
	GLfloat materialShininess = 70.0f;

	bool gbAnimate = FALSE;
	bool gbLight = FALSE;


	float gfXAngle = 0.0f;
	float gfZAngle = 0.0f;
	float gfYAngle = 0.0f;
	float gfRadiousOfLightRotation = 85.0f;

	int iBottom_Left_X_Of_Window = 0, iBottom_Left_Y_Of_Window = 0;

	GLfloat material_ambient[][4] = { { 0.0215f,0.1745f,0.0215f,1.0f },{ 0.135f,0.2225f,0.1575f,1.0f },{ 0.05375f,0.05f,0.06625f,1.0f },
	{ 0.25f,0.20725f,0.20725f,1.0f },{ 0.1745f,0.01175f,0.01175f,1.0f },{ 0.1f,0.18725f,0.1745f,1.0f },
	{ 0.329412f,0.223529f,0.027451f,1.0f },{ 0.2125f,0.1275f,0.054f,1.0f },{ 0.25f,0.25f,0.25f,1.0f },
	{ 0.19125f,0.0735f,0.0225f,1.0f },{ 0.24725f,0.1995f,1.0f },{ 0.19225f,0.19225f,0.19225f,1.0f },
	{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.1f,0.06f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.02f,0.02f,0.02f,1.0f },{ 0.00f,0.05f,0.05f,1.0f },{ 0.0f,0.05f,0.0f },
	{ 0.5f,0.0f,0.0f,1.0f },{ 0.05f,0.05f,0.05f,1.0f },{ 0.05f,0.05f,0.00f,1.0f } };


	GLfloat material_diffused[][4] = { { 0.07568f,0.61424f,0.07568f,1.0f },{ 0.54f,0.89f,0.63f,1.0f },{ 0.18275f,0.17f,0.22525f,1.0f },
	{ 1.0f,0.829f,0.829f,1.0f },{ 0.61424f,0.04136f,0.04136f,1.0f } ,{ 0.396f,0.74151f,0.69102f,1.0f },
	{ 0.780392f,0.568627f,0.113725f,1.0f },{ 0.714f,0.4284f,0.18144f,1.0f },{ 0.4f,0.4f,0.4f,1.0f },
	{ 0.7038f,0.27048f,0.0828f,1.0f },{ 0.75164f,0.60648f,0.22648f,1.0f },{ 0.01f,0.01f,0.01f,1.0f },
	{ 0.01f,0.01f,0.01f,1.0f },{ 0.0f,0.50980392f,0.50980392f,1.0f },{ 0.1f,0.35f,0.1f,1.0f },
	{ 0.5f,0.0f,0.0f,1.0f },{ 0.550f,0.550f,0.550f,1.0f },{ 0.50f,0.50f,0.0f,1.0f },
	{ 0.01f,0.01f,0.01f,1.0f },{ 0.4f,0.5f,0.5f,1.0f },{ 0.40f,0.05f,0.40f,1.0f },
	{ 0.5f,0.40f,0.40f,1.0f },{ 0.5f,0.5f,0.5f,1.0f },{ 0.5f,0.5f,0.4f,1.0f } };


	GLfloat material_specular[][4] = { { 0.633f,0.727811f,0.633f,1.0f },{ 0.316228f,0.316228f,0.316228f },{ 0.332741f,0.328634f,0.346435f,1.0f },
	{ 0.296648f,0.296648f,0.296648f,1.0f },{ 0.72781f,0.626959f,0.626959f,1.0f },{ 0.297254f,0.30829f,0.306678f,1.0f },
	{ 0.992157f,0.941176f,0.807843f,1.0f },{ 0.393546f,0.271906f,0.166721f,1.0f },{ 0.774597f,0.774597f,0.774597f,1.0f },
	{ 0.256777f,0.137622f,0.086014f,1.0f },{ 0.628281f,0.555802f,0.366065f,1.0f },{ 0.5f,0.5f,0.5f,1.0f },
	{ 0.50f,0.5f,0.50f,1.0f },{ 0.50196078f,0.50196078f,0.50196078f,1.0f },{ 0.45f,0.55f,0.45f,1.0f },
	{ 0.70f,0.6f,0.6f,1.0f },{ 0.70f,0.70f,0.70f,1.0f },{ 0.60f,0.60f,0.60f,1.0f },
	{ 0.4f,0.4f,0.4f,1.0f },{ 0.04f,0.7f,0.7f,1.0f },{ 0.04f,0.7f,0.04f,1.0f },
	{ 0.7f,0.04f,0.04f,1.0f },{ 0.7f,0.7f,0.7f,1.0f },{ 0.7f,0.7f,0.04f,1.0f } };


	GLfloat material_shinyness[][1] = { { (0.6f*128.0f) },{ 0.6f*128.0f },{ 0.3f*128.0f },{ 0.088f*128.0f },{ 0.6f*128.0f },{ 0.1f*128.0f },
	{ 0.21794872f*128.0f },{ 0.2f*128.0f },{ 0.6f*128.0f },{ 0.1f*128.0f },{ 0.4f*128.0f },{ 0.25f*128.0f },
	{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },
	{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f } };

	BOOL gbIsXKeypressed = FALSE;
	BOOL gbIsYKeypressed = FALSE;
	BOOL gbIsZKeypressed = FALSE;

	float gfAngleOfLight = 0.0f;
	float gfAngleY = 0.0f;
	float gfAngleZ = 0.0f;

	GLfloat gfRightX = -9.0f;
	GLfloat gfTopY = 5.0f;
	GLfloat gfDistanceAtX = 6.0f;
	GLfloat gfDistanceAtY = 2.0f;
	GLfloat gfPositionX = 0.0f, gfPositionY = 0.0f;

} 

-(id) initWithFrame:(CGRect) frame
{
	self = [super initWithFrame:frame];

	if(self)
	{
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *) super.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys : 
																					[NSNumber numberWithBool:FALSE ],
																					kEAGLDrawablePropertyRetainedBacking,
																					kEAGLColorFormatRGBA8,
																					kEAGLDrawablePropertyColorFormat,
																					nil
																					];
		eaglContext =[[EAGLContext alloc]initWithAPI : kEAGLRenderingAPIOpenGLES3];
		
		if(eaglContext == nil)
		{
			[self release];

			return(nil);
		}

		[EAGLContext setCurrentContext:eaglContext];

		glGenFramebuffers(1,&defaultFramebuffer);
		glGenRenderbuffers(1,&colorRenderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

		[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);

		GLint backingWidth;
		GLint backingHeight;

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,,&backingWidth);

		glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,,&backingHeight);

		glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Failed to create complete frame buffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));

			glDeleteFramebuffers(1,&defaultFramebuffer);
			glDeleteRenderbuffers(1,&colorRenderbuffer);
			glDeleteRenderbuffers(1,&depthRenderbuffer);

			return(nil);
		}

		printf("Renderer is %s | GL Version:%s | GLSL Version:%s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));

		isAnimating = NO;
		animationFrameInterval=60;  

		vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
			"#version 410 core" \
			"\n" \
			"in vec4 vPosition;" \
			"in vec3 vNormal;"\
			"uniform mat4 u_model_matrix;" \
            "uniform mat4 u_view_matrix;" \
			"uniform mat4 u_projection_matrix;" \
			"uniform int u_LKeyPressed;"\
			"uniform vec4 u_light_position;"\
            "out vec3 transformed_normals;"\
            "out vec3 light_direction;"\
            "out vec3 viewer_vector;"\
			"void main(void)" \
			"{" \
			"if (u_LKeyPressed == 1)"\
			"{"\
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition ;" \
            "transformed_normals = mat3 (u_view_matrix * u_model_matrix) * vNormal ;" \
            "light_direction = vec3 (u_light_position) - eyeCoordinates.xyz ;" \
            "viewer_vector = -eyeCoordinates.xyz;"\
			"}"\
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;"\
			"}";
		glShaderSource(vertexShaderObject,1,(const GLchar**) &vertexShaderSourceCode,NULL);

		glCompileShader(vertexShaderObject);
        GLint iInfoLogLength =0;
        GLint iShaderCompiledStatus =0;
		char *szInfoLog = NULL;

		glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
		if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength>0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(vertexShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Vertex Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}
		iInfoLogLength =0;
		iShaderCompiledStatus =0;
		szInfoLog = NULL;

		fragmentShaderObject= glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
			"#version 410"\
			"\n" \
            "uniform int u_LKeyPressed;"\
			"in vec3 transformed_normals;"\
            "in vec3 light_direction;"\
            "in vec3 viewer_vector;"\
            "out vec4 FragColor;" \
            "uniform vec3 u_La;"\
            "uniform vec3 u_Ld;"\
            "uniform vec3 u_Ls;"\
            "uniform vec3 u_Ka;"\
            "uniform vec3 u_Kd;"\
            "uniform vec3 u_Ks;"\
            "uniform float u_material_shininess;"\
			"void main(void)" \
			"{" \
            "vec3 phong_ads_color;"\
            "if(u_LKeyPressed == 1)"\
            "{"\
            "vec3 normalized_transformed_normal = normalize(transformed_normals) ;"+
            "vec3 normalized_light_direction = normalize(light_direction);"+
            "vec3 normalized_viewer_vector = normalize(viewer_vector);"+
            "vec3 ambient = u_La * u_Ka;"+
            "float tn_dot_ld = max(dot(normalized_transformed_normal,normalized_light_direction),0.0);"+
            "vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"+
            "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normal);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"+
            "phong_ads_color = ambient + diffuse + specular ;"+
            "}"+
            "else"+
            "{"+
            "phong_ads_color = vec3(1.0,1.0,1.0) ;"+
             "}"+
			"FragColor = vec4(phong_ads_color,1.0) ;" \
			"}" ;

		glShaderSource(fragmentShaderObject,1,(const GLchar **)&fragmentShaderSourceCode,NULL);
        
		glCompileShader(fragmentShaderObject);
		glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);

        if(iShaderCompiledStatus == GL_FALSE)
		{
			glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);

			if(iInfoLogLength >0)
			{
				szInfoLog = (char *) malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog);
					printf("Fragment Shader Compilation Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

		shaderProgramObject = glCreateProgram();
		glAttachShader(shaderProgramObject,vertexShaderObject);
		glAttachShader(shaderProgramObject,fragmentShaderObject);
		glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_VERTEX,"vPosition");
		glBindAttribLocation(shaderProgramObject,VDG_ATTRIBUTE_NORMAL,"vNormal");
		glLinkProgram(shaderProgramObject);
		GLint iShaderProgramLinkStatus =0;
		glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);

        
		if(iShaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
			
			if(iInfoLogLength >0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);

				if(szInfoLog != NULL)
				{
					GLsizei written;
					glGetProgramInfoLog(shaderProgramObject,iInfoLogLength,&written,szInfoLog);
					printf("Shader Program Link Log : %s \n",szInfoLog);
					free(szInfoLog);
					[self release];
				}
			}
		}

	 LKeyPressedUniform = glGetUniformLocation(shaderProgramObject,"u_LKeyPressed");
	 modelMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
     viewMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix"); 
     projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix"); 
     laUniform = glGetUniformLocation(shaderProgramObject, "u_La"); 
     ldUniform = glGetUniformLocation(shaderProgramObject, "u_Ld"); 
     lsUniform = glGetUniformLocation(shaderProgramObject, "u_Ls"); 
     lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position"); 
     kaUniform = glGetUniformLocation(shaderProgramObject, "u_Ka"); 
     kdUniform = glGetUniformLocation(shaderProgramObject, "u_Kd"); 
     ksUniform = glGetUniformLocation(shaderProgramObject, "u_Ks"); 
     materialShininessUniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess"); 

    
	//Sphere sphere = new Sphere();
    float sphere_vertices[] = new float[1146];
    float sphere_normals[] = new float[1146];
    float sphere_textures[] = new float[764];
    short sphere_elements[] = new short[2280];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

    numVertices = getNumberOfSphereVertices();
    numElements = getNumberOfSphereElements();


    glGenVertexArrays(1,&vao_Sphere);
    glBindVertexArray(vao_Sphere);


    glGenBuffers(1,& vbo_Sphere_Position);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Position);
    glBufferData(GL_ARRAY_BUFFER,sphere_vertices.length * 4,sphere_vertices,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

    glBindBuffer(GL_ARRAY_BUFFER,0); 
    
    glGenBuffers(1,& vbo_Sphere_Normal);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_Sphere_Normal);
    glBufferData(GL_ARRAY_BUFFER,sphere_normals.length * 4,sphere_normals,GL_STATIC_DRAW);

    glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL,3,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

    glBindBuffer(GL_ARRAY_BUFFER,0); 

	glGenBuffers(1,& vbo_Sphere_Elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_Sphere_Elements);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length*2,sphere_elements,GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0); // unbind sphere element Vbo

    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f,0.0f,1.0f,1.0f);
	perspectiveProjectionMatrix = vmath::mat4::identity();

    UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)]; // callback is given by @selector

	[singleTapGestureRecognizer setNumberOfTapsRequired:1];
	[singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
	[singleTapGestureRecognizer setDelegate:self];
	[self addGestureRecognizer:singleTapGestureRecognizer];


	UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)]; // callback is given by @selector

	[doubleTapGestureRecognizer setNumberOfTapsRequired:2];
	[doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger
	[doubleTapGestureRecognizer setDelegate:self];
	[self addGestureRecognizer:doubleTapGestureRecognizer];

	[singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];

	UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
	[self addGestureRecognizer:swipeGestureRecognizer];

	UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
	[self addGestureRecognizer:longPressGestureRecognizer];

	}

	return(self);

}


+(Class)layerClass
{
	return([CAEAGLLayer class]);
}


-(void) drawView:(id) sender
{
	[EAGLContext setCurrentContext:eaglContext];

	glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);

	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelMatrix = mat4::identity();
	mat4 ViewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLa_Uniform, 1, lightAmbient);
		glUniform3fv(gLd_Uniform, 1, lightDiffused);
		glUniform3fv(gLs_Uniform, 1, LightSpecular);

		if (gbIsXKeypressed)
		{
			lightPosition[0] = 0.0f;
			lightPosition[1] = gfRadiousOfLightRotation*cos(gfXAngle);
			lightPosition[2] = gfRadiousOfLightRotation*sin(gfXAngle);
		}
		else if (gbIsYKeypressed)
		{
			lightPosition[1] = 0.0f;
			lightPosition[0] = gfRadiousOfLightRotation*cos(gfYAngle);
			lightPosition[2] = gfRadiousOfLightRotation*sin(gfYAngle);
		}
		else if (gbIsZKeypressed)
		{
			lightPosition[2] = 0.0f;
			lightPosition[0] = gfRadiousOfLightRotation*cos(gfZAngle);
			lightPosition[1] = gfRadiousOfLightRotation*sin(gfZAngle);
		}
		glUniform4fv(gLight_Position_Uniform, 1, lightPosition);

		/*glUniform3fv(gKa_Uniform, 1, materialAmbient);
		glUniform3fv(gKd_Uniform, 1, materialtDiffused);
		glUniform3fv(gKs_Uniform, 1, materialSpecular);
		glUniform1f(gMaterial_Shinyness_Uniform, materialShininess);*/
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	modelMatrix = translate(0.0f, 0.0f, -10.0f);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);

	gfPositionY = gfTopY;
	gfPositionX = gfRightX;
	//glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	iBottom_Left_X_Of_Window = 0;
	iBottom_Left_Y_Of_Window = 0.0f;

	for (int j = 0; j < 6; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			materialShininess = material_shinyness[i][j];
			glUniform3fv(gKa_Uniform, 1, material_diffused[(j * 4) + i]);
			glUniform3fv(gKd_Uniform, 1, material_ambient[(j * 4) + i]);
			glUniform3fv(gKs_Uniform, 1, material_specular[(j * 4) + i]);
			glUniform1f(gMaterial_Shinyness_Uniform, material_shinyness[i][j]);
			modelMatrix = mat4::identity();
			modelMatrix = translate(0.0f,0.0f, -2.0f);
			resize((WIN_WIDTH / 3), (WIN_HEIGHT / 5), iBottom_Left_X_Of_Window, iBottom_Left_Y_Of_Window);
			glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
			
			iBottom_Left_X_Of_Window = iBottom_Left_X_Of_Window + (WIN_WIDTH / 3) + 90;			
		}
		iBottom_Left_Y_Of_Window = iBottom_Left_Y_Of_Window + (WIN_HEIGHT / 5) + 10;
		iBottom_Left_X_Of_Window = 0;
	}

	glBindVertexArray(0);

	glUseProgram(0);
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	[self updateAngle];
}

-(void) drawSphere
{
    
    glUniform3fv(kaUniform, 1, material_ambient);
    glUniform3fv(kdUniform, 1, material_diffuse);
    glUniform3fv(ksUniform, 1, material_specular);
    glUniform1f(materialShininessUniform, material_shinyness);

    
    glBindVertexArray(vao_Sphere);

	// Draw Sphere

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Elements);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT); 

	glBindVertexArray(0);

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext presentRenderbuffer:GL_RENDERBUFFER];

	[self updateAngle];

}

-(void) updateAngle
{
	anglePyramid = anglePyramid - 0.7f;
	if (anglePyramid <= 0.0f)
		anglePyramid = 360.0f;
}

-(void)layoutSubviews
{
	GLint width;
	GLint height;

	glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);

	[eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);

	glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);

	glGenRenderbuffers(1,&depthRenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);

	glViewport(0,0,width,height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f,(GLfloat) width / (GLfloat) height,0.1f,100.0f);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		printf("Failed to create complete frame buffer object %x",glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

	[self drawView:nil];
}

-(void) startAnimation
{
	if(!isAnimating)
	{
		displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];

		[displayLink setPreferredFramesPerSecond : animationFrameInterval];

		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

		isAnimating = YES;
	}
}

-(void) stopAnimation
{
	if(isAnimating)
	{
		[displayLink invalidate];

		displayLink = nil;

		isAnimating = NO;
	}
}

-(BOOL) acceptsFirstResponder
{
	return(YES);
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{

}

-(void) onSingleTap:(UITapGestureRecognizer *)gr
{
//   if(isAnimating)
//     isAnimating = NO;
//   else
//     isAnimating = YES;	
}

-(void) onDoubleTap:(UITapGestureRecognizer *) gr
{
		// doubleTap ++;
		// if(doubleTap > 1)
		//   doubleTap =0;

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
	[self release];

	exit(0);
}

-(void) onLongPress :(UILongPressGestureRecognizer *)gr
{

}

-(void) dealloc
{
 
    if(vao_Pyramid)
	{
		glDeleteVertexArrays(1,&vao_Pyramid);
		vao_Pyramid =0;
	}

	if(vbo_Pyramid_Position)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Position);
		vbo_Pyramid_Position =0;
	}

	if(vbo_Pyramid_Normal)
	{
		glDeleteBuffers(1,&vbo_Pyramid_Normal);
		vbo_Pyramid_Normal =0;
	}

	glDetachShader(shaderProgramObject,vertexShaderObject);

	glDetachShader(shaderProgramObject,fragmentShaderObject);

	glDeleteShader(vertexShaderObject);
	vertexShaderObject =0;

	glDeleteShader(fragmentShaderObject);
	fragmentShaderObject=0;

	glDeleteProgram(shaderProgramObject);
	shaderProgramObject=0;


	if(depthRenderbuffer)
	{
		glDeleteRenderbuffers(1,&depthRenderbuffer);
		depthRenderbuffer=0;
	}

	if(colorRenderbuffer)
	{
		glDeleteRenderbuffers(1,&colorRenderbuffer);
		colorRenderbuffer=0;
	}

	if(defaultFramebuffer)
	{
		glDeleteRenderbuffers(1,&defaultFramebuffer);
		defaultFramebuffer=0;
	}

	if([EAGLContext currentContext]==eaglContext)
	{
		[EAGLContext setCurrentContext:nil];
	}

	[eaglContext release];
	eaglContext = nil;

	[super dealloc];
}

@end
