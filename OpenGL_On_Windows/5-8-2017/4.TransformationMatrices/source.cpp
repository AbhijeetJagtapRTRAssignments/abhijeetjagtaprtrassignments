#include<Windows.h>
#include<stdio.h>
#include<math.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600
#define PI 3.1415926535898

float gfUpdateAngle;
float gfUpdateAngleInRadian;
float gfUpdateYCord = 0;

int giHeightOfWindow = 0;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;


void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

void DrawColoredPyramid();
void DrawColoredCube();
void UpdateAngle();

/*-------------------     TRANSFORMATION MATRICES ----------------------*/
GLfloat gIdentityMatrix[16];
GLfloat gTranslationMatrix[16];
GLfloat gScaleMatrix[16];
GLfloat gRotation_XMatrix[16];
GLfloat gRotation_YMatrix[16];
GLfloat gRotation_ZMatrix[16];

/*-------------------     TRANSFORMATION RELATED USER DEFINED FUNCTIONS ----------------------*/
void Identity();
void Scale(GLfloat glfScaleX, GLfloat glfScaleY, GLfloat glfScaleZ);
void Translate(GLfloat gfTranslateX,GLfloat gfTranslateY, GLfloat gfTranslateZ );
void RotateX();
void RotateY();
void RotateZ();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= L"WINDOW";

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Pyramid and CUBE Native"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
	break;

	case WM_SIZE:
		giHeightOfWindow = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
	break;
	case VK_ESCAPE:
		
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f, 1.0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	
	Identity();

	resize(WIN_WIDTH, WIN_HEIGHT);	
	return;
}


void display()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	
	
	/* 
	//DISPLAY PYRAMID
	glLoadMatrixf(gIdentityMatrix);

	//glTranslatef(-2.0f,0.0f,-6.0f);
	Translate(-2.0f,0.0f,-7.0f);
	glMultMatrixf(gTranslationMatrix);
	
	//glRotatef(gfUpdateAngle,0.0f, 1.0f, 0.0f);
	RotateY();
	glMultMatrixf(gRotation_YMatrix);
	DrawColoredPyramid();
*/
	glMatrixMode(GL_MODELVIEW);
	
	glLoadMatrixf(gIdentityMatrix);

	//glTranslatef(2.0f, 0.0f, -6.0f);
	Translate(0.0f,0.0f,-7.0f);
	glMultMatrixf(gTranslationMatrix);
	
	//glScalef(0.75f,0.75f,1.0f);
	Scale(0.75f,0.75f,1.0f);
	glMultMatrixf(gScaleMatrix);

	//glRotatef(gfUpdateAngle,1.0f, 1.0f, 1.0f);
	RotateX();
	RotateY();
	RotateZ();

	glMultMatrixf(gRotation_XMatrix);
	glMultMatrixf(gRotation_YMatrix);
	glMultMatrixf(gRotation_ZMatrix);
	DrawColoredCube();
	
	UpdateAngle();
	SwapBuffers(ghdc);
	return;
}



void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glFrustum(-20.0f, 20.0f, -20.0f, 20.0f, 0.1f, 100.0f);

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}


void DrawColoredPyramid()
{
	glBegin(GL_TRIANGLES);

		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(- 1.0,- 1.0,1.0f);
		glColor3f(0.0f,0.0f,1.0f);
		glVertex3f( 1.0,- 1.0,1.0f);

		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);	
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(1.0,- 1.0,1.0f);
		glColor3f(0.0f,0.0f,1.0f);
		glVertex3f( 1.0,- 1.0,-1.0f);

		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);
		glColor3f(0.0f,0.0f,1.0f);
		glVertex3f( 1.0,- 1.0,-1.0f);
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(- 1.0,- 1.0,-1.0f);

		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(0.0f, 1.0,0.0f);
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(- 1.0,- 1.0,-1.0f);
		glColor3f(0.0f,1.0f,0.0f);
		glVertex3f(- 1.0,- 1.0,1.0f);
	glEnd();
	return;
}


void DrawColoredCube()
{
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f); 
	glVertex3f(1.0f, 1.0f, -1.0f); 
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); 
	glVertex3f(1.0f, 1.0f, 1.0f); 
		
	//BOTTOM FACE
	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); 

	//FRONT FACE
	glColor3f(0.0f, 0.0f, 1.0f); 
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); 

	//BACK FACE
	glColor3f(0.0f, 1.0f, 1.0f); 
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); 

	//RIGHT FACE
	glColor3f(1.0f, 0.0f, 1.0f); 
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); 
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//LEFT FACE
	glColor3f(1.0f, 1.0f, 0.0f); 
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); 
		
	glEnd();
	return;
}


void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle +0.10f;
	gfUpdateAngleInRadian = (gfUpdateAngle * (PI/180));
	if(gfUpdateAngle  >=  360.0f)
	{
		gfUpdateAngle = 0;
	}
}

void Identity()
{
	gIdentityMatrix[0]  = 1.0f;
	gIdentityMatrix[1]  = 0.0f;
	gIdentityMatrix[2]  = 0.0f;
	gIdentityMatrix[3]  = 0.0f;
	gIdentityMatrix[4]  = 0.0f;
	gIdentityMatrix[5]  = 1.0f;
	gIdentityMatrix[6]  = 0.0f;
	gIdentityMatrix[7]  = 0.0f;
	gIdentityMatrix[8]  = 0.0f;
	gIdentityMatrix[9]  = 0.0f;
	gIdentityMatrix[10] = 1.0f;
	gIdentityMatrix[11] = 0.0f;
	gIdentityMatrix[12] = 0.0f;
	gIdentityMatrix[13] = 0.0f;
	gIdentityMatrix[14] = 0.0f;
	gIdentityMatrix[15] = 1.0f;

	return;
}

void Translate(GLfloat gfTranslateX,GLfloat gfTranslateY,GLfloat gfTranslateZ)
{
	gTranslationMatrix[0]  = 1.0f;
	gTranslationMatrix[1]  = 0.0f;
	gTranslationMatrix[2]  = 0.0f;
	gTranslationMatrix[3]  = 0.0f;
	gTranslationMatrix[4]  = 0.0f;
	gTranslationMatrix[5]  = 1.0f;
	gTranslationMatrix[6]  = 0.0f;
	gTranslationMatrix[7]  = 0.0f;
	gTranslationMatrix[8]  = 0.0f;
	gTranslationMatrix[9]  = 0.0f;
	gTranslationMatrix[10] = 1.0f;
	gTranslationMatrix[11] = 0.0f;
	gTranslationMatrix[12] = gfTranslateX;
	gTranslationMatrix[13] = gfTranslateY;
	gTranslationMatrix[14] = gfTranslateZ;
	gTranslationMatrix[15] = 1.0f;

	return;
}

void Scale(GLfloat glfScaleX, GLfloat glfScaleY, GLfloat glfScaleZ)
{
	gScaleMatrix[0]  = glfScaleX;
	gScaleMatrix[1]  = 0.0f;
	gScaleMatrix[2]  = 0.0f;
	gScaleMatrix[3]  = 0.0f;
	gScaleMatrix[4]  = 0.0f;
	gScaleMatrix[5]  = glfScaleY;
	gScaleMatrix[6]  = 0.0f;
	gScaleMatrix[7]  = 0.0f;
	gScaleMatrix[8]  = 0.0f;
	gScaleMatrix[9]  = 0.0f;
	gScaleMatrix[10] = glfScaleZ;
	gScaleMatrix[11] = 0.0f;
	gScaleMatrix[12] = 0.0f;
	gScaleMatrix[13] = 0.0f;
	gScaleMatrix[14] = 0.0f;
	gScaleMatrix[15] = 1.0f;

	return;
}


void RotateX()
{
	gRotation_XMatrix[0]  = 1.0f;	//00
	gRotation_XMatrix[1]  = 0.0f;	//10	
	gRotation_XMatrix[2]  = 0.0f;	//20
	gRotation_XMatrix[3]  = 0.0f;	//30

	gRotation_XMatrix[4]  = 0.0f;	//01
	gRotation_XMatrix[5]  = cos(gfUpdateAngleInRadian);	//11
	gRotation_XMatrix[6]  = sin(gfUpdateAngleInRadian);		//21
	gRotation_XMatrix[7]  = 0.0f;	//31
	
	gRotation_XMatrix[8]  = 0.0f;	//02
	gRotation_XMatrix[9]  = -sin(gfUpdateAngleInRadian);	;	//12
	gRotation_XMatrix[10] = cos(gfUpdateAngleInRadian);	;	//22
	gRotation_XMatrix[11] = 0.0f;	//32
	
	gRotation_XMatrix[12] = 0.0f;	//03
	gRotation_XMatrix[13] = 0.0f;	//13
	gRotation_XMatrix[14] = 0.0f;	//23
	gRotation_XMatrix[15] = 1.0f;	//33

	return;
}
 

void RotateY()
{
	gRotation_YMatrix[0]  = cos(gfUpdateAngleInRadian);	//00
	gRotation_YMatrix[1]  = 0.0f;	//10	
	gRotation_YMatrix[2]  = -sin(gfUpdateAngleInRadian);	//20
	gRotation_YMatrix[3]  = 0.0f;	//30

	gRotation_YMatrix[4]  = 0.0f;	//01
	gRotation_YMatrix[5]  = 1.0f;	//11
	gRotation_YMatrix[6]  = 0.0f;	//21
	gRotation_YMatrix[7]  = 0.0f;	//31
	
	gRotation_YMatrix[8]  = sin(gfUpdateAngleInRadian);	//02
	gRotation_YMatrix[9]  = 0.0f;	//12
	gRotation_YMatrix[10] = cos(gfUpdateAngleInRadian);		//22
	gRotation_YMatrix[11] = 0.0f;	//32
	
	gRotation_YMatrix[12] = 0.0f;	//03
	gRotation_YMatrix[13] = 0.0f;	//13
	gRotation_YMatrix[14] = 0.0f;	//23
	gRotation_YMatrix[15] = 1.0f;	//33

	return;
}

void RotateZ()
{
	gRotation_ZMatrix[0]  = cos(gfUpdateAngleInRadian);	//00
	gRotation_ZMatrix[1]  = sin(gfUpdateAngleInRadian);	//10	
	gRotation_ZMatrix[2]  = 0.0f;	//20
	gRotation_ZMatrix[3]  = 0.0f;	//30

	gRotation_ZMatrix[4]  = -sin(gfUpdateAngleInRadian);	//01
	gRotation_ZMatrix[5]  = cos(gfUpdateAngleInRadian);		//11
	gRotation_ZMatrix[6]  = 0.0f;	//21
	gRotation_ZMatrix[7]  = 0.0f;	//31
	
	gRotation_ZMatrix[8]  = 0.0f;	//02
	gRotation_ZMatrix[9]  = 0.0f;	//12
	gRotation_ZMatrix[10] = 1.0f;	//22
	gRotation_ZMatrix[11] = 0.0f;	//32
	
	gRotation_ZMatrix[12] = 0.0f;	//03
	gRotation_ZMatrix[13] = 0.0f;	//13
	gRotation_ZMatrix[14] = 0.0f;	//23
	gRotation_ZMatrix[15] = 1.0f;	//33

	return;
}


void uninitialize()
{
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}