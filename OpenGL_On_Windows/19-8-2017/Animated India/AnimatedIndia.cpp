#include<Windows.h>
#include<stdio.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

float gfUpdateAngle;
float gfUpdateYCord = 0;

int giHeightOfWindow = 0;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

float fgSaffronRed = 0.956862f;		//244/255 ;
float fgSaffronGreen = 0.36862f;	//196/255;
float fgSaffronBlue = 0.088235f;	//48/255;

float gfSaffronRedVar   = 0.0f;
float gfSaffronBlueVar  = 0.0f;
float gfSaffronGreenVar = 0.0f;
float gfGreenVar		= 0.0f;

BOOL gbAnimate_I1 = FALSE;
BOOL gbAnimate_N  = FALSE;
BOOL gbAnimate_D  = FALSE;
BOOL gbAnimate_I2 = FALSE;
BOOL gbAnimate_A  = FALSE;
BOOL gbAnimateSlantRightTop = FALSE;
BOOL gbAnimateSlantRightBottom = FALSE;
BOOL gbAnimateSaffronRed   = FALSE;
BOOL gbAnimateSaffronGreen = FALSE;
BOOL gbAnimateSaffronBlue  = FALSE;


GLfloat gfTranslate_XI1 = 04.0f;
GLfloat gfTranslate_YN  = 06.0f;
GLfloat gfTranslate_YI2 = -6.0f;
GLfloat gfTranslate_XA  =  4.0f;

GLfloat gfTranslateSlant_RightTop_X = -8.000f; 
GLfloat gfTranslateSlant_RightBot_X = -7.998f;
GLfloat gfTranslateSlant_LEFTTop_X  = -8.000f;
GLfloat gfTranslateSlant_LEFTBot_X	= -7.998f;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

void UpdateAngle();

void Design_I();
void Design_N();
void Design_D();
void Design_A();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= L"WINDOW";

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("ANIMATE INDIA"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
	break;

	case WM_SIZE:
		giHeightOfWindow = HIWORD(lParam);
		resize(LOWORD(lParam), HIWORD(lParam));
	break;
	case VK_ESCAPE:
		
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f, 1.0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	resize(WIN_WIDTH, WIN_HEIGHT);	
	return;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
		glTranslatef(-gfTranslate_XI1,0.0f,-7.0f);		//TRANSLATE UPTO -3.325f	--WHICH IS CALCULATED DISTANCE OF I
		Design_I();
		gfTranslate_XI1 = gfTranslate_XI1 - 0.0005f;
		if(gfTranslate_XI1 <= 3.325)
		{
			gfTranslate_XI1 = 3.325f;
			gbAnimate_I1 = TRUE;
		}

		if(gbAnimate_I1 == TRUE)
		{
			glLoadIdentity();
			glTranslatef(-1.875f,gfTranslate_YN,-7.0f);
			Design_N();
			
			gfTranslate_YN = gfTranslate_YN - 0.0005f;
			if(gfTranslate_YN <= 0)
			{
				gfTranslate_YN = 0.0f;
				gbAnimate_N = TRUE;
			}

			if(gbAnimate_N == TRUE)
			{
				glLoadIdentity();
				glTranslatef(0.0f,0.0f,-7.0f);
				Design_D();

				if(gbAnimate_D == TRUE)
				{
					glLoadIdentity();
					glTranslatef(1.5f,gfTranslate_YI2,-7.0f);
					Design_I();
					gfTranslate_YI2 = gfTranslate_YI2 + 0.0005f;
					if(gfTranslate_YI2 >= 0)
					{
						gfTranslate_YI2 = 0.0f;
						gbAnimate_I2 = TRUE;
					}
					if(gbAnimate_I2 == TRUE)
					{
						glLoadIdentity();
						glTranslatef(gfTranslate_XA,0.0f,-7.0f);
						Design_A();
						gfTranslate_XA = gfTranslate_XA - 0.0005f;
						if(gfTranslate_XA <= 3.50f)
						{
							gfTranslate_XA = 3.50f;
							gbAnimate_A = TRUE;
						}
					}
				}
			}
		}

	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}
void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle +0.10f;
	if(gfUpdateYCord <= 15.0f )
	{
		gfUpdateYCord = gfUpdateYCord + 0.001f;
	}

	if(gfUpdateAngle  >=  360.0f)
	{
		gfUpdateAngle = 0;
	}
}
void uninitialize()
{
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}

/*
	HERE WE ARE PLACING TOTAL 5 CHARACTERS WITH 4 SPACES ---->0.2 EACH CHARCTER AND SPACE
	AND, FIRST 01 AND LAST 0.1 DISTANCE BETWEEN BORDER AND START AND END CHARACTER		
	HENCE TOTAL = 0.2*9 =1.8
	AND			  0.1*2 =0.2
	THAT IS TOTAL DISTANCE BETWEEN -1.0 AND 1.0	---ACROSS X-AXIS
	AND LEAVE 0.3 DISTANCE AT TOP AND BOTTOM	---ACROSS Y-AXIS
*/

void Design_I()
{
	/*-0.025  to  0.025*/
	glBegin(GL_QUADS);

	//LEFT SIDE OF 'N'
    glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(-0.025f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(-0.025f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.025f,-1.9f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.025f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE


	glEnd();
}

void Design_N()
{
	/*0.65  to  1.55  = 0.90*/
	glBegin(GL_QUADS);

	//LEFT SIDE OF 'N'
    glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(-0.5f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(-0.5f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(-0.45f,-1.9f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(-0.45f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//RIGHT SIDE OF 'N'
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.40f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(0.40f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.45f,-1.9f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.45f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//SLANT OF 'N'
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(-0.50f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE
	glVertex3f(-0.45f,1.9f,0.0f);	//LEFT TOP OF UPPER RECTANGLE

	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(0.40f,-1.90f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.45f,-1.80f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE

	glEnd();
}

void Design_D()
{
	/*-0.45  to 0.45 = 0.90*/
	glBegin(GL_QUADS);
	
	//LEFT SIDE OF 'D'
    glColor3f(gfSaffronRedVar,gfSaffronGreenVar,gfSaffronBlueVar);		//--SAFFRON		
	glVertex3f(-0.5f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glColor3f(0.0f,gfGreenVar,0.0f);
	glVertex3f(-0.5f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(-0.45f,-1.9f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	glColor3f(gfSaffronRedVar,gfSaffronGreenVar,gfSaffronBlueVar);		//--SAFFRON		
	glVertex3f(-0.45f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//RIGHT SIDE OF 'D'
	glColor3f(gfSaffronRedVar,gfSaffronGreenVar,gfSaffronBlueVar);		//--SAFFRON		
	glVertex3f(0.40f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glColor3f(0.0f,gfGreenVar,0.0f);
	glVertex3f(0.40f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.45f,-1.9f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	glColor3f(gfSaffronRedVar,gfSaffronGreenVar,gfSaffronBlueVar);		//--SAFFRON		
	glVertex3f(0.45f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//UPPER SIDE OF 'D'
	glColor3f(gfSaffronRedVar,gfSaffronGreenVar,gfSaffronBlueVar);		//--SAFFRON		
	glVertex3f(-0.55f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE
	glVertex3f(-0.55f,1.85f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glVertex3f(0.45f,1.85f,0.0f);		//RIGHT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.45f,1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//LOWER SIDE OF 'D'
	glColor3f(0.0f,gfGreenVar,0.0f);
	glVertex3f(-0.55f,-1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE
	glVertex3f(-0.55f,-1.85f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	glVertex3f(0.45f,-1.85f,0.0f);		//RIGHT BOTTOM OF UPPER RECTANGLE
	glVertex3f(0.45f,-1.9f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	gfSaffronRedVar   = gfSaffronRedVar   +  0.001f;
	gfSaffronBlueVar  = gfSaffronBlueVar  +  0.001f;
	gfSaffronGreenVar = gfSaffronGreenVar +  0.001f;
	gfGreenVar		  = gfGreenVar		  +  0.001f;
	
	if(gfSaffronRedVar >= fgSaffronRed)
	{
		gfSaffronRedVar = fgSaffronRed;
		gbAnimateSaffronRed = TRUE;
	}
	if(gfSaffronBlueVar >= fgSaffronBlue)
	{
		gfSaffronBlueVar = fgSaffronBlue;
		gbAnimateSaffronBlue = TRUE;
	}
	if(gfSaffronGreenVar >= fgSaffronGreen)
	{
		gfSaffronGreenVar = fgSaffronGreen;
		gbAnimateSaffronGreen = TRUE;
	}     
	if(gfGreenVar >= 1.0f)
	{
		gfGreenVar = 1.0f;
		if((gbAnimateSaffronRed == TRUE) && (gbAnimateSaffronGreen == TRUE) && (gbAnimateSaffronBlue == TRUE) )
		{
			gbAnimate_D = TRUE;
		}
	}


	glEnd();
}
void Design_A()
{
	/*-1.05 to 1.05 = 2.1*/
	glBegin(GL_QUADS);
	
	//LEFT SIDE OF 'A'
    glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.0f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(-1.0f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(-1.05f,-1.88f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.05f,1.88f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	//RIGHT SIDE OF 'A'
    glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.0f,1.9f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
	
	glColor3f(0.0f,1.0f,0.0f);
	glVertex3f(1.0f,-1.9f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
	glVertex3f(1.05f,-1.88f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
	
	glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
	glVertex3f(0.05f,1.88f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

	if(gbAnimate_A == TRUE)
	{
		//CENTRE OF 'A'
		glColor3f(fgSaffronRed,fgSaffronGreen,fgSaffronBlue);		//--SAFFRON		
		glVertex3f(gfTranslateSlant_LEFTTop_X,0.405f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_LEFTBot_X,0.385f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightBot_X,0.385f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightTop_X,0.405f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

		//CENTRE OF 'A'
		glColor3f(1.0f,1.0f,1.0f);		//--WHITE
		glVertex3f(gfTranslateSlant_LEFTTop_X,0.38f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_LEFTBot_X,0.36f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightBot_X,0.36f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightTop_X,0.38f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

		//CENTRE OF 'A'
		glColor3f(0.0f,1.0f,0.0f);		//--GREEN
		glVertex3f(gfTranslateSlant_LEFTTop_X,0.35f,0.0f);		//LEFT TOP OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_LEFTBot_X,0.33f,0.0f);	//LEFT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightBot_X,0.33f,0.0f);	//RIGHT BOTTOM OF UPPER RECTANGLE
		glVertex3f(gfTranslateSlant_RightTop_X,0.35f,0.0f);		//RIGHT TOP OF UPPER RECTANGLE

		gfTranslateSlant_RightTop_X = gfTranslateSlant_RightTop_X  + 0.001f;
		gfTranslateSlant_RightBot_X = gfTranslateSlant_RightBot_X  + 0.001f;

		if(gfTranslateSlant_RightTop_X >= 0.45f)
		{
			gfTranslateSlant_RightTop_X = 0.45f;
			gbAnimateSlantRightTop = TRUE;
		}
		if(gfTranslateSlant_RightBot_X >= 0.39f)
		{
			gfTranslateSlant_RightBot_X = 0.39f;
			gbAnimateSlantRightBottom = TRUE;
		}
		if((gbAnimateSlantRightTop == TRUE) && (gbAnimateSlantRightBottom == TRUE))
		{
			gfTranslateSlant_LEFTTop_X = gfTranslateSlant_LEFTTop_X + 0.001f;
			gfTranslateSlant_LEFTBot_X = gfTranslateSlant_LEFTBot_X + 0.001f;
			if(gfTranslateSlant_LEFTTop_X  >= -0.40f)
			{
				gfTranslateSlant_LEFTTop_X = -0.40f;
			}
			if(gfTranslateSlant_LEFTBot_X	>= -0.39f)
			{
				gfTranslateSlant_LEFTBot_X = -0.39f;
			}
		}
	}

	glEnd();
}


