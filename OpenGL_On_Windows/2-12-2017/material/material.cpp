#include<Windows.h>
#include<stdio.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

GLfloat light0_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat light0_diffused[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light0_specular[] = {1.0f,1.0f,1.0f,0.0f};
GLfloat light0_position[] = {0.0f,0.0f,1.0f,0.0f};


GLfloat material_ambient[][4]  = {{0.0215f,0.1745f,0.0215f,1.0f},{0.135f,0.2225f,0.1575f,1.0f},{0.05375f,0.05f,0.06625f,1.0f},
								 {0.25f,0.20725f,0.20725f,1.0f},{0.1745f,0.01175f,0.01175f,1.0f},{0.1f,0.18725f,0.1745f,1.0f},
								 {0.329412f,0.223529f,0.027451f,1.0f}, {0.2125f,0.1275f,0.054f,1.0f},{0.25f,0.25f,0.25f,1.0f},
								 {0.19125f,0.0735f,0.0225f,1.0f},{0.24725f,0.1995f,1.0f},{0.19225f,0.19225f,0.19225f,1.0f},	
								 {0.0f,0.0f,0.0f,1.0f}, {0.0f,0.1f,0.06f,1.0f}, {0.0f,0.0f,0.0f,1.0f},
							     {0.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},{0.0f,0.0f,0.0f,1.0f},
								 {0.02f,0.02f,0.02f,1.0f},{0.00f,0.05f,0.05f,1.0f},{0.0f,0.05f,0.0f},
								 {0.5f,0.0f,0.0f,1.0f}, {0.05f,0.05f,0.05f,1.0f},{0.05f,0.05f,0.00f,1.0f}};


GLfloat material_diffused[][4] = {{0.07568f,0.61424f,0.07568f,1.0f},{0.54f,0.89f,0.63f,1.0f},{0.18275f,0.17f,0.22525f,1.0f},
								 {1.0f,0.829f,0.829f,1.0f},{0.61424f,0.04136f,0.04136f,1.0f} ,{0.396f,0.74151f,0.69102f,1.0f},
								 {0.780392f,0.568627f,0.113725f,1.0f}, {0.714f,0.4284f,0.18144f,1.0f}, {0.4f,0.4f,0.4f,1.0f},
								 {0.7038f,0.27048f,0.0828f,1.0f},{0.75164f,0.60648f,0.22648f,1.0f},{0.01f,0.01f,0.01f,1.0f},
								 {0.01f,0.01f,0.01f,1.0f}, {0.0f,0.50980392f,0.50980392f,1.0f},{0.1f,0.35f,0.1f,1.0f},
								 {0.5f,0.0f,0.0f,1.0f},{0.550f,0.550f,0.550f,1.0f},{0.50f,0.50f,0.0f,1.0f},
								 {0.01f,0.01f,0.01f,1.0f},{0.4f,0.5f,0.5f,1.0f},{0.40f,0.05f,0.40f,1.0f},
								 {0.5f,0.40f,0.40f,1.0f},{0.5f,0.5f,0.5f,1.0f},{0.5f,0.5f,0.4f,1.0f}};


GLfloat material_specular[][4] = {{0.633f,0.727811f,0.633f,1.0f},{0.316228f,0.316228f,0.316228f},{0.332741f,0.328634f,0.346435f,1.0f},
								 {0.296648f,0.296648f,0.296648f,1.0f},{0.72781f,0.626959f,0.626959f,1.0f},{0.297254f,0.30829f,0.306678f,1.0f},
							     {0.992157f,0.941176f,0.807843f,1.0f}, {0.393546f,0.271906f,0.166721f,1.0f}, {0.774597f,0.774597f,0.774597f,1.0f},
								 {0.256777f,0.137622f,0.086014f,1.0f},{0.628281f,0.555802f,0.366065f,1.0f},{0.5f,0.5f,0.5f,1.0f},
								 {0.50f,0.5f,0.50f,1.0f}, {0.50196078f,0.50196078f,0.50196078f,1.0f},{0.45f,0.55f,0.45f,1.0f},
								 {0.70f,0.6f,0.6f,1.0f},{0.70f,0.70f,0.70f,1.0f},{0.60f,0.60f,0.60f,1.0f},
								 {0.4f,0.4f,0.4f,1.0f},{0.04f,0.7f,0.7f,1.0f},{0.04f,0.7f,0.04f,1.0f},
								 {0.7f,0.04f,0.04f,1.0f},{0.7f,0.7f,0.7f,1.0f}, {0.7f,0.7f,0.04f,1.0f}};


GLfloat material_shinynes[][1]= {{(0.6f*128.0f)},{0.6f*128.0f},{0.3f*128.0f},{0.088f*128.0f},{0.6f*128.0f},{0.1f*128.0f},
								 {0.21794872f*128.0f}, {0.2f*128.0f},{0.6f*128.0f},{0.1f*128.0f},{0.4f*128.0f},{0.25f*128.0f},
								 {0.25f*128.0f}, {0.25f*128.0f}, {0.25f*128.0f},{0.25f*128.0f},{0.25f*128.0f},{0.25f*128.0f},
								 {0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f},{0.078125f * 128.0f}};


BOOL gbIsXKeypressed = FALSE;
BOOL gbIsYKeypressed = FALSE;
BOOL gbIsZKeypressed = FALSE;

float gfAngleOfLight   = 0.0f;
float gfAngleY  = 0.0f;
float gfAngleZ = 0.0f;

BOOL gbLihting = FALSE;
GLUquadric *quadric = NULL;

GLfloat gfRightX = -9.5f;
GLfloat gfTopY	= 8.5f;
GLfloat gfDistanceAtX = 6.5f;
GLfloat gfDistanceAtY = 2.5f;
GLfloat gfPositionX = 0.0f, gfPositionY =0.0f; 

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= TEXT("WINDOW");

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("LIGHT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,TEXT("ERROR WHILE CreateWindow"),TEXT("ERROR"),0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if((wParam == 'L' )|| (wParam == 'l'))
		{
			if(gbLihting == FALSE)
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_LIGHT0);
				gbLihting = TRUE ;
			}
			else
			{
				glDisable(GL_LIGHT0);
				glDisable(GL_LIGHTING);
				gbLihting = FALSE;
			}
		}
		else if((wParam == 'X') || (wParam == 'x'))
		{
			gbIsXKeypressed = TRUE;
			gbIsYKeypressed = FALSE;
			gbIsZKeypressed = FALSE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
		else if((wParam == 'Y') || (wParam == 'y'))
		{
			gbIsXKeypressed = FALSE;
			gbIsYKeypressed = TRUE;
			gbIsZKeypressed = FALSE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
		else if((wParam == 'Z') || (wParam == 'Z'))
		{
			gbIsXKeypressed = FALSE;
			gbIsYKeypressed = FALSE;
			gbIsZKeypressed = TRUE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f,0.25f,0.25f, 1.0);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light0_diffused);
	glLightfv(GL_LIGHT0,GL_AMBIENT,light0_ambient);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light0_specular);
	glEnable(GL_LIGHT0);

	quadric = gluNewQuadric();

	resize(WIN_WIDTH,WIN_HEIGHT);
}

void UpdateAngle()
{
		gfAngleOfLight   =  gfAngleOfLight + 0.1f;
		if(gfAngleOfLight  >= 360.0f )
		{
			gfAngleOfLight = 0.0f;  
		}
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

  glPushMatrix();
    
    gluLookAt(0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	

	glPushMatrix();
		if(gbIsXKeypressed)
		{
			glRotatef(gfAngleOfLight,1.0f,0.0f,0.0f);
			//light0_position[0] = light0_position[1] = 0.0f;
			light0_position[2] = gfAngleOfLight;
		}
		else if(gbIsYKeypressed)
		{
			glRotatef(gfAngleOfLight,0.0f,1.0f,0.0f);
			light0_position[2] = light0_position[1] = 0.0f;
			light0_position[2] = gfAngleOfLight;
		}
		else if(gbIsZKeypressed)
		{
			glRotatef(gfAngleOfLight,0.0f,0.0f,1.0f);
			light0_position[2] = light0_position[0] = 0.0f;
			light0_position[1] = gfAngleOfLight;
		}
		//
		glLightfv(GL_LIGHT0,GL_POSITION,light0_position);
	glPopMatrix();

	glTranslatef(0.0f,gfTopY,-16.5f);
	for(int j =0; j < 6; j++)
	{
		glTranslatef(0.0f,-gfDistanceAtY,0.0f);
		for(int i =0; i <4; i++)
		{
			glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,material_diffused[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,material_ambient[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,material_specular[(j*4)+i]);
			glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,material_shinynes[(j*4)+i]);

			//glRotatef(90.0f,1.0f,0.0f,0.0f);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			glPushMatrix();
			glTranslatef(gfPositionX,0.0f,0.0f);			
			gluSphere(quadric,0.5,30,30);
			glPopMatrix();
			gfPositionX = gfPositionX + gfDistanceAtX;
		}
		gfPositionX = gfRightX;
	}
	gfPositionY = gfTopY;
	gfPositionX = gfRightX;
	
  glPopMatrix();

	UpdateAngle();
	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}


void uninitialize()
{
	if(quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}