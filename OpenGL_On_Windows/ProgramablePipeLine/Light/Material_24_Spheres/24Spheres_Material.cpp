#include<Windows.h>
#include<stdio.h>

#include<C:/glew/include/gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include"Sphere.h"
using namespace vmath;

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

FILE *fp;


enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE
};

GLuint gVao_Cube;
GLuint gVbo_Vertex_Cube;
GLuint gVbo_Normal_Cube;

GLuint gVao_Sphere;
GLuint gVbo_Vertex_Sphere;
GLuint gVbo_Normal_Sphere;
GLuint gVbo_Sphere_Element;

GLuint gMVPUniform;


mat4 gPerspectiveProjectionMatrix;
mat4 gRotationMatrix;
mat4 gScaleMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float gfUpdateAngle = 0.0f;

//SPHERE RELATED VARIABLES
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;

//LIGHT RELATED VARIABLES

GLuint  gProjectionMatrixUniform;
GLuint  gModelMatrixUniform;
GLuint  gViewMatrixUniform;

GLuint gLa_Uniform;
GLuint gLd_Uniform;
GLuint gLs_Uniform;

GLuint gKa_Uniform;
GLuint gKd_Uniform;
GLuint gKs_Uniform;
GLuint gMaterial_Shinyness_Uniform;

GLuint gLKeyPressedUniform;
GLuint gLight_Position_Uniform;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffused[] = { 1.0f, 1.0f ,1.0f ,1.0f };
GLfloat LightSpecular[] = { 1.0f ,1.0f, 1.0f ,1.0f };
GLfloat lightPosition[] = { 50.0f, 50.0f, 150.0f, 1.0f };

/*GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0 };
GLfloat materialtDiffused[] = { 1.0f, 1.0f ,1.0f ,1.0f };
GLfloat materialSpecular[] = { 1.0f ,1.0f, 1.0f ,1.0f };
*/
GLfloat materialShininess = 70.0f;

bool gbAnimate = FALSE;
bool gbLight = FALSE;


float gfXAngle = 0.0f;
float gfZAngle = 0.0f;
float gfYAngle = 0.0f;
float gfRadiousOfLightRotation = 85.0f;

GLfloat material_ambient[][4] = { { 0.0215f,0.1745f,0.0215f,1.0f },{ 0.135f,0.2225f,0.1575f,1.0f },{ 0.05375f,0.05f,0.06625f,1.0f },
{ 0.25f,0.20725f,0.20725f,1.0f },{ 0.1745f,0.01175f,0.01175f,1.0f },{ 0.1f,0.18725f,0.1745f,1.0f },
{ 0.329412f,0.223529f,0.027451f,1.0f },{ 0.2125f,0.1275f,0.054f,1.0f },{ 0.25f,0.25f,0.25f,1.0f },
{ 0.19125f,0.0735f,0.0225f,1.0f },{ 0.24725f,0.1995f,1.0f },{ 0.19225f,0.19225f,0.19225f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.1f,0.06f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },
{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.0f,0.0f,1.0f },
{ 0.02f,0.02f,0.02f,1.0f },{ 0.00f,0.05f,0.05f,1.0f },{ 0.0f,0.05f,0.0f },
{ 0.5f,0.0f,0.0f,1.0f },{ 0.05f,0.05f,0.05f,1.0f },{ 0.05f,0.05f,0.00f,1.0f } };


GLfloat material_diffused[][4] = { { 0.07568f,0.61424f,0.07568f,1.0f },{ 0.54f,0.89f,0.63f,1.0f },{ 0.18275f,0.17f,0.22525f,1.0f },
{ 1.0f,0.829f,0.829f,1.0f },{ 0.61424f,0.04136f,0.04136f,1.0f } ,{ 0.396f,0.74151f,0.69102f,1.0f },
{ 0.780392f,0.568627f,0.113725f,1.0f },{ 0.714f,0.4284f,0.18144f,1.0f },{ 0.4f,0.4f,0.4f,1.0f },
{ 0.7038f,0.27048f,0.0828f,1.0f },{ 0.75164f,0.60648f,0.22648f,1.0f },{ 0.01f,0.01f,0.01f,1.0f },
{ 0.01f,0.01f,0.01f,1.0f },{ 0.0f,0.50980392f,0.50980392f,1.0f },{ 0.1f,0.35f,0.1f,1.0f },
{ 0.5f,0.0f,0.0f,1.0f },{ 0.550f,0.550f,0.550f,1.0f },{ 0.50f,0.50f,0.0f,1.0f },
{ 0.01f,0.01f,0.01f,1.0f },{ 0.4f,0.5f,0.5f,1.0f },{ 0.40f,0.05f,0.40f,1.0f },
{ 0.5f,0.40f,0.40f,1.0f },{ 0.5f,0.5f,0.5f,1.0f },{ 0.5f,0.5f,0.4f,1.0f } };


GLfloat material_specular[][4] = { { 0.633f,0.727811f,0.633f,1.0f },{ 0.316228f,0.316228f,0.316228f },{ 0.332741f,0.328634f,0.346435f,1.0f },
{ 0.296648f,0.296648f,0.296648f,1.0f },{ 0.72781f,0.626959f,0.626959f,1.0f },{ 0.297254f,0.30829f,0.306678f,1.0f },
{ 0.992157f,0.941176f,0.807843f,1.0f },{ 0.393546f,0.271906f,0.166721f,1.0f },{ 0.774597f,0.774597f,0.774597f,1.0f },
{ 0.256777f,0.137622f,0.086014f,1.0f },{ 0.628281f,0.555802f,0.366065f,1.0f },{ 0.5f,0.5f,0.5f,1.0f },
{ 0.50f,0.5f,0.50f,1.0f },{ 0.50196078f,0.50196078f,0.50196078f,1.0f },{ 0.45f,0.55f,0.45f,1.0f },
{ 0.70f,0.6f,0.6f,1.0f },{ 0.70f,0.70f,0.70f,1.0f },{ 0.60f,0.60f,0.60f,1.0f },
{ 0.4f,0.4f,0.4f,1.0f },{ 0.04f,0.7f,0.7f,1.0f },{ 0.04f,0.7f,0.04f,1.0f },
{ 0.7f,0.04f,0.04f,1.0f },{ 0.7f,0.7f,0.7f,1.0f },{ 0.7f,0.7f,0.04f,1.0f } };


GLfloat material_shinyness[][1] = { { (0.6f*128.0f) },{ 0.6f*128.0f },{ 0.3f*128.0f },{ 0.088f*128.0f },{ 0.6f*128.0f },{ 0.1f*128.0f },
{ 0.21794872f*128.0f },{ 0.2f*128.0f },{ 0.6f*128.0f },{ 0.1f*128.0f },{ 0.4f*128.0f },{ 0.25f*128.0f },
{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },{ 0.25f*128.0f },
{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f },{ 0.078125f * 128.0f } };

BOOL gbIsXKeypressed = FALSE;
BOOL gbIsYKeypressed = FALSE;
BOOL gbIsZKeypressed = FALSE;

float gfAngleOfLight = 0.0f;
float gfAngleY = 0.0f;
float gfAngleZ = 0.0f;

GLfloat gfRightX = -9.0f;
GLfloat gfTopY = 5.0f;
GLfloat gfDistanceAtX = 6.0f;
GLfloat gfDistanceAtY = 2.0f;
GLfloat gfPositionX = 0.0f, gfPositionY = 0.0f;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCndLine, int iCmdShow)
{
	fp = fopen("24Material.txt", "w");
	if (NULL == fp)
	{
		printf("\nERROR WHILE OPENING LOG FILE \n");
		exit(1);
	}

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("WINDOW");

	BOOL bDone = FALSE;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON1"));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("24Material"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP, TEXT("ERROR WHILE CreateWindow"), TEXT("ERROR"), 0);
		return 0;
	}
	else
	{
		ghwnd = hwnd;
	}

	initialize();

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();

	TCHAR szMessage[500] = { '\0' };

	switch (iMsg)
	{

	case WM_CREATE:
		fprintf(fp, "\nINSIDE WM_CREATE\n");
		break;

	case WM_KEYDOWN:
		if ((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if (gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if (wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if ((wParam == 'A') || (wParam == 'a'))
		{
			if (gbAnimate == false)
			{
				gbAnimate = true;
			}
			else
			{
				gbAnimate = false;
			}
		}
		else if ((wParam == 'L') || (wParam == 'l'))
		{
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
		}
		else if ((wParam == 'X') || (wParam == 'x'))
		{
			gbIsXKeypressed = TRUE;
			gbIsYKeypressed = FALSE;
			gbIsZKeypressed = FALSE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
		else if ((wParam == 'Y') || (wParam == 'y'))
		{
			gbIsXKeypressed = FALSE;
			gbIsYKeypressed = TRUE;
			gbIsZKeypressed = FALSE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
		else if ((wParam == 'Z') || (wParam == 'z'))
		{
			gbIsXKeypressed = FALSE;
			gbIsYKeypressed = FALSE;
			gbIsZKeypressed = TRUE;

			gfAngleOfLight = gfAngleY = gfAngleZ = 0.0f;
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case VK_ESCAPE:

		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void initialize()
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		fprintf(fp, "\nERROR: error while initialising the GLEW\n");
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	fprintf(fp, "\n OPENGL VERSION: %s ", glGetString(GL_VERSION));
	fprintf(fp, "\n OPENGL SHADER VERSION: %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	//fclose(fp);

	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix ;" \
		"uniform mat4 u_view_matrix ;" \
		"uniform mat4 u_projection_matrix ;" \
		"uniform vec4 u_light_position ;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;"\
		"out vec3 light_direction;"\
		"out vec3 viewer_vector;"\
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)"\
		"{"\
		"vec4 eye_coordinates = u_view_matrix *u_model_matrix* vPosition;"\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"\
		"light_direction = vec3(u_light_position) -eye_coordinates.xyz;"\
		"viewer_vector = -eye_coordinates.xyz;"\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
	//COMPILE VERTEX SHADER

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\nVERTEX SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n VERTEX SHADER COMPILED SUCCESSGFULLY \n");
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec3 transformed_normals;"\
		"in vec3 light_direction;"\
		"in vec3 viewer_vector;"\
		"uniform vec3 u_La ;" \
		"uniform vec3 u_Ld ;" \
		"uniform vec3 u_Ls ;" \
		"uniform vec3 u_Ka ;" \
		"uniform vec3 u_Kd ;" \
		"uniform vec3 u_Ks ;" \
		"uniform float u_material_shininess ;" \
		"uniform int u_lighting_enabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;"\
		"if(u_lighting_enabled != 1)"\
		"{"\
		"phong_ads_color = vec3(1.0,1.0,1.0);"\
		"}"\
		"else"\
		"{"\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"\
		"vec3 normalized_light_direction = normalize(light_direction);"\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
		"vec3 ambient = u_La*u_Ka;"\
		"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"\
		"vec3 diffuse = u_Ld* u_Kd * tn_dot_ld;"\
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);"\
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		"phong_ads_color = ambient + diffuse + specular;"\
		"}"\
		"FragColor = vec4(phong_ads_color,1.0) ;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const char **)&fragmentShaderSourceCode, NULL);
	//COMPILE FRAGMENT SHADER
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n FRAGMENT SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n FRAGMENT SHADER COMPILED SUCCESSGFULLY \n");
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_LINK_STATUS, &iShaderLinkStatus);
	if (iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n LINK TIME ERROR LOG: \n%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\nSHADER LINKED SUCCESSFULLY \n");
	}

	//GET ALL UNIFORM LOCATIONS


	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	gLight_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
	gLa_Uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	gLd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gLs_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	gKa_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	gKd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKs_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gMaterial_Shinyness_Uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	//VERTICES,COLOR,SHADER_ATTRIBUTES,vbo,vao initialisation

	//START THE SPHERE RECORDER
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//	START THE SPHERE RECORDER

	glGenVertexArrays(1, &gVao_Sphere);
	glBindVertexArray(gVao_Sphere);

	//BIND TO THE VERTEX ARRAY OF SHERE
	glGenBuffers(1, &gVbo_Vertex_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Vertex_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE NORMAL BUFFER
	glGenBuffers(1, &gVbo_Normal_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE SPHERE ELEMENT BUFFER
	glGenBuffers(1, &gVbo_Sphere_Element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//UNBIND FROM THE SPHERE VAO
	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.25f, 0.25f, 0.25f, 1.0);

	gPerspectiveProjectionMatrix = mat4::identity();
	gRotationMatrix = mat4::identity();
	gScaleMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle + 0.090f;
	if (gfUpdateAngle >= 360.0f)
	{
		gfUpdateAngle = 0;
	}

	gfXAngle += 0.005f;
	if (gfXAngle >= 360.0f)
	{
		gfXAngle = 0.0f;
	}

	gfYAngle += 0.005f;
	if (gfYAngle >= 360.0f)
	{
		gfYAngle = 0.0f;
	}

	gfZAngle += 0.005f;
	if (gfZAngle >= 360.0f)
	{
		gfZAngle = 0.0f;
	}
}

void display()
{
	float fYPosition = gfTopY;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelMatrix = mat4::identity();
	mat4 ViewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLa_Uniform, 1, lightAmbient);
		glUniform3fv(gLd_Uniform, 1, lightDiffused);
		glUniform3fv(gLs_Uniform, 1, LightSpecular);

		if (gbIsXKeypressed)
		{
			lightPosition[0] = 0.0f;
			lightPosition[1] = gfRadiousOfLightRotation*cos(gfXAngle);
			lightPosition[2] = gfRadiousOfLightRotation*sin(gfXAngle);
		}
		else if (gbIsYKeypressed)
		{
			lightPosition[1] = 0.0f;
			lightPosition[0] = gfRadiousOfLightRotation*cos(gfYAngle);
			lightPosition[2] = gfRadiousOfLightRotation*sin(gfYAngle);
		}
		else if (gbIsZKeypressed)
		{
			lightPosition[2] = 0.0f;
			lightPosition[0] = gfRadiousOfLightRotation*cos(gfZAngle);
			lightPosition[1] = gfRadiousOfLightRotation*sin(gfZAngle);
		}
		glUniform4fv(gLight_Position_Uniform, 1, lightPosition);

		/*glUniform3fv(gKa_Uniform, 1, materialAmbient);
		glUniform3fv(gKd_Uniform, 1, materialtDiffused);
		glUniform3fv(gKs_Uniform, 1, materialSpecular);
		glUniform1f(gMaterial_Shinyness_Uniform, materialShininess);*/
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	modelMatrix = translate(0.0f, 0.0f, -10.0f);

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);

	gfPositionY = gfTopY;
	gfPositionX = gfRightX;

	for (int j = 0; j < 6; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			materialShininess = material_shinyness[i][j];
			glUniform3fv(gKa_Uniform, 1, material_diffused[(j * 4) + i]);
			glUniform3fv(gKd_Uniform, 1, material_ambient[(j * 4) + i]);
			glUniform3fv(gKs_Uniform, 1, material_specular[(j * 4) + i]);
			glUniform1f(gMaterial_Shinyness_Uniform, material_shinyness[i][j]);
			modelMatrix = mat4::identity();
			modelMatrix = translate(gfPositionX, gfPositionY, -15.0f);
			glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

			gfPositionX = gfPositionX + gfDistanceAtX;
		}
		gfPositionY = gfPositionY - gfDistanceAtY;
		gfPositionX = gfRightX;
	}

	glBindVertexArray(0);

	glUseProgram(0);

	UpdateAngle();

	SwapBuffers(ghdc);
	return;
}


void resize(int width, int height)
{
	fprintf(fp, "\nINSIDE RESIZE \n");
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (float)width / float(height), 0.1f, 100.0f);
	return;
}


void uninitialize()
{
	if (gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gVbo_Normal_Sphere)
	{
		glDeleteBuffers(1, &gVbo_Normal_Sphere);
		gVbo_Normal_Sphere = 0;
	}
	if (gVbo_Sphere_Element)
	{
		glDeleteBuffers(1, &gVbo_Sphere_Element);
		gVbo_Sphere_Element = 0;
	}
	if (gVao_Sphere)
	{
		glDeleteVertexArrays(1, &gVao_Sphere);
		gVao_Sphere = 0;
	}
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject = 0;


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	if (fp)
	{
		fprintf(fp, "\nPROGRAM ENDED SUCCESSFULLY \n");;
		fclose(fp);
		fp = NULL;
	}

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}