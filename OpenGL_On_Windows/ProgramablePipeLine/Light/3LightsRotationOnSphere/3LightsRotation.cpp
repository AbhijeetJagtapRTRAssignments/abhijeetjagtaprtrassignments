#include<Windows.h>
#include<stdio.h>

#include<C:/glew/include/gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include"Sphere.h"
using namespace vmath;

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

FILE *fp;


enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE
};

GLuint gVao_Sphere;
GLuint gVbo_Vertex_Sphere;
GLuint gVbo_Normal_Sphere;
GLuint gVbo_Sphere_Element;


GLuint gVao_Triangle;
GLuint gVbo_Position;
GLuint gVbo_Normal_Triangle;

GLuint gMVPUniform;


mat4 gPerspectiveProjectionMatrix;
mat4 gRotationMatrix;
mat4 gScaleMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float gfUpdateAngle = 0.0f;

//SPHERE RELATED VARIABLES
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;

//LIGHT RELATED VARIABLES

GLuint  gProjectionMatrixUniform;
GLuint  gModelMatrixUniform;
GLuint  gViewMatrixUniform;

GLuint gLa0_Uniform;
GLuint gLd0_Uniform;
GLuint gLs0_Uniform;
GLuint gLight0_Position_Uniform;

GLuint gLa1_Uniform;
GLuint gLd1_Uniform;
GLuint gLs1_Uniform;
GLuint gLight1_Position_Uniform;

GLuint gLa2_Uniform;
GLuint gLd2_Uniform;
GLuint gLs2_Uniform;
GLuint gLight2_Position_Uniform;

GLuint gKa_Uniform;
GLuint gKd_Uniform;
GLuint gKs_Uniform;
GLuint gMaterial_Shinyness_Uniform;

GLuint gLKeyPressedUniform;

GLfloat light0_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light0_Diffused[] = { 1.0f, 0.0f ,0.0f ,1.0f };
GLfloat Light0_Specular[] = { 1.0f ,0.0f, 0.0f ,1.0f };
GLfloat light0_Position[] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLfloat light1_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light1_Diffused[] = { 0.0f, 1.0f ,0.0f ,1.0f };
GLfloat Light1_Specular[] = { 0.0f ,1.0f, 0.0f ,1.0f };
GLfloat light1_Position[] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLfloat light2_Ambient[] =  { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light2_Diffused[] = { 0.0f, 0.0f ,1.0f ,1.0f };
GLfloat Light2_Specular[] = { 0.0f ,0.0f, 1.0f ,1.0f };
GLfloat light2_Position[] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0 };
GLfloat materialtDiffused[] = { 1.0f, 1.0f ,1.0f ,1.0f };
GLfloat materialSpecular[] = { 1.0f ,1.0f, 1.0f ,1.0f };
GLfloat materialShininess = 50.0f;

bool gbAnimate = FALSE;
bool gbLight = FALSE;

float gfRedAngle = 0.0f;
float gfBlueAngle = 0.0f;
float gfGreendAngle = 0.0f;
float gfRadiousOfLightRotation =5.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCndLine, int iCmdShow)
{
	fp = fopen("3LIGHTSROTATION.txt", "w");
	if (NULL == fp)
	{
		printf("\nERROR WHILE OPENING LOG FILE \n");
		exit(1);
	}

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("2LIGHTS");

	BOOL bDone = FALSE;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON1"));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("3LIGHTSROTATION"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP, TEXT("ERROR WHILE CreateWindow"), TEXT("ERROR"), 0);
		return 0;
	}
	else
	{
		ghwnd = hwnd;
	}

	initialize();

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();

	TCHAR szMessage[500] = { '\0' };

	switch (iMsg)
	{

	case WM_CREATE:
		fprintf(fp, "\nINSIDE WM_CREATE\n");
		break;

	case WM_KEYDOWN:
		if ((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if (gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if (wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if ((wParam == 'A') || (wParam == 'a'))
		{
			if (gbAnimate == false)
			{
				gbAnimate = true;
			}
			else
			{
				gbAnimate = false;
			}
		}
		else if ((wParam == 'L') || (wParam == 'l'))
		{
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case VK_ESCAPE:

		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void initialize()
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		fprintf(fp, "\nERROR: error while initialising the GLEW\n");
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	fprintf(fp, "\n OPENGL VERSION: %s ", glGetString(GL_VERSION));
	fprintf(fp, "\n OPENGL SHADER VERSION: %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	//fclose(fp);

	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix ;" \
		"uniform mat4 u_view_matrix ;" \
		"uniform mat4 u_projection_matrix ;" \
		"uniform int u_lighting_enabled;" \
		"uniform vec4 u_light0_position;" \
		"uniform vec4 u_light1_position;" \
		"uniform vec4 u_light2_position;" \
		"out vec3 transformed_normals;"\
		"out vec3 light0_direction;"\
		"out vec3 light1_direction;"\
		"out vec3 light2_direction;"\
		"out vec3 viewer_vector;"\
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)"\
		"{"\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"\
		"light0_direction = vec3(u_light0_position) -eye_coordinates.xyz;"\
		"light1_direction = vec3(u_light1_position) -eye_coordinates.xyz;"\
		"light2_direction = vec3(u_light2_position) -eye_coordinates.xyz;"\
		"viewer_vector = -eye_coordinates.xyz; "\
		"}"\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition ;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
	//COMPILE VERTEX SHADER

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\nVERTEX SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n VERTEX SHADER COMPILED SUCCESSGFULLY \n");
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec3 transformed_normals;"\
		"in vec3 light0_direction;"\
		"in vec3 light1_direction;"\
		"in vec3 light2_direction;"\
		"in vec3 viewer_vector;"\
		"uniform vec3 u_La0 ;" \
		"uniform vec3 u_Ld0 ;" \
		"uniform vec3 u_Ls0 ;" \
		"uniform vec3 u_La1 ;" \
		"uniform vec3 u_Ld1 ;" \
		"uniform vec3 u_Ls1 ;" \
		"uniform vec3 u_La2 ;" \
		"uniform vec3 u_Ld2 ;" \
		"uniform vec3 u_Ls2 ;" \
		"uniform vec3 u_Ka ;" \
		"uniform vec3 u_Kd ;" \
		"uniform vec3 u_Ks ;" \
		"uniform float u_material_shininess ;" \
		"uniform int u_lighting_enabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;"\
		"if(u_lighting_enabled != 1)"\
		"{"\
		"phong_ads_color = vec3(1.0,1.0,1.0);"\
		"}"\
		"else"\
		"{"\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"\
		"vec3 normalized_light0_direction = normalize(light0_direction);"\
		"vec3 normalized_light1_direction = normalize(light1_direction);"\
		"vec3 normalized_light2_direction = normalize(light2_direction);"\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"\
		"vec3 light0_ambient = u_La0*u_Ka;"\
		"vec3 light1_ambient = u_La1 * u_Ka;"\
		"vec3 light2_ambient = u_La2 * u_Ka;"\
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals,normalized_light0_direction),0.0);"\
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals,normalized_light1_direction),0.0);"\
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals,normalized_light2_direction),0.0);"\
		"vec3 light0_diffuse = u_Ld0 * u_Kd * tn_dot_ld0;"\
		"vec3 light1_diffuse = u_Ld1 * u_Kd * tn_dot_ld1;"\
		"vec3 light2_diffuse = u_Ld2 * u_Kd * tn_dot_ld2;"\
		"vec3 light0_reflection_vector = reflect(-normalized_light0_direction,normalized_transformed_normals);"\
		"vec3 light1_reflection_vector = reflect(-normalized_light1_direction,normalized_transformed_normals);"\
		"vec3 light2_reflection_vector = reflect(-normalized_light2_direction,normalized_transformed_normals);"\
		"vec3 light0_specular = u_Ls0 * u_Ks * pow(max(dot(light0_reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		"vec3 light1_specular = u_Ls1 * u_Ks * pow(max(dot(light1_reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		"vec3 light2_specular = u_Ls2 * u_Ks * pow(max(dot(light2_reflection_vector,normalized_viewer_vector),0.0),u_material_shininess);"\
		"phong_ads_color = light0_ambient + light0_diffuse + light0_specular +light1_ambient + light1_diffuse + light1_specular + light2_ambient + light2_diffuse + light2_specular;"\
		"}"\
		"FragColor = vec4(phong_ads_color,1.0) ;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const char **)&fragmentShaderSourceCode, NULL);
	//COMPILE FRAGMENT SHADER
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n FRAGMENT SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n FRAGMENT SHADER COMPILED SUCCESSGFULLY \n");
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_LINK_STATUS, &iShaderLinkStatus);
	if (iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n LINK TIME ERROR LOG: \n%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\nSHADER LINKED SUCCESSFULLY \n");
	}

	//GET ALL UNIFORM LOCATIONS


	gModelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	gViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	gLight0_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");
	gLa0_Uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	gLd0_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	gLs0_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");

	gLight1_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");
	gLa1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	gLd1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	gLs1_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");

	gLight2_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");
	gLa2_Uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
	gLd2_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
	gLs2_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");

	gKa_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	gKd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gKs_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	gMaterial_Shinyness_Uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	//VERTICES,COLOR,SHADER_ATTRIBUTES,vbo,vao initialisation

	//START THE SPHERE RECORDER
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//STARTS PYRAMID RECORDER
	const GLfloat triangleVertices[] =
	{
		/*PHASE ONE*/
		0.0f, 1.0,0.0f,
		-1.0,-1.0,1.0f,
		1.0,-1.0,1.0f,

		/*PHASE TWO*/
		0.0f, 1.0,0.0f,
		1.0,-1.0,1.0f,
		1.0,-1.0,-1.0f,

		/*PHASE THREE*/
		0.0f, 1.0,0.0f,
		1.0,-1.0,-1.0f,
		-1.0,-1.0,-1.0f,

		/*PHASE FOUR*/
		0.0f, 1.0,0.0f,
		-1.0,-1.0,-1.0f,
		-1.0,-1.0,1.0f
	};

	const GLfloat triangleNormals[] =
	{
		/*PHASE ONE*/
		0.0f,0.447214f,0.894427f,
		0.0f,0.447214f,0.894427f,
		0.0f,0.447214f,0.894427f,

		/*PHASE TWO*/
		0.894427f,0.447214f,0.0f,
		0.894427f,0.447214f,0.0f,
		0.894427f,0.447214f,0.0f,

		/*PHASE THREE*/
		0.0f,0.447214f,-0.894427f,
		0.0f,0.447214f,-0.894427f,
		0.0f,0.447214f,-0.894427f,

		/*PHASE FOUR*/
		-0.894427f,0.447214f,0.0f,
		-0.894427f,0.447214f,0.0f,
		-0.894427f,0.447214f,0.0f
	};

	glGenVertexArrays(1, &gVao_Triangle);
	glBindVertexArray(gVao_Triangle);
	glGenBuffers(1, &gVbo_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_Normal_Triangle);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleNormals), triangleNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_Normal_Triangle);
	glBindVertexArray(0);

	//	START THE SPHERE RECORDER

	glGenVertexArrays(1, &gVao_Sphere);
	glBindVertexArray(gVao_Sphere);

	//BIND TO THE VERTEX ARRAY OF SHERE
	glGenBuffers(1, &gVbo_Vertex_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Vertex_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE NORMAL BUFFER
	glGenBuffers(1, &gVbo_Normal_Sphere);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//BIND TO THE SPHERE ELEMENT BUFFER
	glGenBuffers(1, &gVbo_Sphere_Element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//UNBIND FROM THE SPHERE VAO
	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0);

	gPerspectiveProjectionMatrix = mat4::identity();
	gRotationMatrix = mat4::identity();
	gScaleMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle + 0.090f;
	if (gfUpdateAngle >= 360.0f)
	{
		gfUpdateAngle = 0;
	}

	gfRedAngle += 0.001f;
	if (gfRedAngle >= 360.0f)
	{
		gfRedAngle = 0.0f;
	}

	gfGreendAngle += 0.001f;
	if (gfGreendAngle >= 360.0f)
	{
		gfGreendAngle = 0.0f;
	}

	gfBlueAngle += 0.001f;
	if (gfBlueAngle >= 360.0f)
	{
		gfBlueAngle = 0.0f;
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	vmath::mat4 modelMatrix = mat4::identity();
	mat4 ViewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	if (gbLight == true)
	{
		light0_Position[1] = gfRadiousOfLightRotation*cos(gfRedAngle);
		light0_Position[2] = gfRadiousOfLightRotation*sin(gfRedAngle);
		
		light1_Position[0] = gfRadiousOfLightRotation*cos(gfGreendAngle);
		light1_Position[2] = gfRadiousOfLightRotation*sin(gfGreendAngle);

		light2_Position[0] = gfRadiousOfLightRotation*cos(gfBlueAngle);
		light2_Position[1] = gfRadiousOfLightRotation*sin(gfBlueAngle);

		glUniform1i(gLKeyPressedUniform, 1);

		glUniform3fv(gLa0_Uniform, 1, light0_Ambient);
		glUniform3fv(gLd0_Uniform, 1, light0_Diffused);
		glUniform3fv(gLs0_Uniform, 1, Light0_Specular);
		glUniform4fv(gLight0_Position_Uniform, 1, light0_Position);

		glUniform3fv(gLa1_Uniform, 1, light1_Ambient);
		glUniform3fv(gLd1_Uniform, 1, light1_Diffused);
		glUniform3fv(gLs1_Uniform, 1, Light1_Specular);
		glUniform4fv(gLight1_Position_Uniform, 1, light1_Position);


		glUniform3fv(gLa2_Uniform, 1, light2_Ambient);
		glUniform3fv(gLd2_Uniform, 1, light2_Diffused);
		glUniform3fv(gLs2_Uniform, 1, Light2_Specular);
		glUniform4fv(gLight2_Position_Uniform, 1, light2_Position);

		glUniform3fv(gKa_Uniform, 1, materialAmbient);
		glUniform3fv(gKd_Uniform, 1, materialtDiffused);
		glUniform3fv(gKs_Uniform, 1, materialSpecular);
		glUniform1f(gMaterial_Shinyness_Uniform, materialShininess);
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}
	gRotationMatrix = vmath::rotate(gfUpdateAngle, 0.0f, 1.0f, 0.0f);

	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	//modelMatrix = modelMatrix *gRotationMatrix;

	glUniformMatrix4fv(gModelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(gViewMatrixUniform, 1, GL_FALSE, ViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	/*glBindVertexArray(gVao_Triangle);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);*/

	glBindVertexArray(gVao_Sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_Sphere_Element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	glUseProgram(0);

	UpdateAngle();

	SwapBuffers(ghdc);
	return;
}


void resize(int width, int height)
{
	fprintf(fp, "\nINSIDE RESIZE \n");
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (float)width / float(height), 0.1f, 100.0f);
	return;
}


void uninitialize()
{
	if (gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gVbo_Normal_Triangle)
	{
		glDeleteBuffers(1, &gVbo_Normal_Triangle);
		gVbo_Normal_Triangle = 0;
	}
	if (gVbo_Normal_Sphere)
	{
		glDeleteBuffers(1, &gVbo_Normal_Sphere);
		gVbo_Normal_Sphere = 0;
	}
	if (gVao_Triangle)
	{
		glDeleteVertexArrays(1, &gVao_Triangle);
		gVao_Triangle = 0;
	}
	if (gVao_Sphere)
	{
		glDeleteVertexArrays(1, &gVao_Triangle);
		gVao_Triangle = 0;
	}
	if (gVbo_Position)
	{
		glDeleteBuffers(1, &gVbo_Position);
		gVbo_Position = 0;
	}
	if (gVbo_Vertex_Sphere)
	{
		glDeleteBuffers(1, &gVbo_Vertex_Sphere);
		gVbo_Vertex_Sphere = 0;
	}
	if (gVbo_Sphere_Element)
	{
		glDeleteBuffers(1, &gVbo_Vertex_Sphere);
		gVbo_Sphere_Element = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject = 0;


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	if (fp)
	{
		fprintf(fp, "\nPROGRAM ENDED SUCCESSFULLY \n");;
		fclose(fp);
		fp = NULL;
	}

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}