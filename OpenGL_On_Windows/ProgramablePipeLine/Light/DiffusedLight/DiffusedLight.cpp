#include<Windows.h>
#include<stdio.h>

#include<C:/glew/include/gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
using namespace vmath;

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

FILE *fp;


enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE
};

GLuint gVao_Cube;
GLuint gVbo_Position_Cube;
GLuint gVbo_Normal_Cube;

GLuint gMVPUniform;


mat4 gPerspectiveProjectionMatrix;
mat4 gRotationMatrix;
mat4 gScaleMatrix;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float gfUpdateAngle = 0.0f;

//LIGHT RELATED VARIABLES

GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;
GLuint gLdUniform, gKdUniform, gLightPositionUniform;
GLuint gLKeyPressedUniform;


bool gbAnimate = FALSE;
bool gbLight = FALSE;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCndLine, int iCmdShow)
{
	fp = fopen("DIFFUSED_LIGHT.txt", "w");
	if (NULL == fp)
	{
		printf("\nERROR WHILE OPENING LOG FILE \n");
		exit(1);
	}

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("WINDOW");

	BOOL bDone = FALSE;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("IDI_ICON1"));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DIFFUSED_LIGHT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP, TEXT("ERROR WHILE CreateWindow"), TEXT("ERROR"), 0);
		return 0;
	}
	else
	{
		ghwnd = hwnd;
	}

	initialize();

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();

	TCHAR szMessage[500] = { '\0' };

	switch (iMsg)
	{

	case WM_CREATE:
		fprintf(fp, "\nINSIDE WM_CREATE\n");
		break;

	case WM_KEYDOWN:
		if ((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if (gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if (wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if ((wParam == 'A') ||( wParam == 'a'))
		{
			if (gbAnimate == false)
			{
				gbAnimate = true;
			}
			else
			{
				gbAnimate = false;
			}
		}
		else if ((wParam == 'L') || (wParam == 'l'))
		{
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
		}
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case VK_ESCAPE:

		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void initialize()
{

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		fprintf(fp, "\nERROR: error while initialising the GLEW\n");
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	fprintf(fp, "\n OPENGL VERSION: %s ", glGetString(GL_VERSION));
	fprintf(fp, "\n OPENGL SHADER VERSION: %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	//fclose(fp);

	//VERTEX SHADER
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal ;" \
		"uniform mat4 u_model_view_matrix ;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec4 u_light_position;" \
		"out vec3 diffuse_light;" \
		"void main(void)" \
		"{"\
		"if(u_LKeyPressed == 1)"\
		"{"\
		"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"\
		"vec3 tnorm = normalize(mat3(u_model_view_matrix) *vNormal );"\
		"vec3 s = normalize(vec3(u_light_position - eyeCoordinates));"\
		"diffuse_light = u_Ld * u_Kd * max(dot(s,tnorm),0.0);"\
		"}"\
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
		"}";
		
		
		
		
		/*"#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 outColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"outColor=vColor;"\
		"}";*/


	glShaderSource(gVertexShaderObject, 1, (const char **)&vertexShaderSourceCode, NULL);
	//COMPILE VERTEX SHADER

	glCompileShader(gVertexShaderObject);

	GLint iInfoLength = 0;
	char *szInfoLog = NULL;

	GLint iShaderCompileStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\nVERTEX SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n VERTEX SHADER COMPILED SUCCESSGFULLY \n");
	}

	/*FRAGMENT SHADER*/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode =
		"#version 400 core"\
		"\n"\
		"in vec3 diffuse_light;"\
		"out vec4 FragColor;"\
		"uniform int u_LKeyPressed;"\
		"void main(void)"\
		"{"\
		"vec4 Color;"\
		"if(u_LKeyPressed ==1)"\
		"{"\
		"Color = vec4(diffuse_light,1.0);"\
		"}"\
		"else"\
		"{"\
		"Color = vec4(1.0,1.0,1.0,1.0);"\
		"}"\
		"FragColor = Color; "\
		"}";
		
		
		
		
		
		
		/*"#version 400 core" \
		"\n" \
		"in vec4 outColor;"\
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = outColor;" \
		"}";*/


	glShaderSource(gFragmentShaderObject, 1, (const char **)&fragmentShaderSourceCode, NULL);
	//COMPILE FRAGMENT SHADER
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n FRAGMENT SHADER COMPILATION LOG:%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\n FRAGMENT SHADER COMPILED SUCCESSGFULLY \n");
	}

	//CREATE PROGRAM OBJECT AND LINK THE SHADERS
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//PRELINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//LINK THE SHADERPROGRAM OBJECT AND ITS ERROR HANDLING
	glLinkProgram(gShaderProgramObject);

	GLint iShaderLinkStatus = 0;

	glGetShaderiv(gVertexShaderObject, GL_LINK_STATUS, &iShaderLinkStatus);
	if (iShaderLinkStatus == GL_FALSE)
	{
		glGetShaderiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(fp, "\n LINK TIME ERROR LOG: \n%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(fp, "\nSHADER LINKED SUCCESSFULLY \n");
	}

	gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_view_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	gLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_LKeyPressed");
	gLdUniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	gKdUniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	gLightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	//VERTICES,COLOR,SHADER_ATTRIBUTES,vbo,vao initialisation

	const GLfloat rectangleVertices[] =
	{

		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		//BOTTOM FACE
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		//FRONT FACE
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		//BACK FACE
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		//RIGHT FACE
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		//LEFT FACE
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	const GLfloat cubeNormal[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,

		0.0f,-1.0f,0.0f,
		0.0f,-1.0f,0.0f,
		0.0f,-1.0f,0.0f,
		0.0f,-1.0f,0.0f,

		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,

		0.0f,0.0f,-1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,0.0f,-1.0f,
		0.0f,0.0f,-1.0f,

		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,

		-1.0f,0.0f,0.0f,
		-1.0f,0.0f,0.0f,
		-1.0f,0.0f,0.0f,
		-1.0f,0.0f,0.0f
	};

	//START THE RECORDER	---SAME AS glBegin() for RECTANGLE
	glGenVertexArrays(1, &gVao_Cube);
	glBindVertexArray(gVao_Cube);

	//CREATE THE OUR SLOT OF MEMORY INSIDE GL_ARRAY_BUFFER WHICH IS INSIDE FRAMR BUFFER
	glGenBuffers(1, &gVbo_Position_Cube);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position_Cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM VERTEX BUFFER

	//CREATE AND BIND TO THE NORMAL BUFFER
	glGenBuffers(1, &gVbo_Normal_Cube);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal_Cube);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormal), cubeNormal, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//UNBIND FROM COLOR BUFFER


	glBindVertexArray(0);

	//END OF RECORDER ----SAME AS glEnd()


	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0);

	gPerspectiveProjectionMatrix = mat4::identity();
	gRotationMatrix = mat4::identity();
	gScaleMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void UpdateAngle()
{
	gfUpdateAngle = gfUpdateAngle + 0.090f;
	if (gfUpdateAngle >= 360.0f)
	{
		gfUpdateAngle = 0;
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glUseProgram(gShaderProgramObject);
	vmath::mat4 modelMatrix = mat4::identity();
	mat4 ModelViewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	if (gbLight == true)
	{
		glUniform1i(gLKeyPressedUniform, 1);
		glUniform3f(gLdUniform, 1.0f, 1.0f, 1.0f);
		glUniform3f(gKdUniform, 0.5f, 0.5f, 0.5f);

		float lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };
		glUniform4fv(gLightPositionUniform, 1, (GLfloat *)lightPosition);
	}
	else
	{
		glUniform1i(gLKeyPressedUniform, 0);
	}

	gScaleMatrix = scale(0.75f, 0.75f, 0.75f);
	
	modelMatrix = translate(0.0f, 0.0f, -5.0f);
	RotationMatrix = rotate(gfUpdateAngle, gfUpdateAngle, gfUpdateAngle);
	ModelViewMatrix = modelMatrix * RotationMatrix;
	ModelViewMatrix = ModelViewMatrix *gScaleMatrix;

	glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, ModelViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	
	//NOW DRAW RECTANGLE BEFORE THAT MAKE IDENTITY TO modelViewMatrix and modelViewProjectionMatrix
	

	glBindVertexArray(gVao_Cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

glUseProgram(0);

	UpdateAngle();

	SwapBuffers(ghdc);
	return;
}


void resize(int width, int height)
{
	fprintf(fp, "\nINSIDE RESIZE \n");
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (float)width / float(height), 0.1f, 100.0f);
	return;
}


void uninitialize()
{
	if (gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gVbo_Normal_Cube)
	{
		glDeleteBuffers(1, &gVbo_Normal_Cube);
		gVbo_Normal_Cube = 0;
	}
	if (gVbo_Position_Cube)
	{
		glDeleteBuffers(1, &gVbo_Position_Cube);
		gVbo_Position_Cube = 0;
	}
	if (gVao_Cube)
	{
		glDeleteVertexArrays(1, &gVao_Cube);
		gVao_Cube = 0;
	}
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	glDeleteShader(gShaderProgramObject);
	gVertexShaderObject = gFragmentShaderObject = gShaderProgramObject = 0;


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	if (fp)
	{
		fprintf(fp, "\nPROGRAM ENDED SUCCESSFULLY \n");;
		fclose(fp);
		fp = NULL;
	}

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}