#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen

int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT DEATHLY HOLLOWS"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void DrawDeathlyHollowsSymbol()
{
	float fCounter = 0.02f;

	GLint circle_points = 1000;
	double angle;
	
	GLfloat glf_XCordUp = 0.0f;
	GLfloat glf_YCordUp = 0.8f;
	
	GLfloat glf_XCordLeft = -0.8f;
	GLfloat glf_YCordLeft = -0.8f;

	GLfloat glf_XCordRight = 0.8f;
	GLfloat glf_YCordRight = -0.8f;
	
	//----------------------- CALCULATE THE LENGTH OF EACH SIDE BY DISTANCE FORMUKLA -----------------------
	GLfloat glf_SideOne = sqrt(pow((glf_XCordUp - glf_XCordLeft),2) + pow((glf_YCordUp - glf_YCordLeft),2));
	GLfloat glf_SideTwo = sqrt(pow((glf_XCordRight - glf_XCordLeft),2) + pow((glf_YCordRight - glf_YCordLeft),2));
	GLfloat glf_SideThree = sqrt(pow((glf_XCordRight - glf_XCordUp),2) + pow((glf_YCordRight - glf_YCordUp),2));

	GLfloat glf_TriPerimetr  (glf_SideOne + glf_SideTwo + glf_SideThree);

	//---------------------- CALCULATE THE X AND Y CO-ORDINATES OF CIRCLE --------------------------
	GLfloat glf_XCircleCentr = ( ((glf_SideOne *glf_XCordRight ) + (glf_SideTwo * glf_XCordUp) + (glf_SideThree * glf_XCordLeft)) / glf_TriPerimetr);
	GLfloat glf_YCircleCentr = ( ((glf_SideOne *glf_YCordRight ) + (glf_SideTwo * glf_YCordUp) + (glf_SideThree * glf_YCordLeft)) / glf_TriPerimetr);

	GLfloat glf_SemiTrianglePerimeter = glf_TriPerimetr /2.0f;
	
	//--------------------- HERONES FORMULA -----------------------------
	GLfloat glf_TriangleArea = sqrt((glf_SemiTrianglePerimeter)*(glf_SemiTrianglePerimeter - glf_SideOne)*(glf_SemiTrianglePerimeter - glf_SideTwo)*(glf_SemiTrianglePerimeter - glf_SideThree));
	

	GLfloat glf_CircleRadious = (2*glf_TriangleArea)/ glf_TriPerimetr ;

	//------------------------ CALCULATE THE HEIGHT OF THE TRIANGLE ---------------------------
	GLfloat  glf_XMidOfBase;
	if(glf_XCordLeft == glf_XCordRight)
	{
		 glf_XMidOfBase = (glf_XCordLeft);
	}
	else
	{
		 glf_XMidOfBase = (glf_XCordLeft + glf_XCordRight);
	}
	
	GLfloat glf_YMidOfBase ;

	if(glf_YCordLeft == glf_YCordRight)
	{
		glf_YMidOfBase = (glf_YCordLeft);
	}
	else
	{		
		glf_YMidOfBase = (glf_YCordLeft + glf_YCordRight);
	}
	glBegin(GL_LINES);
		
		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);
		glVertex3f(glf_XCordLeft,glf_YCordRight,0.0f);

		glVertex3f(glf_XCordLeft,glf_YCordRight,0.0f);
		glVertex3f(glf_XCordRight,glf_YCordRight,0.0f);

		glVertex3f(glf_XCordRight,glf_YCordRight,0.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);

		glColor3f(1.0f,0.0f,1.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);
		glVertex3f(glf_XMidOfBase,glf_YMidOfBase,0.0f);
				
	glEnd();

	glBegin(GL_LINE_LOOP);
		glColor3f(0.0f,1.0f,1.0f);
			
		for(int i=0; i < circle_points; i++)
		{
			angle = 2 * PI * i/circle_points;
			glVertex2f((GLfloat)((glf_CircleRadious *cos(angle)) + glf_XCircleCentr),(GLfloat)((glf_CircleRadious *sin(angle))+ glf_YCircleCentr));
		}
		
		
   glEnd();
	return;
}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	DrawDeathlyHollowsSymbol();
	
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	if(height == 0)	
	{
		height = 1;
	}
	glViewport(0.0f,0.0f,(GLsizei)width,(GLsizei)height);
}

void uninitialize(void)
{
	
}

