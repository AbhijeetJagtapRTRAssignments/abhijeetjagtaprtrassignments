#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen
void DrawColoredTriangle();

int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT TEMPLATE"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	glLoadIdentity();
	glTranslatef(00.0f,0.0f,-3.0f);

	DrawColoredTriangle();
	
	glutSwapBuffers();
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-50.0f,50.0f,-50.0f,50.0f,-50.0f,50.f);
	return;
}

void DrawWhiteTriangle()
{
	glBegin(GL_TRIANGLES);
		glColor3f(1.00f,1.0f,1.0f);

		glVertex3f(0.0f,50.0f,0.0f);
		glVertex3f(-50.0f,-50.0f,0.0f);
		glVertex3f(50.0f,-50.0f,0.0f);

	glEnd();

}

void DrawColoredTriangle()
{
	glBegin(GL_TRIANGLES);

		glColor3f(0.50f,0.0f,0.0f);
		glVertex3f(0.0f,50.0f,0.0f);

		glColor3f(0.0f,0.8f,0.0f);
		glVertex3f(-50.0f,-50.0f,0.0f);

		glColor3f(0.0f,0.0f,0.6f);
		glVertex3f(50.0f,-50.0f,0.0f);

		
	glEnd();
	return;
}

void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}


void uninitialize(void)
{
	
}

