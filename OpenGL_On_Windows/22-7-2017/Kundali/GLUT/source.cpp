#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen

int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT KUNDALI"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void DrawKundali()
{
	float fCounter = 0.02f;
	
	GLfloat glf_XCordRight = 1.0f;
	GLfloat glf_XCordLeft = -1.0f;
	GLfloat glf_YCordUp = 1.0f;
	GLfloat glf_YCordDown = -1.0f;

	glBegin(GL_LINES);
		glColor3f(0.0f,1.0f,1.0f);
		
	
			glf_YCordUp = glf_YCordUp - fCounter;
			glf_YCordDown = glf_YCordDown + fCounter;
			glf_XCordLeft = glf_XCordLeft + fCounter;
			glf_XCordRight = glf_XCordRight - fCounter;

			glVertex3f(glf_XCordLeft, glf_YCordUp, 0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);

			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);

			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordUp, 0.0f);

			glVertex3f(glf_XCordRight, glf_YCordUp, 0.0f);
			glVertex3f(glf_XCordLeft, glf_YCordUp, 0.0f);

			glColor3f(1.0f,1.0f,0.0f);

			glVertex3f(glf_XCordLeft,0.0f,0.0f);
			glVertex3f(0.0f,glf_YCordUp,0.0f);

			glVertex3f(0.0f,glf_YCordUp,0.0f);
			glVertex3f(glf_XCordRight,0.0f,0.0f);

			glVertex3f(glf_XCordRight,0.0f,0.0f);
			glVertex3f(0.0f,glf_YCordDown,0.0f);

			glVertex3f(0.0f,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordLeft,0.0f,0.0f);

			glVertex3f(glf_XCordRight,glf_YCordUp,0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);

			glVertex3f(glf_XCordRight,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordUp,0.0f);
		
	glEnd();
	return;
}


void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	DrawKundali();
	
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	if(height == 0)	
	{
		height = 1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
}

void uninitialize(void)
{
	
}

