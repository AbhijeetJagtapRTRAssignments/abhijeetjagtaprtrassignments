#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen
struct Color
{
	GLfloat Red;
	GLfloat Green;
	GLfloat Blue;
};


void DifferentColor(struct Color *, int iCounter);

int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT Concentric Rectangles"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void DrawConcentricRectangles(int iNumOfTriangles)
{
	float fCounter = 0.02f;
	struct Color objColor;

	GLfloat glf_XCordRight = 1.0f;
	GLfloat glf_XCordLeft = -1.0f;
	GLfloat glf_YCordUp = 1.0f;
	GLfloat glf_YCordDown = -1.0f;

	if(iNumOfTriangles < 1)
	{
		return;
	}
		
		for(int i = 0;  i < iNumOfTriangles; i++)
		{
			DifferentColor(&objColor,i);
			glf_YCordUp = glf_YCordUp - fCounter;
			glf_YCordDown = glf_YCordDown + fCounter;
			glf_XCordLeft = glf_XCordLeft + fCounter;
			glf_XCordRight = glf_XCordRight - fCounter;

			if((glf_YCordUp <= fCounter) || (glf_XCordRight <= fCounter))
			{
				break;
			}

			glBegin(GL_LINES);
			glColor3f(objColor.Red,objColor.Green, objColor.Blue);

			glVertex3f(glf_XCordLeft, glf_YCordUp, 0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);

			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);

			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordUp, 0.0f);

			glVertex3f(glf_XCordRight, glf_YCordUp, 0.0f);
			glVertex3f(glf_XCordLeft, glf_YCordUp, 0.0f);

			glEnd();
		}
		
	
	return;
}

void DifferentColor(struct Color *objPtr, int iCounter)
{
	switch(iCounter)
	{
		case 1:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 2:
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 3:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
		break;
		case 4:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;		
		break;
		case 5:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 6:	
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 7:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 8:
			objPtr->Red   = 0.5f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 9:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 0:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;

	}
}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	DrawConcentricRectangles(10);
	
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	if(height == 0)	
	{
		height = 1;
	}
	
	glViewport(0,0,(GLsizei)width, (GLsizei)height);
}


void uninitialize(void)
{
	
}

