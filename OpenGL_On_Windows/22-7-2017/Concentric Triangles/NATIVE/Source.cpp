#include<Windows.h>
#include<stdio.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

float gfRed;
float gfGreen;
float gfBlue;
int giColorDecider = 0;


struct Color
{
	GLfloat Red;
	GLfloat Green;
	GLfloat Blue;
};

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);
void GetColor();

void DrawConcentricTriangles(int iNumOfTriangles);
void DifferentColor(struct Color *, int iCounter);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= L"WINDOW";

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Concentric Triangles Native"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	break;
	case VK_ESCAPE:
		
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	
	return;
}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	
	DrawConcentricTriangles(10);
	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
	return;
}

void DrawConcentricTriangles(int iNumOfTriangles)
{
	float fCounter = 0.02f;

	struct Color objColor;
	
	GLfloat glf_XCordRight = 1.0f;
	GLfloat glf_XCordLeft = -1.0f;
	GLfloat glf_YCordUp = 1.0f;
	GLfloat glf_YCordDown = -1.0f;

	if(iNumOfTriangles < 1)
	{
		return;
	}
	/*if(giColorDecider >= 8)
	{
		giColorDecider = 0;
	}
	GetColor();
	*/	
	
		for(int i = 0;  i < iNumOfTriangles; i++)
		{
			DifferentColor(&objColor,i);
			glf_YCordUp = glf_YCordUp - fCounter;
			glf_YCordDown = glf_YCordDown + fCounter;
			glf_XCordLeft = glf_XCordLeft + fCounter;
			glf_XCordRight = glf_XCordRight - fCounter;

			if((glf_YCordUp <= fCounter) || (glf_XCordRight <= fCounter))
			{
				break;
			}
			glBegin(GL_LINES);
			glColor3f(objColor.Red,objColor.Green, objColor.Blue);
			glVertex3f(0.0f, glf_YCordUp, 0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordLeft,glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);
			glVertex3f(glf_XCordRight, glf_YCordDown,0.0f);
			glVertex3f(0.0f, glf_YCordUp, 0.0f);
			glEnd();
		}
		
		
	
	return;
}
void DifferentColor(struct Color *objPtr, int iCounter)
{
	switch(iCounter)
	{
		case 1:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 2:
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 3:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
		break;
		case 4:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;		
		break;
		case 5:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 6:	
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 7:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 8:
			objPtr->Red   = 0.5f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 9:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 0:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;

	}
}

void GetColor()
{
	if(giColorDecider == 0)
	{
		gfRed = gfGreen = gfBlue = 0.0f; 
	}
	else
	{
		if(giColorDecider > 3 )
		{
			gfRed = 1.0f;
		}
		else
		{
			gfRed = 0.0f;
		}
		if(((giColorDecider % 2) == 0) && (giColorDecider != 4))
		{
			gfGreen = 1.0f;
		}
		if((giColorDecider%2) == 1)
		{
			gfBlue = 1.0;
		}
	}
}
void uninitialize()
{
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}