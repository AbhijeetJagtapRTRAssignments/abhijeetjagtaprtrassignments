#include <GL/freeglut.h>
#include<math.h>

#define PI 3.1415926535898

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen

struct Color
{
	GLfloat Red;
	GLfloat Green;
	GLfloat Blue;
};
int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("CONCENTRICCIRCLES GLUT "); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}

void DifferentColor(struct Color *objPtr, int iCounter)
{
	switch(iCounter)
	{
		case 1:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 2:
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;
		break;
		case 3:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
		break;
		case 4:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 0.0f;		
		break;
		case 5:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 6:	
			objPtr->Red   = 0.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 7:
			objPtr->Red   = 1.0f;
			objPtr->Green = 1.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 8:
			objPtr->Red   = 0.5f;
			objPtr->Green = 0.0f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 9:
			objPtr->Red   = 0.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;
		case 10:
			objPtr->Red   = 1.0f;
			objPtr->Green = 0.5f;
			objPtr->Blue  = 1.0f;
	
		break;

	}
}

void ConcentricCirclesByLineLoop(int iNumOfCircles)
{
	float fCounter = 1.0f;

	GLint circle_points = 1000;
	GLfloat glfRed = 0.1f;
	GLfloat glfGreen= 0.5f;
	GLfloat glfBlue= 0.8f;
	
	struct Color objColor;

	double angle;
	
	double dXCord = 0.0f;
	double dYCord = 0.0f;

	if(iNumOfCircles < 1)
	{
		return;
	}
	while(1)
		{
			DifferentColor(&objColor, iNumOfCircles);
			glBegin(GL_LINE_LOOP);
			glColor3f(objColor.Red,objColor.Green, objColor.Blue);
			
				for(int i=0; i < circle_points; i++)
				{
					angle = 2 * PI * i/circle_points;
					dXCord = fCounter*cos(angle);
					dYCord = fCounter*sin(angle);
					glVertex2f((GLfloat)dXCord,(GLfloat)dYCord);
				}
			glEnd();

		fCounter = fCounter -0.03f;
		iNumOfCircles = iNumOfCircles - 1;
		if((fCounter <= 0.0f) || (iNumOfCircles <= 0) )
		{
			break;
		}
		}
	
}

void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	ConcentricCirclesByLineLoop(30);
	//DrawConcentricCircles(50);
	
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f,0.0f,0.0f,0.0f); //blue 
}

void keyboard(unsigned char key,int x,int y)
{
	switch(key)
	{
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width,int height)
{
	if(height == 0)	
	{
		height = 1;
	}
	glViewport(0.0f,0.0f,(GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	
}

