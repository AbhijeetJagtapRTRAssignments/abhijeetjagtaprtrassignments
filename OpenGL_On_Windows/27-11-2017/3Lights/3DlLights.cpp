#include<Windows.h>
#include<stdio.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLuint Texture_Stone;
GLuint Texture_Smiley;


float gfUpdateAngleCube    = 0.0f;
float gfUpdateAnglePyramid = 0.0f;
float gfUpdateAngleShere   = 0.0f;


void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

int LoadTexture(GLuint *Texture, TCHAR imageResourceId[]);

GLfloat lightRed_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat lightRed_diffused[] = {1.0f,0.0f,0.0f,0.0f};
GLfloat lightRed_specular[] = {1.0f,0.0f,0.0f,0.0f};
GLfloat lightRed_position[] = {0.0f,0.0f,0.0f,0.0f};

GLfloat lightGreen_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat lightGreen_diffused[] = {0.0f,1.0f,0.0f,0.0f};
GLfloat lightGreen_specular[] = {0.0f,1.0f,0.0f,0.0f};
GLfloat lightGreen_position[] = {0.0f,0.0f,0.0f,0.0f};

GLfloat lightBlue_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat lightBlue_diffused[] = {0.0f,0.0f,1.0f,0.0f};
GLfloat lightBlue_specular[] = {0.0f,0.0f,1.0f,0.0f};
GLfloat lightBlue_position[] = {0.0f,0.0f,0.0f,0.0f};

GLfloat material_ambient[]  = {0.0f,0.0f,0.0f,0.0f};
GLfloat material_diffused[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat material_specular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat material_shinynes[] = {50.0f};

float gfAngleRed   = 0.0f;
float gfAngleBlue  = 0.0f;
float gfAngleGreen = 0.0f;

BOOL gbLihting = FALSE;
GLUquadric *quadric = NULL;

//GLboolean glbPyramid = GL_TRUE;
//GLboolean glbCube    = GL_FALSE;
GLboolean glbSphere  = GL_TRUE;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= TEXT("WINDOW");

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("LIGHT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if((wParam == 'L' )|| (wParam == 'l'))
		{
			if(gbLihting == FALSE)
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_LIGHT0);
				gbLihting = TRUE ;
			}
			else
			{
				glDisable(GL_LIGHT0);
				glDisable(GL_LIGHTING);
				gbLihting = FALSE;
			}
		}
		else if((wParam == 'S') || (wParam == 's'))
		{	
			glbSphere  = GL_TRUE;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f, 1.0);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightRed_diffused);
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightRed_ambient);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightRed_specular);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_DIFFUSE,lightGreen_diffused);
	glLightfv(GL_LIGHT1,GL_AMBIENT,lightGreen_ambient);
	glLightfv(GL_LIGHT1,GL_SPECULAR,lightGreen_specular);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2,GL_DIFFUSE,lightBlue_diffused);
	glLightfv(GL_LIGHT2,GL_AMBIENT,lightBlue_ambient);
	glLightfv(GL_LIGHT2,GL_SPECULAR,lightBlue_specular);
	glEnable(GL_LIGHT2);
	
	glMaterialfv(GL_FRONT,GL_DIFFUSE,material_diffused);
	glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambient);
	glMaterialfv(GL_FRONT,GL_SPECULAR,material_specular);
	glMaterialfv(GL_FRONT,GL_SHININESS,material_shinynes);
	

	quadric = gluNewQuadric();

	resize(WIN_WIDTH,WIN_HEIGHT);
}

void UpdateAngle()
{
	gfAngleRed   =  gfAngleRed + 0.1f;
	if(gfAngleRed  >= 360.0f )
	{
		gfAngleRed = 0.0f;  
	}
	
	gfAngleGreen =  gfAngleGreen + 0.1f;
	if(gfAngleGreen  >= 360.0f)
	{
		gfAngleGreen  = 0.0f;
	}
	
	gfAngleBlue  =  gfAngleBlue +0.1f;
	if(gfAngleBlue >= 360.0f)
	{
		gfAngleBlue = 0.0f;
	}
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-3.0f);
  
  glPushMatrix();
    
    gluLookAt(0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	
	glPushMatrix();
		glRotatef(gfAngleRed,0.1f,0.0f,0.0f);
		lightRed_position[1] = gfAngleRed;
		glLightfv(GL_LIGHT0,GL_POSITION,lightRed_position);
	glPopMatrix();

	glPushMatrix();
		glRotatef(gfAngleGreen,0.0f,1.0f,0.0f);
		lightGreen_position[0] = gfAngleGreen;
		glLightfv(GL_LIGHT1,GL_POSITION,lightGreen_position);
	glPopMatrix();

	glPushMatrix();
		glRotatef(gfAngleBlue,0.0f,1.0f,0.0f);
		lightBlue_position[2] = gfAngleBlue;
		glLightfv(GL_LIGHT2,GL_POSITION,lightBlue_position);
	glPopMatrix();

	if(glbSphere == GL_TRUE)
	{
		glRotatef(90.0f,1.0f,0.0f,0.0f);
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
		//glRotatef(gfUpdateAngleShere,0.0f,0.0f,1.0f);
		gluSphere(quadric,0.75,30,30);
	}	
  glPopMatrix();

	UpdateAngle();
	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}


void uninitialize()
{
	if(quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}