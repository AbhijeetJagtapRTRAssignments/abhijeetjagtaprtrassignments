#include <GL/freeglut.h>

//global variable declaration
bool bFullscreen=false; //variable to toggle for fullscreen


static int Shoulder = 0;
static int Elbow = 0;


int main(int argc,char** argv)
{
	//function prototypes
	void display(void);
	void resize(int,int);
	void keyboard(unsigned char,int,int);
	void mouse(int,int,int,int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800,600); //to declare initial window size
	glutInitWindowPosition(100,100); //to declare initial window position
	glutCreateWindow("GLUT ROBOTIC ARM"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

//	return(0); 
}


void RoboticArm()
{
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	glTranslatef(0.0f,0.0f,-12.0f);
	glPushMatrix();		//MODEL TRANSFORMTION	--->PUSH 1
	
	glRotatef((GLfloat)Shoulder,0.0f,0.0f,1.0f);
	glTranslatef(1.0f,0.0f,0.0f);
	
	glPushMatrix();		//FOR REVOLUTION	--->PUSH 2
	glScalef(2.0f,0.5f,1.0f);

	
	glColor3f(0.5f,0.3f,0.05f);
	glutSolidSphere(0.5f,10,10);
	
	glPopMatrix();		//HERE ONE POLYGON GETS DRAWED AND SAVED --
	glTranslatef(1.0f,0.0f,0.0f);
	
	glRotatef((GLfloat)Elbow,0.0f,0.0f,1.0f);
	glTranslatef(1.0f,0.0f,0.0f);
	glPushMatrix();

	glScalef(2.0f,0.5f,1.0f);
	
	glColor3f(0.5f,0.35f,0.05f);
	glutSolidSphere(0.5,10,10);
	glPopMatrix();
	
	glPopMatrix();


}
void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	
	gluLookAt(0.0f,0.0f,5.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);
	RoboticArm();

	glutSwapBuffers();
}

void initialize(void)
{
	glClearColor(0.0f,0.0f,0.0f,0.0f); //black 
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
}

void keyboard(unsigned char key,int x,int y)
{
 switch(key)
 {
	case 27: 
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if(bFullscreen==false)
		{
			glutFullScreen();
			bFullscreen=true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen=false;
		}
	break;
	
	case 'S':
		Elbow =(Elbow + 6) %360;

	break;
	
	case 's':
		Elbow =(Elbow - 6) %360;
	break;
	
	case 'E':
		Shoulder =(Shoulder + 3) %360;
	break;
	
	case 'e':
		Shoulder =(Shoulder - 3) %360;
	break;

	default:
		break;
 }
}

void mouse(int button,int state,int x,int y)
{
	switch(button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}



void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glFrustum(-20.0f, 20.0f, -20.0f, 20.0f, 0.1f, 100.0f);

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}



void uninitialize(void)
{
	
}

