//#ifndef _LIST_H_
//#define _LIST_H_

#include <iostream>
using namespace std;

#define TRUE 1
#define FALSE 0

typedef int BOOL;
typedef BOOL t_result;
typedef class List * t_ptrList;
typedef class ImplementList * t_ptrImplementList;
 



class List
{
 protected:
	typedef struct node
	{
		int iData;
		struct node *nextNode;
	} node_t;

	node_t *DummyHead;
	int iNumberofNode;
 public:
   
	virtual t_result InsertFirst() = 0;
	virtual t_result InsertLast()  = 0;
	virtual t_result DeleteFirst() = 0;
	virtual t_result DeleteLast()  = 0;
	virtual t_result FindAt()      = 0; 
   virtual t_result Display()    = 0;
   static t_ptrImplementList CreateObject();
   //List();//                 = 0;
};

class ImplementList : public List
{
 private:
    t_result CreateNode();
    node_t *tempNode;
   
 public:
    //class ImplementList * ptrImplementList;
   ImplementList();
   t_result InsertFirst();
   t_result InsertLast();
   t_result DeleteFirst();
   t_result DeleteLast();
   t_result FindAt();
   t_ptrList CreateObject();
   t_result Display();
};

//#endif
