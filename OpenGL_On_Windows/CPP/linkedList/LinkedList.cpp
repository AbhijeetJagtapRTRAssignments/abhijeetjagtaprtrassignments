#include"LinkedList.h"


MyLinkedList ::MyLinkedList()
{
	iNumberOfNodes = 0;
	temp = NULL;
}

MyLinkedList ::~MyLinkedList()
{
	if(head != NULL)
	{
		DestroyLinkedList(head);
	}
}

void MyLinkedList ::CreateNode(int iData)
{
	temp =( t_node_t *)malloc(sizeof(t_node_t));
	if(NULL == temp)
	{
		printf("\n Error while allocating memory to Node\n");
		exit (1);
	}
	temp->no =iData;
	temp->next = NULL;
	iNumberOfNodes = iNumberOfNodes+1;
}
void MyLinkedList ::InsertFirst(t_node_t **ptrFirst, int data)
{
	InsertAt(ptrFirst,1, data);
}

void MyLinkedList ::InsertAt(t_node_t **ptrFirst, int iLcation, int data)
{
	int iCounter = 1;
	t_node_t *localTemp = NULL, *localtemp2 = NULL;


	if(iLcation > (iNumberOfNodes + 1))
	{
		printf("\nLinked List contains only :%d nodes...So can not insert data at %d Location \n",iNumberOfNodes,iLcation);
		return;
	}

	CreateNode(data);
	
	if(1 == iLcation)
	{
		if(*ptrFirst == NULL)
		{
			*ptrFirst = temp;
		}
		else
		{
			temp->next = *ptrFirst;
			*ptrFirst = temp;
		}			
		(*ptrFirst)->no = data;
		head = (*ptrFirst);
	}
	else
	{
		localTemp = (*ptrFirst);
		while(iCounter < (iLcation -1))
		{
			localTemp = localTemp->next;
			iCounter++;
		}
		localtemp2 = localTemp->next;
		
		localTemp->next = temp;
		temp->next = localtemp2;
	}

}

void MyLinkedList ::DisplayLinkedList(t_node_t *ptrFirst)
{
	int iCounter = 1;
	while(ptrFirst != NULL)
	{
		printf("\nIndex <%d> , Data <%d>\n",iCounter, ptrFirst->no);
		ptrFirst = ptrFirst->next;
		iCounter++;
	}
}

void MyLinkedList ::DestroyLastNode(t_node_t * ptrNode)
{
	if(0 == iNumberOfNodes)
	{
		printf("\n Linked List is empty!!!\n");
		return;
	}

	t_node_t *localTemp = ptrNode;
	
	if(localTemp->next == NULL)
	{
		free(localTemp);
		ptrNode = localTemp = NULL;
		iNumberOfNodes -=1;
		return;
	}
	while(localTemp->next->next != NULL)
	{
		localTemp = localTemp->next;
	}
	free(localTemp->next);
	localTemp->next = NULL;
	iNumberOfNodes -=1;
return;	
}

void MyLinkedList ::DestroyLinkedList(t_node_t *ptrNode)
{
	while(0 == iNumberOfNodes)
	{
		DestroyLastNode(ptrNode);
	}
}