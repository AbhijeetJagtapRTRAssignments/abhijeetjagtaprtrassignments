#include<stdio.h>

#include<stdlib.h>

typedef struct t_node
{
	int no;
	struct t_node *next;
}t_node_t;

class MyLinkedList
{
private:
	int iNumberOfNodes;
public:
	t_node_t *head,*temp;
	
	MyLinkedList();
	~MyLinkedList();

	void InsertFirst(t_node_t **ptrFirst, int data);
	void InsertAt(t_node_t **ptrFirst,int i, int data);
	
	void CreateNode(int iData);

	void DestroyLastNode(t_node_t *ptrNode);
	void DestroyLinkedList(t_node_t *ptrNode);

	void DisplayLinkedList(t_node_t *ptrHead);
};

