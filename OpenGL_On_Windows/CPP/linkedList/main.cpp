#include<iostream>
#include"LinkedList.h"

using namespace std;

int main()
{
	printf("\nMain\n");
	t_node_t *head = NULL;
	 MyLinkedList obj;
	 obj.InsertFirst(&head,1);
	 obj.InsertFirst(&head,2);
	 obj.InsertFirst(&head,3);
	 obj.InsertFirst(&head,4);
	 obj.InsertFirst(&head,5);
	 obj.InsertAt(&head,7,15);

	 obj.DisplayLinkedList(head);

	 obj.DestroyLastNode(head);
	 obj.DisplayLinkedList(head);
	return 0;
}