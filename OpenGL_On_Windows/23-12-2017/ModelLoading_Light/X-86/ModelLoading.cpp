#include<Windows.h>
#include<stdio.h>
#include<vector>

#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;


float gfUpdateAngle;
float gfUpdateYCord = 0;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

FILE *g_fp_LogFile =NULL;
float g_fl_angle_y =0.0f;

/*-------------- OBJ FILE LOADING SPECIFIC -------------------*/

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3		//Minimum number of entries in face data

//Vector of vector of floats to hold vertex data
std::vector<std::vector<float>> g_vertices;

//Vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;

//Vector of vector of floats to hold Normal data
std::vector<std::vector<float>> g_normals;

//Vector of vector of int to hold face data
std::vector<std::vector<int>> g_face_tri,g_face_texture,g_face_normals;

FILE *g_fp_MeshFile =NULL;

char line[BUFFER_SIZE];

/*----------------- LIGHTING SPECIFIC ------------------*/

GLfloat light_ambient[]  = {0.5f,0.5f,0.5f,1.0f};
GLfloat light_diffused[] = {1.0f,0.0f,0.0f,0.0f};
GLfloat light_specular[] = {0.5f,0.5f,0.5f,1.0f};
GLfloat light_position[] = {0.0f,0.0f,01.0f,0.0f};
BOOL gbLihting = FALSE;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	g_fp_LogFile = fopen("LogFile.txt","w+");
	if(g_fp_LogFile == NULL)
	{
		printf("\n Error while creating log file\n");
		exit(1);
	}
	fprintf(g_fp_LogFile,"\n Log File Opened successfully\n");

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= TEXT("WINDOW");

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("LIGHT"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		//MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",TEXT("ERROR"),0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
		else if((wParam == 'L' )|| (wParam == 'l'))
		{
			if(gbLihting == FALSE)
			{
				glEnable(GL_LIGHTING);
				glEnable(GL_LIGHT0);
				gbLihting = TRUE ;
			}
			else
			{
				glDisable(GL_LIGHT0);
				glDisable(GL_LIGHTING);
				gbLihting = FALSE;
			}
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	break;
	case VK_ESCAPE:
		
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	void LoadMeshFile(char *szFilename);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f, 1.0);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffused);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_position);

	glEnable(GL_LIGHT0);

	LoadMeshFile("MonkeyHead.OBJ");

	resize(WIN_WIDTH,WIN_HEIGHT);
}

void updateangle()
{
	g_fl_angle_y += 0.1f;

	if(g_fl_angle_y > 360.0f)
	{
		g_fl_angle_y = 0.0f;
	}
}

void display()
{
	fprintf(g_fp_LogFile,"\n Inside Display\n");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-5.0f);
	glScalef(1.5f,1.5f,1.5f);
	glRotatef(g_fl_angle_y,0,1,0);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	
	for(int i=0; i !=g_face_tri.size(); i++)
	{
		glBegin(GL_TRIANGLES);
		for(int j =0; j != g_face_tri[i].size(); j++)
		{
				int vi = g_face_tri[i][j] -1;
				//glNormal3f(g_normals[vi][0],g_normals[vi][1],g_normals[vi][2]);
				glVertex3f(g_vertices[vi][0],g_vertices[vi][1],g_vertices[vi][2]);
		}
		glEnd();

	}

	updateangle();
	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glFrustum(-20.0f, 20.0f, -20.0f, 20.0f, 0.1f, 100.0f);

	gluPerspective(45.0f,((GLfloat)width/(GLfloat)height),0.1f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return;
}


void uninitialize()
{
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	if(g_fp_LogFile)
	{
		fclose(g_fp_LogFile);
		g_fp_LogFile = NULL;
	}

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}


void LoadMeshFile(char *szFilename)
{	
	if(szFilename == NULL)
	{
		fprintf(g_fp_LogFile,"\n Empty fileName string!!!\n");
		exit(1);
	}
	
	g_fp_MeshFile = fopen(szFilename,"r");
	if(g_fp_MeshFile == NULL)
	{
		fprintf(g_fp_LogFile,"\n Error while openinhg file \n");
		exit(1);
	}

	//Seprator strings
	char *sep_space = " ";
	char *sep_fslash = "/";

	//Character pointer for holding first word
	char *first_token =NULL;
	//Character pointer for holding word seperated by specified seperator strtok
	char *token = NULL;

	//Character pointers for ho;ding strings associated with 
	//1] vertex index	2] Texture Index	3]Normal Index
	char *token_vertex_index= NULL,*token_texture_index= NULL,*token_normal_index= NULL;
	char *Face_tokens[NR_FACE_TOKENS];

	int nr_tokens;

	while(fgets(line,BUFFER_SIZE,g_fp_MeshFile))
	{
		first_token = strtok(line,sep_space);

		if(strcmp(first_token,"v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for(int i = 0; i!=NR_POINT_COORDS; i++)
			{
				vec_point_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_vertices.push_back(vec_point_coord);

		}
		else if(strcmp(first_token,"vt")== S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for(int i =0; i != NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_normals.push_back(vec_texture_coord);
		}
		else if(strcmp(first_token,"vn")== S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for(int i =0; i != NR_NORMAL_COORDS; i++)
			{
				vec_normal_coord[i] = (float)atof(strtok(NULL,sep_space));
			}
			g_normals.push_back(vec_normal_coord);
		}
		else if(strcmp(first_token,"f")== S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3),texture_vertex_indices(3),normal_vertex_indices(3);

			memset((void *)Face_tokens,0,NR_FACE_TOKENS);

			nr_tokens =0;

			while(token = strtok(NULL,sep_space))
			{
				if(strlen(token) < 3)
				{
					break;
				}
				Face_tokens[nr_tokens] = token;
				nr_tokens++;
			}
	
			for(int i =0; i !=NR_FACE_TOKENS; i++)
			{
				token_vertex_index  = strtok(Face_tokens[i],sep_fslash);
				token_texture_index = strtok(NULL,sep_fslash);
				token_normal_index  = strtok(NULL,sep_fslash);

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i]  = atoi(token_texture_index);
				normal_vertex_indices[i]   = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(triangle_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void *)line,(int)'\0',BUFFER_SIZE);
	}

	fprintf(g_fp_LogFile,"\n g_vertices: <%lu>\t g_texture:<%lu>\t g_normals:<%lu>\t g_face_tri:<%lu>\n",
				g_vertices.size(),g_texture.size(),g_normals.size(),g_face_tri.size());
	fprintf(g_fp_LogFile,"\n Closing Mesh File\n");
	fclose(g_fp_MeshFile);
}