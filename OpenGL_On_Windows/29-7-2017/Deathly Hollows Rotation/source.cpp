#include<Windows.h>
#include<stdio.h>
#include<math.h>


#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

#define WIN_WIDTH  800
#define WIN_HEIGHT 800
#define PI 3.1415926535898


DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

BOOL gbFullscreen = FALSE;
BOOL gbEscapeKeyIsPressed = FALSE;
BOOL gbActiveWindow = FALSE;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

float gfangleTri;
float gfAngleCircle;

void initialize(void);
void display(void);
void resize(int width, int height);
void uninitialize(void);

void DrawDeathlyHollowsSymbol();
void UpdateAngle();


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCndLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= L"WINDOW";

	BOOL bDone = FALSE;

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(BLACK_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Deathly Hollows Native"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}
	else
	{
		ghwnd =hwnd;
	}

	initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while(bDone == FALSE)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message ==  WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbEscapeKeyIsPressed == TRUE)
			{
				bDone = TRUE;
			}
			else
			{
				UpdateAngle();
				display();
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen();
	
	TCHAR szMessage[500]={'\0'};

	switch(iMsg)
	{

	case WM_CREATE:
		break;

	case WM_KEYDOWN:
		if((wParam == 'F') || (wParam == 'f'))
		{
			ToggleFullScreen();
			if(gbFullscreen == FALSE)
			{
				gbFullscreen = TRUE;
			}
			else
			{
				gbFullscreen = FALSE;
			}
		}
		else if(wParam == VK_ESCAPE)
		{
			gbEscapeKeyIsPressed = TRUE;
		}
	break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
	break;
	case VK_ESCAPE:
		
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;	
	}
	return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void initialize()
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc =GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex ==0)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd) == FALSE)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	ghrc= wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	if(wglMakeCurrent(ghdc,ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd,ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	
	return;
}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	
	glRotatef(gfangleTri,0.0f, 1.0f, 0.0f);
	DrawDeathlyHollowsSymbol();

	SwapBuffers(ghdc);
	return;
}

void resize(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}	

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	gluPerspective(45.0f,(GLsizei)width/(GLsizei)height,0.1f,100.0f);
	
	return;
}

void DrawDeathlyHollowsSymbol()
{
	float fCounter = 0.02f;

	GLint circle_points = 1000;
	double angle;
	
	GLfloat glf_XCordUp = 0.0f;
	GLfloat glf_YCordUp = 0.8f;
	
	GLfloat glf_XCordLeft = -0.8f;
	GLfloat glf_YCordLeft = -0.8f;

	GLfloat glf_XCordRight = 0.8f;
	GLfloat glf_YCordRight = -0.8f;
	
	//----------------------- CALCULATE THE LENGTH OF EACH SIDE BY DISTANCE FORMUKLA -----------------------
	GLfloat glf_SideOne = sqrt(pow((glf_XCordUp - glf_XCordLeft),2) + pow((glf_YCordUp - glf_YCordLeft),2));
	GLfloat glf_SideTwo = sqrt(pow((glf_XCordRight - glf_XCordLeft),2) + pow((glf_YCordRight - glf_YCordLeft),2));
	GLfloat glf_SideThree = sqrt(pow((glf_XCordRight - glf_XCordUp),2) + pow((glf_YCordRight - glf_YCordUp),2));

	GLfloat glf_TriPerimetr  (glf_SideOne + glf_SideTwo + glf_SideThree);

	//---------------------- CALCULATE THE X AND Y CO-ORDINATES OF CIRCLE --------------------------
	GLfloat glf_XCircleCentr = ( ((glf_SideOne *glf_XCordRight ) + (glf_SideTwo * glf_XCordUp) + (glf_SideThree * glf_XCordLeft)) / glf_TriPerimetr);
	GLfloat glf_YCircleCentr = ( ((glf_SideOne *glf_YCordRight ) + (glf_SideTwo * glf_YCordUp) + (glf_SideThree * glf_YCordLeft)) / glf_TriPerimetr);

	GLfloat glf_SemiTrianglePerimeter = glf_TriPerimetr /2.0f;
	
	//--------------------- HERONES FORMULA -----------------------------
	GLfloat glf_TriangleArea = sqrt((glf_SemiTrianglePerimeter)*(glf_SemiTrianglePerimeter - glf_SideOne)*(glf_SemiTrianglePerimeter - glf_SideTwo)*(glf_SemiTrianglePerimeter - glf_SideThree));
	

	GLfloat glf_CircleRadious = (2*glf_TriangleArea)/ glf_TriPerimetr ;

	//------------------------ CALCULATE THE HEIGHT OF THE TRIANGLE ---------------------------
	GLfloat  glf_XMidOfBase;
	if(glf_XCordLeft == glf_XCordRight)
	{
		 glf_XMidOfBase = (glf_XCordLeft);
	}
	else
	{
		 glf_XMidOfBase = (glf_XCordLeft + glf_XCordRight);
	}
	
	GLfloat glf_YMidOfBase ;

	if(glf_YCordLeft == glf_YCordRight)
	{
		glf_YMidOfBase = (glf_YCordLeft);
	}
	else
	{		
		glf_YMidOfBase = (glf_YCordLeft + glf_YCordRight);
	}
	
	glBegin(GL_LINES);
		
		glColor3f(1.0f,0.0f,0.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);
		glVertex3f(glf_XCordLeft,glf_YCordRight,0.0f);

		glVertex3f(glf_XCordLeft,glf_YCordRight,0.0f);
		glVertex3f(glf_XCordRight,glf_YCordRight,0.0f);

		glVertex3f(glf_XCordRight,glf_YCordRight,0.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);

		glColor3f(1.0f,0.0f,1.0f);
		glVertex3f(glf_XCordUp,glf_YCordUp,0.0f);
		glVertex3f(glf_XMidOfBase,glf_YMidOfBase,0.0f);
				
	glEnd();

	//glRotatef(gfAngleCircle,0.0f, 1.0f, 0.0f);

	glBegin(GL_LINE_LOOP);
		glColor3f(0.0f,1.0f,1.0f);
			
		for(int i=0; i < circle_points; i++)
		{
			angle = 2 * PI * i/circle_points;
			glVertex2f((GLfloat)((glf_CircleRadious *cos(angle)) + glf_XCircleCentr),(GLfloat)((glf_CircleRadious *sin(angle))+ glf_YCircleCentr));
		}
		
		
   glEnd();
	return;
}

void UpdateAngle()
{
	gfangleTri = gfangleTri +0.10f;
	
	if(gfangleTri  >=  360.0f )
	{
		gfangleTri = 0;
	}
	gfAngleCircle = gfAngleCircle + 0.01;
	if(gfAngleCircle >= 360.0f)
	{
		gfAngleCircle = 0.0f;
	}
}

void uninitialize()
{
	if(gbFullscreen == TRUE)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL,NULL);

	wglDeleteContext(ghrc);
	ghrc =  NULL;

	ReleaseDC(ghwnd,ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);

	return;
}

void ToggleFullScreen()
{
	MONITORINFO mi;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		//ShowCursor(FALSE);
	}
	else
	{
	
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

}