#include<Windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine, int iCmdShow)

{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[]= L"WINDOW";
	
	int iHeight = GetSystemMetrics(SM_CYMAXIMIZED) ;
	int iWidth =GetSystemMetrics(SM_CXMAXIMIZED) ;
	int iHorizontalBorder = GetSystemMetrics(SM_CXFIXEDFRAME);
	int iVerticalBorder = GetSystemMetrics(SM_CYFIXEDFRAME);
	

	wndclass.cbSize =sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra =0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.lpszClassName =szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance,TEXT("IDI_ICON1"));
	wndclass.hCursor =LoadCursor(NULL,IDC_ARROW);
	wndclass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hbrBackground =(HBRUSH)GetStockObject(WHITE_BRUSH);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("ASSIGNMENT WINDOW"),
		WS_OVERLAPPEDWINDOW,
		((iWidth/2) -(2*iVerticalBorder)),
		((iHeight/2) -iHorizontalBorder),
		(iWidth/2),
		(iHeight/2),
		NULL,
		NULL,
		hInstance,
		NULL);

	if(NULL == hwnd)
	{
		MessageBox(HWND_DESKTOP,L"ERROR WHILE CreateWindow",L"ERROR",0);
		return 0;
	}

	ShowWindow(hwnd,iCmdShow);
	UpdateWindow(hwnd);
	
	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch(iMsg)
	{

	case WM_CREATE:
		//MessageBox(hwnd,L"INSIDE WM_CREATE",L"WM_CREATE",MB_OK);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
		return(DefWindowProc(hwnd,iMsg,wParam,lParam));
}