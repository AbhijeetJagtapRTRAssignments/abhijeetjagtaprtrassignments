var canvas=null;
var gl=null; 

var bFullscreen=false;

var canvas_original_width;
var canvas_original_height;

const WebGLMacros= 
{
VDG_ATTRIBUTE_VERTEX:0,
VDG_ATTRIBUTE_COLOR:1,
VDG_ATTRIBUTE_NORMAL:2,
VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var gVao_Triangle;
var gVbo_Position_Triangle;
var gVbo_Color_Triangle;

var gVao_Rectangle;
var gVbo_Position_Rectangle;
var gVbo_Color_Rectangle;


var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main()
{
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    canvas_original_width=canvas.width;
    canvas_original_height=canvas.height;
    
    // register all required event handlers just like event-deligate model
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);


    init();

    resize();
    draw();
}

function toggleFullScreen()
{
    var fullscreen_element =
    document.fullscreenElement ||
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    if(fullscreen_element==null)
    {
        if(canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        bFullscreen=true;
    }
    else
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        bFullscreen=false;
    }
}

function init()
{
    gl = canvas.getContext("webgl2");
    if(gl==null) 
    {
        console.log("ERROR WHILE GETTING WEBGL CONTEXT2 ");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
    
    // vertex shader
    var vertexShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 outColor;"+
    "uniform mat4 u_mvp_matrix;"+
    "void main(void)"+
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
	"outColor=vColor;"+
    "}";
    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    // fragment shader
    var fragmentShaderSourceCode=
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
	"in vec4 outColor;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = outColor;" +
    "}"
    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }
    
    shaderProgramObject=gl.createProgram();
    gl.attachShader(shaderProgramObject,vertexShaderObject);
    gl.attachShader(shaderProgramObject,fragmentShaderObject);
    
    gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");
 
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
   
    var TriangleVertices=new Float32Array([
                                           0.0,  1.0, 0.0,   
                                           -1.0, -1.0, 0.0, 
                                           1.0, -1.0, 0.0   
                                           ]);
    var TriangleColor=new Float32Array([
                                           1.0,  0.0, 0.0,   
                                           0.0, 1.0, 0.0, 
                                           0.0, 0.0, 1.0   
                                           ]);
	
    gVao_Triangle=gl.createVertexArray();
    gl.bindVertexArray(gVao_Triangle);
    
		gVbo_Position_Triangle = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Position_Triangle);
			gl.bufferData(gl.ARRAY_BUFFER,TriangleVertices,gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
		
		gVbo_Color_Triangle=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Color_Triangle);
			gl.bufferData(gl.ARRAY_BUFFER,TriangleColor,gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);
	
	
    var RectangleVertices=new Float32Array([
                                            -1.0,1.0,0.0,
											-1.0,-1.0,0.0,
											1.0,-1.0,0.0,
											1.0,1.0,0.0
                                           ]);										   
	var RectangleColor=new Float32Array([
                                           	0.392,0.5843,0.9294,
											0.392,0.5843,0.9294,
											0.392,0.5843,0.9294,
											0.392,0.5843,0.9294
                                       ]);
									   
	gVao_Rectangle=gl.createVertexArray();
	gl.bindVertexArray(gVao_Rectangle);
		gVbo_Position_Rectangle=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Position_Rectangle);
			gl.bufferData(gl.ARRAY_BUFFER,RectangleVertices,gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
		
		gVbo_Color_Rectangle=gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Color_Rectangle);
			gl.bufferData(gl.ARRAY_BUFFER,RectangleColor,gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3, gl.FLOAT,false,0,0);
			gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);


    gl.clearColor(0.0, 0.0, 0.0, 1.0); 
    
  
    perspectiveProjectionMatrix=mat4.create();
}

function resize()
{
    // code
    if(bFullscreen==true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;
    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;
    }
   
    gl.viewport(0, 0, canvas.width, canvas.height);
    
mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0 );
}

function draw()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT);
    
    gl.useProgram(shaderProgramObject);
    
    var modelViewMatrix=mat4.create();
    var modelViewProjectionMatrix=mat4.create();
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,-0.0,-5.0]);
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(gVao_Triangle);

    gl.drawArrays(gl.TRIANGLES,0,3);
    
    gl.bindVertexArray(null);
	
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-5.0]);
    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

    gl.bindVertexArray(gVao_Rectangle);

    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    
    gl.bindVertexArray(null);
	
	
    gl.useProgram(null);
    
    // animation loop
    requestAnimationFrame(draw, canvas);
}

function uninitialize()
{

    if(gVao_Triangle)
    {
        gl.deleteVertexArray(gVao_Triangle);
        gVao_Triangle=null;
    }
    
    if(gVbo_Position_Triangle)
    {
        gl.deleteBuffer(gVbo_Position_Triangle);
        gVbo_Position_Triangle=null;
    }
    if(gVbo_Color_Triangle)
    {
        gl.deleteBuffer(gVbo_Color_Triangle);
        gVbo_Color_Triangle=null;
    } 
    if(shaderProgramObject)
    {
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }
        
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }
        
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject=null;
    }
}

function keyDown(event)
{
    // code
    switch(event.keyCode)
    {
        case 27: 
            uninitialize();
           
            window.close(); 
            break;
        case 70: 
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
  
}
