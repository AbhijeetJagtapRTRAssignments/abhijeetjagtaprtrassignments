var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE:3,
};

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var vao_Smiley;
var vbo_Smiley_Position;
var vbo_Smiley_Texture;

var checkerboard_Texture=0;
var uniform_texture0_sampler;

var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


// onload function
function main()
{
   
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
   
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
   
    gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

 //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "out_texture0_coord = vTexture0_Coord;"+
    "}" ;


    gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(gVertexShaderObject);

    if (gl.getShaderParameter(gVertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gVertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

// fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
    "}" ;

    gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(gFragmentShaderObject);

    if(gl.getShaderParameter(gFragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gFragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    
    gShaderProgramObject = gl.createProgram();

    gl.attachShader(gShaderProgramObject,gVertexShaderObject);
    gl.attachShader(gShaderProgramObject,gFragmentShaderObject);


    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE,"vTexture0_Coord");


    gl.linkProgram(gShaderProgramObject);

    if (gl.getProgramParameter(gShaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(gShaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    mvpUniform = gl.getUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    uniform_texture0_sampler = gl.getUniformLocation(gShaderProgramObject,"u_texture0_sampler");

    var squareVertices = new Float32Array([
        1.0, 1.0, 0.0,
        -1.0, 1.0, 0.0,
        -1.0, -1.0, 0.0,
        1.0, -1.0, 0.0
    ]);

     
    var squareTextureCoordinates = new Float32Array([
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
		1.0, 1.0
    
    ]);


    checkerboard_Texture=loadGLTexture();
    
    vao_Smiley=gl.createVertexArray();
    gl.bindVertexArray(vao_Smiley);
    
    vbo_Smiley_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Position);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    vbo_Smiley_Texture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Texture);
    
    gl.bufferData(gl.ARRAY_BUFFER,0,gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE,
                           2, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);
  
    gl.enable(gl.DEPTH_TEST);

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function loadGLTexture()
{
	var i = 0, j = 0, c = 0;
	
	
	var TEX_WIDTH  =  64;
	var TEX_HEIGHT =  64;
	var DEPTH = 4;
	var checker_board_texture_array = new Uint8Array(TEX_HEIGHT*TEX_WIDTH*DEPTH);
	
	for (i = 0; i < TEX_HEIGHT; i++)
	{
		for (j = 0; j < TEX_WIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			checker_board_texture_array[0 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[1 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[2 + DEPTH*(i + TEX_HEIGHT*(j))] = c;
			checker_board_texture_array[3 + DEPTH*(i + TEX_HEIGHT*(j))] = 255;
		}
	}
	
	var texture=gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_ALIGNMENT, true);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, TEX_WIDTH, TEX_HEIGHT, 0, gl.RGBA, gl.UNSIGNED_BYTE,checker_board_texture_array);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.bindTexture(gl.TEXTURE_2D, null);
	
	return texture;
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var squareVertices = new Float32Array
	([
        0.0, 1.0, 0.0,   
        -2.0, 1.0, 0.0,   
        -2.0, -1.0, 0.0,  
        0.0, -1.0, 0.0,
		
        2.414210, 1.0, -1.41421,
        1.0, 1.0, 0.0,
        1.0, -1.0, 0.0,
        2.41421, -1.0, -1.41421
        ]);


    var squareTexcoords =  new Float32Array
	([

        0.0,0.0,
		0.0,1.0,
		1.0,1.0,
		1.0,0.0,

		0.0,0.0,
		0.0,1.0,
		1.0,1.0,
		1.0,0.0
    
    ]);

    gl.useProgram(gShaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);



    gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D,checkerboard_Texture);
  gl.uniform1i(uniform_texture0_sampler,0);
 

 
  gl.bindVertexArray(vao_Smiley);
  
  gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Position);

  gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.DYNAMIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER,null);

  gl.bindBuffer(gl.ARRAY_BUFFER,vbo_Smiley_Texture);

  gl.bufferData(gl.ARRAY_BUFFER,squareTexcoords,gl.DYNAMIC_DRAW);

  gl.bindBuffer(gl.ARRAY_BUFFER,null);

  gl.drawArrays(gl.TRIANGLE_FAN,0,4); 
  gl.drawArrays(gl.TRIANGLE_FAN,4,4); 
 
  
 
  gl.bindVertexArray(null);
 
  gl.useProgram(null);

 
    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: 
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : 
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(vao_Smiley)
    {
        gl.deleteVertexArray(vao_Smiley);
        vao_Smiley = null;
    }

    if(vbo_Smiley_Position)
    {
        gl.deleteBuffer(vbo_Smiley_Position);
        vbo_Smiley_Position = null;
    }

    if(vbo_Smiley_Texture)
    {
        gl.deleteBuffer(vbo_Smiley_Texture);
        vbo_Smiley_Texture = null;
    }

    if(gShaderProgramObject)
    {
        if(gFragmentShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gFragmentShaderObject);
        gl.deleteShader(gFragmentShaderObject);
        gFragmentShaderObject = null;
        }

        if(gVertexShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gVertexShaderObject);
        gl.deleteShader(gVertexShaderObject);
        gVertexShaderObject = null;
        }
        gl.deleteProgram(gShaderProgramObject);
        gShaderProgramObject = null;
    }
}
