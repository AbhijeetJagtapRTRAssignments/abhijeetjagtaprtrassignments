var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var Vao_Rectangle;
var Vbo_Position;
var Vbo_Texture;

var Texture_Smiley = 0;
var TextureSamplerUniform;

var gTexture_White=0;
var WhiteColor = (255,255,255,255);

var KeyPressed =0;

var MVPUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


function main()
{
    
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

     // register keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);


    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{

    gl = canvas.getContext("webgl2");
    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

 //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "out_texture0_coord = vTexture0_Coord;"+
    "}" ;


    gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(gVertexShaderObject);

    if (gl.getShaderParameter(gVertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gVertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
    "}" ;

    gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(gFragmentShaderObject);

    if(gl.getShaderParameter(gFragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gFragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    // shader program

    gShaderProgramObject = gl.createProgram();

    gl.attachShader(gShaderProgramObject,gVertexShaderObject);
    gl.attachShader(gShaderProgramObject,gFragmentShaderObject);

    // pre-link bindind of shader program object with vertex shader attributes 

    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

    // linking 

    gl.linkProgram(gShaderProgramObject);

    if (gl.getProgramParameter(gShaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(gShaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

	Texture_Smiley = loadGLTexture("smiley.png");
	gTexture_White = loadWhiteColorTexture();

   

    MVPUniform = gl.getUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    TextureSamplerUniform = gl.getUniformLocation(gShaderProgramObject,"u_texture0_sampler");

   // Square vertices

    var squareVertices = new Float32Array([
        1.0, 1.0, 0.0,        
        -1.0, 1.0, 0.0,
        -1.0, -1.0, 0.0,
        1.0, -1.0, 0.0
    ]);

     
    var squareTextureCoordinates = new Float32Array([
    
        1.0, 0.0,
        
            0.0, 0.0,
        
            0.0, 1.0,
        
            1.0, 1.0
    
    ]);


    
    Vao_Rectangle=gl.createVertexArray();
    gl.bindVertexArray(Vao_Rectangle);
    
    Vbo_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,Vbo_Position);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    Vbo_Texture = gl.createBuffer();
    
    gl.bindBuffer(gl.ARRAY_BUFFER,Vbo_Texture);
    gl.bufferData(gl.ARRAY_BUFFER,squareTextureCoordinates,gl.STATIC_DRAW );
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, // 2 is for S and T co-ordinates in our pyramidTexcoords array
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);

    gl.enable(gl.DEPTH_TEST);
    

    gl.clearColor(0.0,0.0,0.0,1.0);

    perspectiveProjectionMatrix = mat4.create();
}

function loadWhiteColorTexture()
{
	var TEX_WIDTH = 64,TEX_HEIGHT =64,DEPTH =4;
	var i = 0, j = 0, c = 0;
	var texture_array = new Uint8Array(TEX_HEIGHT*TEX_WIDTH*DEPTH);
	var texture;
	
	
	for (i = 0; i < TEX_HEIGHT; i++)
	{
		for (j = 0; j < TEX_WIDTH; j++)
		{
			//c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;

			texture_array[0 + 4*(i + TEX_HEIGHT*(DEPTH))] = 255;
			texture_array[1 + 4*(i + TEX_HEIGHT*(DEPTH))] = 255;
			texture_array[2 + 4*(i + TEX_HEIGHT*(DEPTH))] = 255;
			texture_array[3 + 4*(i + TEX_HEIGHT*(DEPTH))] = 255;
		}
	}
	
	 texture = gl.createTexture();
	 gl.bindTexture(gl.TEXTURE_2D, texture);
     gl.pixelStorei(gl.UNPACK_ALIGNMENT, true);//LUMINANCE
     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, TEX_WIDTH, TEX_HEIGHT, 0, gl.RGBA, gl.UNSIGNED_BYTE,texture_array);
	 gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
     gl.bindTexture(gl.TEXTURE_2D, null);
	 
	return texture;
}


function loadGLTexture(path)
{
	var texture;
	texture = gl.createTexture();
    texture.image = new Image();
    texture.image.src=path;
    texture.image.onload = function ()
    {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }
	
	return texture;
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{

 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(gShaderProgramObject);

    
	var rectangleTexcoord=[];

	
	if (KeyPressed == 1)
	{
		var quadVertices=new Float32Array([
			0.5, 0.5,
			0.0, 0.5,
			0.0, 0.0,
			0.5, 0.0
		]);
		
		rectangleTexcoord=quadVertices.copyWithin();
	}
	else if (KeyPressed == 2) 
	{
		var quadVertices=new Float32Array([
		
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0
		]);
		rectangleTexcoord=quadVertices.copyWithin();

	}
	else if (KeyPressed == 3)
	{
		var quadVertices=new Float32Array([
			2.0, 2.0,
			0.0, 2.0,
			0.0, 0.0,
			2.0, 0.0
		]);
		rectangleTexcoord=quadVertices.copyWithin();

	}
	else if (KeyPressed == 4)
	{
		var quadVertices=new Float32Array([
		
			0.5, 0.5,
			0.5, 0.5,
			0.5, 0.5,
			0.5, 0.5
		]);
		rectangleTexcoord=quadVertices.copyWithin();
		
	}
	else
	{
		var quadVertices=new Float32Array([
			0.5, 0.5,
			0.5, 0.5,
			0.5, 0.5,
			0.5, 0.5
		]);

		rectangleTexcoord=quadVertices.copyWithin();
	}
	
	gl.bindBuffer(gl.ARRAY_BUFFER, Vbo_Texture);
	gl.bufferData(gl.ARRAY_BUFFER,rectangleTexcoord, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);

    mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);

    // Bind with texture

    gl.uniformMatrix4fv(MVPUniform,false,modelViewProjectionMatrix);


    gl.activeTexture(gl.TEXTURE0);
    
        if (KeyPressed ==1 || KeyPressed ==2 || KeyPressed ==3 || KeyPressed ==4)
        {
            gl.bindTexture(gl.TEXTURE_2D, Texture_Smiley);
           
        }
    
        else
        {
            
            gl.bindTexture(gl.TEXTURE_2D, gTexture_White);
        }
    
    gl.uniform1i(TextureSamplerUniform, 0);
    
    gl.bindVertexArray(Vao_Rectangle);

    gl.drawArrays(gl.TRIANGLE_FAN,0,4);

    gl.bindVertexArray(null);
    
    gl.useProgram(null);

    
    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27: 
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : 
                   togglefullscreen();
                   break;

           case 49:   
                   KeyPressed =1;
                   break;
          
           case 50: 
                   KeyPressed =2;
                   break;
           
           case 51: 
                   KeyPressed =3;
                   break;

           case 52:  
                   KeyPressed =4;
                   break;

            default:
                   KeyPressed =0;

	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(Vao_Rectangle)
 {
        gl.deleteVertexArray(Vao_Rectangle);
        Vao_Rectangle = null;
    }

    if(Vbo_Position)
    {
        gl.deleteBuffer(Vbo_Position);
        Vbo_Position = null;
    }

    if(Vbo_Texture)
    {
        gl.deleteBuffer(Vbo_Texture);
        Vbo_Texture = null;
    }

    if(gShaderProgramObject)
    {
        if(gFragmentShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gFragmentShaderObject);
        gl.deleteShader(gFragmentShaderObject);
        gFragmentShaderObject = null;
        }

        if(gVertexShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gVertexShaderObject);
        gl.deleteShader(gVertexShaderObject);
        gVertexShaderObject = null;
        }
        gl.deleteProgram(gShaderProgramObject);
        gShaderProgramObject = null;
    }
}
