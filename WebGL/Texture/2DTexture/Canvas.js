var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
  VDG_ATTRIBUTE_VERTEX:0,
  VDG_ATTRIBUTE_COLOR:1,
  VDG_ATTRIBUTE_NORMAL:2,
  VDG_ATTRIBUTE_TEXTURE0:3,
};

var gVertexShaderObject;
var gFragmentShaderObject;
var gShaderProgramObject;

var gVao_Smiley;
var gVbo_Position_Quad;
var gVbo_Texture_Quad;

var smiley_Texture=0;
var uniform_texture0_sampler;

var MVPUniform;

var PerspectiveProjectionMatrix;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame ||
                           window.webkitCancelAnimationFrame ||
                           window.webkitCancelRequestAnimationFrame ||
                           window.mozCancelRequestAnimationFrame ||
                           window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame ||
                           window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame ||
                           window.msCancelAnimationFrame ;


function main()
{
   
    canvas = document.getElementById("AMC");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    
    console.log("Canvas Width : "+canvas.width+" And Canvas Height : "+canvas.height);

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();

    draw();
}


function togglefullscreen()
{
  var fullscreen_element = 
                          document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || 
                          document.msFullscreenElement || null ;

if (fullscreen_element == null)
{
  if (canvas.requestFullscreen)
      canvas.requestFullscreen();
  else if (canvas.mozRequestFullScreen)
     canvas.mozRequestFullScreen();
  else if (canvas.webkitRequestFullscreen)
     canvas.webkitRequestFullscreen();
  else if (canvas.msRequestFullscreen)
     canvas.msRequestFullscreen();

     bFullscreen = true;
}

else
{
 if (document.exitFullscreen)
     document.exitFullscreen();
 else if (document.mozCancelFullScreen)
     document.mozCancelFullScreen();
 else if (document.webkitExitFullscreen)
     document.webkitExitFullscreen();
 else if (document.msExitFullscreen)
     document.msExitFullscreen();

     bFullscreen = false;
}

}

function init()
{
   gl = canvas.getContext("webgl2");

    if(gl == null)
    {
       console.log("Failed to get WebGL rendering context");
       return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

 //vertex Shader 

    var vertexShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "out vec2 out_texture0_coord;"+
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition; " +
    "out_texture0_coord = vTexture0_Coord;"+
    "}" ;


    gVertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject,vertexShaderSourceCode);
    gl.compileShader(gVertexShaderObject);

    if (gl.getShaderParameter(gVertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gVertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

 // fragment shader 

    var fragmentShaderSourceCode =
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main()" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);"+
    "}" ;

    gFragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
    gl.compileShader(gFragmentShaderObject);

    if(gl.getShaderParameter(gFragmentShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(gFragmentShaderObject);
        if(error.length >0)
        {
            alert(error);
            uninitialize();
        }
    }

    gShaderProgramObject = gl.createProgram();

    gl.attachShader(gShaderProgramObject,gVertexShaderObject);
    gl.attachShader(gShaderProgramObject,gFragmentShaderObject);

   
    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

    gl.bindAttribLocation(gShaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_Coord");

    gl.linkProgram(gShaderProgramObject);

    if (gl.getProgramParameter(gShaderProgramObject,gl.LINK_STATUS)== false)
    {
        var error = gl.getProgramInfoLog(gShaderProgramObject);
        if(error.length>0)
        {
            alert(error);
            uninitialize();
        }
    }

    MVPUniform = gl.getUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    uniform_texture0_sampler = gl.getUniformLocation(gShaderProgramObject,"u_texture0_sampler");

   // Square vertices

    var squareVertices = new Float32Array([
        1.0, 1.0, 0.0,
        -1.0, 1.0, 0.0,
         -1.0, -1.0, 0.0,
         1.0, -1.0, 0.0
    ]);

     
    var squareTextureCoordinates = new Float32Array([
    
			1.0, 0.0,
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0
    
    ]);

		smiley_Texture = gl.createTexture();
        smiley_Texture.image = new Image();
        smiley_Texture.image.src="smiley.png";
        smiley_Texture.image.onload = function ()
        {
          gl.bindTexture(gl.TEXTURE_2D,smiley_Texture);
          gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
          gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,smiley_Texture.image);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
          gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
          gl.bindTexture(gl.TEXTURE_2D,null);
    
        }
    
    gVao_Smiley=gl.createVertexArray();
    gl.bindVertexArray(gVao_Smiley);
    
    gVbo_Position_Quad = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Position_Quad);
    gl.bufferData(gl.ARRAY_BUFFER,squareVertices,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
                           3, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gVbo_Texture_Quad = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,gVbo_Texture_Quad);
    gl.bufferData(gl.ARRAY_BUFFER,squareTextureCoordinates,gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
                           2, 
                           gl.FLOAT,
                           false,0,0);
    gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    
    gl.bindVertexArray(null);
    
    gl.enable(gl.DEPTH_TEST);

    gl.clearColor(0.0,0.0,0.0,1.0);

    PerspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    if(bFullscreen == true)
    {
        canvas.width=window.innerWidth;
        canvas.height=window.innerHeight;

    }
    else
    {
        canvas.width=canvas_original_width;
        canvas.height=canvas_original_height;        
    }

    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(PerspectiveProjectionMatrix,45.0,parseFloat(canvas.width/canvas.height),0.1,100.0);
    
}


function draw()
{
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(gShaderProgramObject);
    
    var modelViewMatrix= mat4.create();
    var modelViewProjectionMatrix= mat4.create();

    mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);

    mat4.multiply(modelViewProjectionMatrix,PerspectiveProjectionMatrix,modelViewMatrix);

    gl.uniformMatrix4fv(MVPUniform,false,modelViewProjectionMatrix);

    gl.bindTexture(gl.TEXTURE_2D,smiley_Texture);

    gl.uniform1i(uniform_texture0_sampler, 0);
    
    gl.bindVertexArray(gVao_Smiley);

    gl.drawArrays(gl.TRIANGLE_FAN,0,4);

    gl.bindVertexArray(null);
    
    gl.useProgram(null);

    requestAnimationFrame(draw,canvas);
}


function keyDown(event)
{
    switch(event.keyCode)
       {
           case 27:
                   uninitialize();
                   window.close();
                   break;
           
           case 70 : 
                   togglefullscreen();
                   break;
	}
}

function mouseDown()
{
    
}

function uninitialize()
{
    if(gVao_Smiley)
    {
        gl.deleteVertexArray(gVao_Smiley);
        gVao_Smiley = null;
    }

    if(gVbo_Position_Quad)
    {
        gl.deleteBuffer(gVbo_Position_Quad);
        gVbo_Position_Quad = null;
    }

    if(gVbo_Texture_Quad)
    {
        gl.deleteBuffer(gVbo_Texture_Quad);
        gVbo_Texture_Quad = null;
    }

    if(gShaderProgramObject)
    {
        if(gFragmentShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gFragmentShaderObject);
        gl.deleteShader(gFragmentShaderObject);
        gFragmentShaderObject = null;
        }

        if(gVertexShaderObject)
        {
        gl.detachShader(gShaderProgramObject,gVertexShaderObject);
        gl.deleteShader(gVertexShaderObject);
        gVertexShaderObject = null;
        }
        gl.deleteProgram(gShaderProgramObject);
        gShaderProgramObject = null;
    }
}
