//onload function

function main()
{
	//get canvas elementFromPoint
	var canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas process failed!!!");
	else
		console.log("Obtaining canvas process successful");
	
	console.log("Canvas Width : " +canvas.width+"And Canvas Height :"+canvas.height);
	
	var context =canvas.getContext("2d");
	if(!context)
		console.log("Obtaining context process failed!!!");
	else
		console.log("Obtaining context process successful");
	
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width, canvas.height);
	
	context.textAlign="center";
	context.textBaseline="middle";
	
	var str = "HELLO WORLD !!!";
	
	context.fillStyle="white";
	
	context.fontSize = "80px sans-serif";
	context.fillText(str,canvas.width/2, canvas.height/2);
	
	
}