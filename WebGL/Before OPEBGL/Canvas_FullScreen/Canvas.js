//onload function

var canvas  = null;
var context = null;
var gl = null;

var bFullScreen = false;

var canvas_oiginal_height;
var canvas-Original_width;

var requstAnimation =
window.requstAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.msRequestAnimationFrame;

function main()
{
	//get canvas elementFromPoint
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas process failed!!!");
	else
		console.log("Obtaining canvas process successful");
	
	//console.log("Canvas Width : " +canvas.width+"And Canvas Height :"+canvas.height);
	
	context =canvas.getContext("2d");
	if(!context)
		console.log("Obtaining context process failed!!!");
	else
		console.log("Obtaining context process successful");
	
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width, canvas.height);
	
	drawText("HELLO WORLD");
	
	window.addEventListener("keydown", keyDown,false);
	window.addEventListener("click",mouseDown,false);
}

function drawText(text)
{
	context.textAlign="center";
	context.textBaseline="middle";
	
	context.fillStyle="white";
	context.fontSize = "80px sans-serif";

	context.fillText(text,canvas.width/2, canvas.height/2);
	
}

function toggleFullScreen()
{
	var fullscreen_element =
	document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
			if(document.exitFullscreen)
				document.exitFullscreen();
			 if(document.webkitExitFullscreen)
				document.webkitExitFullscreen();
			else if(document.msExitFullscreen)
				document.msExitFullscreen();			
	}
}

function keyDown(event)
{
	//alert("A key is pressed");
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			
			drawText("HELLO WORLD");;
		break;
	}
}

function mouseDown()
{
	alert("mouse is clicked");
}