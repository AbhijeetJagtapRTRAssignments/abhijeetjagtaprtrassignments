//onload function

var canvas  = null;
var context = null;
var gl = null;

var bFullScreen = false;

var canvas_oiginal_height;
var canvas_Original_width;

var requstAnimation =
window.requstAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//WITH USE OF THIS WE CAN STOP OUR RUNNING ANIMATION 
//WITHOUT MANIPULATING THE UPDATEANGLE
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame ||
window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame ||
window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame ||
window.msCancelAnimationFrame;

function main()
{
	//get canvas elementFromPoint
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas process failed!!!");
	else
		console.log("Obtaining canvas process successful");
	
	canvas_Original_width = canvas.width;
	canvas_oiginal_height = canvas.height;
	console.log("Canvas Width : " +canvas.width+"And Canvas Height :"+canvas.height);
	
	window.addEventListener("keydown", keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize, false);
	
	//INITIALIZE WEBGL
		init();
		
	//START DRAWING HERE AS A WARM-UP
	resize();
	draw();
}

function init()
{
	//HERE GET WEBGL 2.0 CONTEXT
	gl = canvas.getContext("webgl1");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	gl.clearColor(0.0,0.0,1.0,1.0);
}

function resize()
{
		if(bFullScreen == true)
		{
			canvas.width  = window.innerWidth;
			canvas.height = window.innerHeight;
		}
		else
		{
			canvas.width  = canvas_Original_width;
			canvas.height = canvas_oiginal_height;
		}
		gl.viewport(0,0,canvas.width, canvas.height);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	requestAnimationFrame(draw,canvas.height);
}
function toggleFullScreen()
{
	var fullscreen_element =
	document.fullscreen_element ||
	document.webkitFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else
	{
			if(document.exitFullscreen)
				document.exitFullscreen();
			 if(document.webkitExitFullscreen)
				document.webkitExitFullscreen();
			else if(document.msExitFullscreen)
				document.msExitFullscreen();			
	}
}

function keyDown(event)
{
	//alert("A key is pressed");
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			
			//drawText("HELLO WORLD");;
		break;
	}
}

function mouseDown()
{
	//alert("mouse is clicked");
}